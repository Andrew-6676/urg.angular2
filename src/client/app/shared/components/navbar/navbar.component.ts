import { Component }   from '@angular/core';

import { AppService }    from '../../services/app.service';
import { LoggerService } from '../../services/logger.service';
import {DialogsService} from '../dialogs/dialogs.service';

/**
 * This class represents the navigation bar component.
 */
@Component({
	moduleId: module.id,
	selector: 'sd-navbar',
	templateUrl: 'navbar.component.html',
	styleUrls: ['navbar.component.css'],
})

export class NavbarComponent {
	constructor(
		public _AppService: AppService,
	    public _dialogs: DialogsService,
	    private _logger: LoggerService
	) {};

	logout() {
		this._AppService.logout();
		return false;
	}

	sendMessage() {
		this._dialogs
			.prompt('Отправка сообщения', '', 'mail')
			.subscribe(
				(resp:any) => {
					if (resp) {
						this._AppService.sendMail(resp.text, resp.subject);
					}
				}
			);
		return false;
	}
}
