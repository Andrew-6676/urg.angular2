import { Component, Input } from '@angular/core';

import { AppService }     from '../../services/app.service';
import { LoggerService }  from '../../services/logger.service';


@Component({
	moduleId: module.id,
	selector: 'md-debug',
	templateUrl: 'debug.component.html',
	styleUrls: ['debug.component.css'],
})
export class DebugComponent {
	@Input() src : any;
	private data: any;

	constructor(
		public _AppService: AppService,
		public _logger: LoggerService
	) {	}


}
