import { Component, Input } from '@angular/core';

import { AppService }     from '../../services/app.service';
import { LoggerService }  from '../../services/logger.service';


@Component({
	moduleId: module.id,
	selector: 'file-preview',
	templateUrl: 'file-preview.component.html',
	styleUrls: ['file-preview.component.css'],
})
export class FilePreviewComponent {
	@Input() file : any;

	constructor(
		public _AppService: AppService,
		public _logger: LoggerService
	) {	}


}
