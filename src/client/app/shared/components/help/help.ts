import { Component, Input, ViewChild } from '@angular/core';
import { Router }           from '@angular/router';

import { LoggerService }    from '../../services/logger.service';
import { AppService }       from '../../services/app.service';
import { HelpService }      from '../../services/help.service';
import { Observable }       from 'rxjs/Observable';
import '../../../operators'

@Component({
	moduleId: module.id,
	selector: 'help',
	template:
			`<ng-template #popTemplate>
				<div class="help-content" [innerHtml]="help.text"></div>
				<hr>
				<div style="text-align: right">
					<button *ngIf="false" mat-raised-button (click)="goToHelp()" type="button">
						<mat-icon>forward</mat-icon>
						Перейти к справке
					</button>
					<button mat-raised-button color="primary" (click)="pop.hide()" type="button">
						<mat-icon>close</mat-icon>
						Закрыть
					</button>
				</div>
			</ng-template>

			<div class="host" mat-mini-fab
				    [popover]="popTemplate"
			        [popoverTitle]="help.title"
			        placement="right"
			        triggers="" 
				    #pop="bs-popover">
				<mat-icon class="question-sign"
				         (click)="showHelp()"
				         (mouseenter)="mouseEnter()"
				         (mouseout)="mouseOut()">
					help_outline
				</mat-icon>
			</div>
	`,
	styles: [
		`
		:host >>> .popover {
			min-width: 400px;
			max-width: 600px;
			box-shadow: 5px 5px 55px rgba(0,0,0,.8);
			/*top: 22px !important;*/
		}
		:host >>> .popover>.arrow {
			/*top: 18px !important;*/
		}
		/*:host >>> .popover>.arrow:after {*/
			/*top: 18px !important;*/
		/*}*/
		.host {
			display: flex;
			position: relative;
			height: 100%;
			align-items: center;
            margin-right: 5px;
		}
		.question-sign {
			color: #bcbcbc !important;
			font-size: 8px;
			line-height: 16px;
			width: 16px;
			/*border-radius: 30px;*/
			/*border: 1px solid #bcbcbc;*/
			height: 16px;
			display: inline-block;
			text-align: center;
			cursor: pointer;
            /*margin-left: 7px;*/
            /*margin-top: -11px;*/
            /*position: absolute;*/
		}
		.question-sign {
			font-size: 20px;
		}
		.question-sign:hover {
			color:#f00 !important;
			/*border-color: #f00;*/
			transition: ease-in 1.5s
		}`
	]
})
export class HelpComponent {
	@Input() param: string;
	@Input() tdelay: number = 1500;

	@ViewChild('pop') popover: any;
	help : {title: string, text:string} = {title: '-', text:'-'};

	private tmr:any = null;
/*-----------------------------------------------------------------------------------------*/
	constructor(
		private _logger: LoggerService,
		public _AppService: AppService,
		private _router: Router,
		private _helpService: HelpService
	) {}

	/*------------------------------------------------------------------------------------*/
	goToHelp() {
		this._logger.log('show help', this.param);
		this._router.navigate(['help', {index: this.param}]);
	}
	/*------------------------------------------------------------------------------------*/
	showHelp() {
		this.loadHelp(this.param);
	}
	/*------------------------------------------------------------------------------------*/
	mouseEnter() {
		this._logger.log('mouse enter');
		this.tmr = setTimeout(() => this.showTooltip(), this.tdelay);
	}
	/*------------------------------------------------------------------------------------*/
	mouseOut() {
		this._logger.debug('mouse out');
		clearTimeout(this.tmr);
	}
	/*------------------------------------------------------------------------------------*/
	showTooltip() {
		this._logger.styledLog('%cshow tooltip', 'background: #222; color: #bada55');
		//this.loadHelp(this.param);
		//this.popover.show();
	}
	/*------------------------------------------------------------------------------------*/
	loadHelp(index: string='-') {
		this._helpService.getByIndex(index)
			.subscribe(
				(resp:any) => {
					if (resp.length > 0) {
						this.help = resp[0];
						this._logger.debug('loaded help =', this.help);
						this.showPopover();
					} else {
						this.help.title = 'Запрашиваемый раздел справки не найден';
						this.help.text = 'Возможно, данный раздел справки ещё не заполнен';
						this.showPopover();
					}
				}
			);
		//this.title = index ? index : 'index';
		this._logger.warn(index);
	}

	showPopover() {
		Observable.of(true).delay(10).do(
			val => {
				this.popover.show();
			}
		).subscribe();
	}
}
