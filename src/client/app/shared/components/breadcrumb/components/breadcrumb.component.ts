import { Component }             from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { BreadcrumbService }     from './breadcrumbService';

/**
 * This component shows a breadcrumb trail for available routes the router can navigate to.
 * It subscribes to the router in order to update the breadcrumb trail as you navigate to a component.
 */
@Component({
	selector: 'bread-crumb',
	//templateUrl: 'breadcrumb.component.html',
	template: `
        <div [hidden]="_urls.length<0">
            <ol class="breadcrumb">
            	<li></li>
                <li *ngFor="let url of _urls; let last = last" [ngClass]="{'active': last}"> <!-- disable link of last item -->
                    <a role="button" *ngIf="!last" (click)="navigateTo(url)">{{friendlyName(url)}}</a>
                    <span *ngIf="last">{{friendlyName(url)}}</span>
                </li>
            </ol>
        </div>
    `,
	//stylesUrls: ['breadcrumb.component.css']
	styles: [`
		.breadcrumb {
		  padding: 0 30px;
		  margin-bottom: 20px;
		  background-color: #f5f5f5;
		  margin-top: -15px;
		  font-size: 0.8em;
		  border-radius: 0;
		}
	`]
})
export class BreadcrumbComponent {

	public _urls: string[];

	constructor(private router: Router, private breadcrumbService: BreadcrumbService) {
		this._urls = new Array();
		this.router.events.subscribe((navigationEnd: NavigationEnd) => {
			this._urls.length = 0; //Fastest way to clear out array
			this.generateBreadcrumbTrail(navigationEnd.urlAfterRedirects ? navigationEnd.urlAfterRedirects : navigationEnd.url);
		});
	}

	generateBreadcrumbTrail(url: string): void {
		// this._urls.unshift(url); //Add url to beginning of array (since the url is being recursively broken down from full url to its parent)
		// if (url.lastIndexOf('/') > 0) {
		// 	this.generateBreadcrumbTrail(url.substr(0, url.lastIndexOf('/'))); //Find last '/' and add everything before it as a parent route
		// }
	}

	navigateTo(url: string): void {
		this.router.navigateByUrl(url);
	}

	friendlyName(url: string): string {
		return !url ? '' : this.breadcrumbService.getFriendlyNameForRoute(url);
	}

}
