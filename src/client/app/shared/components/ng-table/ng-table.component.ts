import { Component, Input, Output, EventEmitter, ElementRef, OnInit, forwardRef } from '@angular/core';
import { AppService }     from '../../services/app.service';
import { LoggerService }  from '../../services/logger.service';

import * as _ from "lodash";

export class NgTableHeader {
    field: string;
    title: string;
    sort?: boolean
}

@Component({
	moduleId: module.id,
	selector: 'ng-table',
	templateUrl: 'ng-table.component.html',
	styleUrls: ['ng-table.component.css'],
})
export class NgTableComponent {
	@Input() data: any[] = [];
	@Input() totalCount: number = 0;
	@Input() headers: NgTableHeader[] = [];
	@Input() maxHeight: number = 200;
	@Input() disabled: boolean = false;
	@Input() checkBoxes: boolean = false;

    @Output() public onSort:EventEmitter<any> = new EventEmitter();
    @Output() public selected:EventEmitter<Map<any,any>> = new EventEmitter();
    @Output() public onPageChange:EventEmitter<any> = new EventEmitter();

    sortData: any = {};
	selectedMap: Map<any,any> = new Map();
    page = {
		currentPage: 1,
		itemsPP: 50,
	};
    /*---------------------------------------------------------------------------------*/

	constructor(
		public _AppService: AppService,
		public _logger: LoggerService
	) {}

	/*---------------------------------------------------------------------------------*/
    public onHeaderClick(field: string, event:MouseEvent):void {
        //this._logger.debug("click sort:", field, event);

        let tmp:any = {};
        Object.assign(tmp, this.sortData);

        if (event.ctrlKey) {
            // зажат Ctrl - возможна сортировка по нескольким столбцам
        } else {
            // сортировка только по одному столбцу
            this.sortData = {};
        }

        if (!(field in tmp)) {
            this.sortData[field] = 'asc';
        } else {
            switch (tmp[field]) {
                case "asc":
                    this.sortData[field] = 'desc';
                    break;
                case "desc":
                    delete this.sortData[field];
            }
        }
        this.onSort.emit(this.sortData);
    }
	/*---------------------------------------------------------------------------------*/
    getHeaderClass(field: string) {
        return {
            'desc': this.sortData[field]=='desc',
            'asc' : this.sortData[field]=='asc'
        };
	}
	/*---------------------------------------------------------------------------------*/
    getValue(obj: Object, path: string) {
        return _.get(obj, path);
    }
	/*---------------------------------------------------------------------------------*/
    onRowClick(item: any) {
        if (this.selectedMap.has(item.id))
            this.selectedMap.delete(item.id);
        else
            this.selectedMap.set(item.id, item);

        this.selected.emit(this.selectedMap);
    }
	/*---------------------------------------------------------------------------------*/
    selectAll() {
        for (let d of this.data) {
            this.selectedMap.set(d.id, d);
        }
        this.selected.emit(this.selectedMap);
    }
	/*---------------------------------------------------------------------------------*/
    unselectAll() {
        this.selectedMap.clear();
        this.selected.emit(this.selectedMap);
    }
	/*---------------------------------------------------------------------------------*/
	onPerPageChanged(pp : number) {
		this.onPageChange.emit({page: this.page.currentPage, itemsPerPage:pp});
	}
	onPageChanged(event: any) {
		this.onPageChange.emit(event);
	}
	/*---------------------------------------------------------------------------------*/
	/*---------------------------------------------------------------------------------*/
	trackByFn(index:any, item:any) {
		//return item.id;
		return index;
	}
}
