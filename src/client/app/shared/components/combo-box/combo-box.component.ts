import { Component, OnInit, Input, OnChanges, SimpleChange, Output, EventEmitter } from '@angular/core';
import { LoggerService } from '../../services/logger.service';


@Component({
    selector: 'combo-box',
    templateUrl: 'combo-box.component.html',
    styleUrls: ['combo-box.component.css']
})
export class ComboBoxComponent implements OnInit, OnChanges {

		// передаваемые из родительского компонента переменные
    @Input() items:any;             // данные для вывода
    @Input() value:string;          // поле для value
    @Input() text:string;           // поле для вывода в option
	@Input() emptyValue:string;     // нименование невыбранного значения
    @Input() model:any;          // ngModel
	@Input() showButtons=true;      // показывать или нет кнопки для редактирования справочника

		//передача событий в родительский компонент
	@Output() onAdd        = new EventEmitter();                            // при нажатии Добавить запись
	@Output() onEdit       = new EventEmitter<{id:string, name:string}>();  // при нажатии Изменить запись
	@Output() onSelectItem = new EventEmitter<{id:string, name:string}>();  // при выборе элемента из списка


    errorMessage: string;

    progress: any;          // текст, выводимы пока данные не загрузятся
    count: number;          // количество элементов в списке
    loaded = false;         // true как тлько данные получены с сервера
    selected = false;       // true если выбран элемент
	selectedIndex = '-1';   // id выбранного элемента
    selectedItem = {id:'-1', name:'<ничего не выбрано>'};     // выбранный элемент

    constructor (
	    private _logger: LoggerService) {}

		// событие создания компонента
    ngOnInit() {
    	return false;
    }


		// событие возникает при изменении данных в компоненте
	ngOnChanges(changes: {[propKey:string]: SimpleChange}) {

		// if (changes.filter && (changes.filter.previousValue !== this.filter && this.filter>0)) {
		// 		// если фильтр изменился и он больше 0
		// 	this.loaded = false;
		// 	this.progress = '  Загрузка...';
		//
		// 	this._logger.debug('filter '+this._service.name+' = '+this.filter);
		// 	this.getItems(this.filter);
		// 	this.selectedIndex = '-1';
		// 	this.selectedItem = {
		// 		id: '-1',
		// 		name: ''
		// 	};
		// 	this.selected = false;
		// } else {
		// 		// если не нужно фильтровать данные
		// 	this._logger.debug('no filter '+this._service.name);
		// 	this.selectedIndex = '-1';
		// 	this.selectedItem = {
		// 		id: '-1',
		// 		name: ''
		// 	};
		// 	this.selected = false;
		// }

		//this._logger.groupEnd();
	}


		// выбор из списка
    onSelect(target:any) {
	    //this._logger.groupEnd();
	    //this._logger.group();

       //  this.selected = (target.value>0);
       //  this.selectedIndex = target.value;
       //  this.selectedItem = {
       //      id: target.value,
       //      name: target.selectedOptions[0].text
       //  };
       //  this._logger.info(this._service.name+' select = '+JSON.stringify(this.selectedItem));
	  // //  this._logger.info(this._service.name+'.selected='+this.selected);
	  //       // передача события в родительский компонент
	  //   this.onSelectItem.emit(this.selectedItem);
    }

		// перезагрузить список
	refresh() {
		// this.loaded = false;
		// this.progress = '  Загрузка...';
		// this._logger.log('(event) refresh '+this._service.name);
		// this.getItems(this.filter);
		// return false;
	}
		// клик по добавить новую запись
	add() {
			// передача события в родительский компонент
		this.onAdd.emit(1);
		return false;
	}
		// клик по изменить текущую запись
	edit() {
		// передача события в родительский компонент
		this.onEdit.emit(this.selectedItem);
		return false;
	}

		// клик по удалить
	del() {
		//this._logger.info('del '+this._service.name);
		return false;
	}

		// изменить текущий выбранный элемент в загруженном списке
	setIndex(i:string) {
		//this._logger.error('set index to '+i);
		this.selectedIndex = i;
		this.onSelectItem.emit(this.selectedItem);
	}

	showData() {
		//console.debug(this._service.name, this.selected, this.selectedIndex, this.selectedItem);
		return false;
	}
}
