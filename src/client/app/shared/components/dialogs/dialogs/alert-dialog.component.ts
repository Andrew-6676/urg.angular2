import { Component }   from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
	moduleId: module.id,
	selector: 'alert-dialog',
	template: `
        <div class="title">{{ title }}</div>
        <div class="body" [innerHtml]="message"></div>
        <div class="footer">
            <button type="button" mat-raised-button (click)="dialogRef.close(true)"><mat-icon>check</mat-icon> OK</button>
        </div>
	`,
	styleUrls: ['dialogs.css'],
})
export class AlertDialog {
	public title: string;
	public message: string;

	constructor(public dialogRef: MatDialogRef<AlertDialog>) {}
}