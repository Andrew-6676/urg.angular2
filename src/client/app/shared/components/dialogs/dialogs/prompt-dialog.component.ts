import { Component }   from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
	moduleId: module.id,
	selector: 'prompt-dialog',
	template: `
        <div class="title">{{ title }}</div>
        <div class="body">
            {{ message }}
            <ng-container *ngIf="type=='mail'">
                <input class="form-control" type="text" placeholder="тема сообщения" [(ngModel)]="data.subject">
	            <br>
                <textarea class="form-control" cols="100" rows="10" placeholder="текст сообщения" [(ngModel)]="data.text"></textarea>
            </ng-container>
	        
        </div>

        <div class="footer">
            <button type="button"
                    mat-raised-button
                    [color]="'warn'"
                    (click)="dialogRef.close(false)"><mat-icon>cancel</mat-icon> Отмена</button>
            <button type="button" 
                    [disabled]="data.text==''"
                    mat-raised-button
                    [color]="'primary'"
                    (click)="dialogRef.close(data)"><mat-icon>send</mat-icon> OK</button>
        </div>
	`,
	styleUrls: ['dialogs.css'],
})
export class PromptDialog {
	public title: string;
	public message: string;
	public type?: string; //'prompt' | 'mail';

	private data = {text:'', subject: ''};

	constructor(public dialogRef: MatDialogRef<PromptDialog>) {}
}