import { Component }   from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
	moduleId: module.id,
	selector: 'confirm-dialog',
	template: `
        <div class="title confirm">{{ title }}</div>
        <div class="body">{{ message }}</div>

        <div class="footer">
            <button type="button" mat-raised-button color="warn"    (click)="dialogRef.close(false)"><mat-icon>cancel</mat-icon> Отмена</button>
            <button type="button" mat-raised-button color="primary" (click)="dialogRef.close(true)"><mat-icon>check</mat-icon> OK</button>
        </div>
	`,
	styleUrls: ['dialogs.css'],
})
export class ConfirmDialog {
	public title: string;
	public message: string;

	constructor(public dialogRef: MatDialogRef<ConfirmDialog>) {}
}