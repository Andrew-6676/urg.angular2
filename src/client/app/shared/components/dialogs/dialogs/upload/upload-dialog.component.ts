import { Component, OnInit, EventEmitter, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormControl }   from '@angular/forms';

import { UploadOutput, UploadInput, UploadFile, humanizeBytes } from 'ngx-uploader';
import { AppService }     from '../../../../services/app.service';
import { LoggerService }  from '../../../../services/logger.service';

@Component({
	moduleId: module.id,
	selector: 'upload-dialog',
	templateUrl: 'upload-dialog.component.html',
	styleUrls: ['../dialogs.css', './upload-dialog.component.css']
})
export class UploadDialog {

	public title: string;
	public category: string;
	public message: string;
	public params: any;

	formData: FormData;
	files: UploadFile[];
	filesData: {[key:string]: any} = {};
	uploadInput: EventEmitter<UploadInput>;
	humanizeBytes: Function;
	dragOver: boolean;
	currentOPGH = 1;
	appLoaded = false;
	result = false;

	constructor(
		// @Inject(DialogsService) private _dialogsService: DialogsService,
		public dialogRef: MatDialogRef<UploadDialog>,
		public _AppService: AppService,
		public _logger: LoggerService,

	) {
		this.files = []; // local uploading files array
		this.uploadInput = new EventEmitter<UploadInput>(); // input events, we use this to emit data to ngx-uploader
		this.humanizeBytes = humanizeBytes;
	}

	/*---------------------------------------------------------------------*/
	onUploadOutput(output: UploadOutput): void {
		if (output.type === 'allAddedToQueue') { // when all files added in queue
			// uncomment this if you want to auto upload files when added
			// const event: UploadInput = {
			//   type: 'uploadAll',
			//   url: '/upload',
			//   method: 'POST',
			//   data: { foo: 'bar' },
			//   concurrency: 0
			// };
			// this.uploadInput.emit(event);
		} else if (output.type === 'addedToQueue'  && typeof output.file !== 'undefined') {
			// add file to array when added
			this.filesData[output.file.id] = {title:'', id_category:-1};
			this.files.push(output.file);
			//this._logger.debug('addedToQueue', output.file);
		} else if (output.type === 'uploading' && typeof output.file !== 'undefined') {
			// update current data in files array for uploading file
			const index = this.files.findIndex(file => typeof output.file !== 'undefined' && file.id === output.file.id);
			this.files[index] = output.file;
			//this._logger.debug('uploading', index, output.file);
		} else if(output.type === 'done' && typeof output.file !== 'undefined') {
			if (output.file.response.status=='ok') {
				this.result = true;
				this._AppService.toastrOpen(output.file.response.message, 'success');

			} else {
				this._AppService.toastrOpen(output.file.response.message, 'error', 0);
			}
		} else if (output.type === 'removed') {
			// remove file from array when removed
			this.files = this.files.filter((file: UploadFile) => file !== output.file);
			//this._logger.debug('removed', output.file, this.files);
		} else if (output.type === 'dragOver') {
			this.dragOver = true;
		} else if (output.type === 'dragOut') {
			this.dragOver = false;
		} else if (output.type === 'drop') {
			this.dragOver = false;
		}
	}



	uploadFile(file: any, params:any): void {
		this._logger.debug(file.name, params);
		const event: UploadInput = {
			type: 'uploadFile',
			file: file,
			url: this.params.url,
			method: 'POST',
			headers: {
				'token': this._AppService.user.token,
				'user': encodeURIComponent(this._AppService.user.login),
				'title': encodeURIComponent(params.title),
				'category': params.id_category,
				'metadata': JSON.stringify({
					'id_unit': {id: this.params.id_unit},
					'id_urg': this.params.id_obj
				})
			},
			data: {
				id_obj: this.params.id_obj,
				id_unit: this.params.id_unit,
				title: params.title,
				category: params.category
			},
			//concurrency: this.formData.concurrency
		};
		this.uploadInput.emit(event);
	}

	uploadAll(): void {
		for (let file of this.files) {
			this.uploadFile(file, this.filesData[file.id]);
		}
	}

	cancelUpload(id: string): void {
		this.uploadInput.emit({ type: 'cancel', id: id });
	}
	cancelAllUpload(): void {
		this.uploadInput.emit({ type: 'cancelAll' });
	}

	removeFile(id: string): void {
		this.uploadInput.emit({ type: 'remove', id: id });
		delete this.filesData[id];
	}

	removeAllFiles(): void {
		this.uploadInput.emit({ type: 'removeAll' });
		this.filesData = {};
	}

	canUploadAll() {
		if (this.files.length==0)
			return false;

		for (let f of this.files) {
			if(this.filesData[f.id].id_category*1<0 || f.progress.status>0 || this.filesData[f.id].title.trim()=='') {
				return false;
			}
		}
		return true;
	}
}