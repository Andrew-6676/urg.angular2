import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule, MatDialogModule, MatIconModule, MatProgressBarModule} from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';

import { DialogsService } from './dialogs.service';

import { ConfirmDialog } from './dialogs/confirm-dialog.component';
import { PromptDialog }  from './dialogs/prompt-dialog.component';
import { AlertDialog }   from './dialogs/alert-dialog.component';
import { WarningDialog } from './dialogs/warning-dialog.component';
import { UploadDialog }  from './dialogs/upload/upload-dialog.component';
import { ErrorDialog }   from './dialogs/error-dialog.component';

import { NgUploaderModule } from 'ngx-uploader';


@NgModule({
	imports: [
		MatDialogModule,
		MatButtonModule,
		MatIconModule,
		MatProgressBarModule,
		NgUploaderModule,
		BrowserModule,

		FormsModule
	],
	exports: [
		ConfirmDialog,
		PromptDialog,
		AlertDialog,
        WarningDialog,
		ErrorDialog,
		UploadDialog,
	],
	declarations: [
		ConfirmDialog,
		PromptDialog,
		AlertDialog,
		WarningDialog,
		UploadDialog,
		ErrorDialog,
	],
	entryComponents: [
		ConfirmDialog,
		PromptDialog,
		AlertDialog,
        WarningDialog,
		UploadDialog,
		ErrorDialog,
	],
	providers: [
		DialogsService,
	],
})
export class DialogsModule { }