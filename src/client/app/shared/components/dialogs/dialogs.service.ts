import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { Observable } from 'rxjs/Rx';

import { ConfirmDialog } from './dialogs/confirm-dialog.component';
import { PromptDialog }  from './dialogs/prompt-dialog.component';
import { AlertDialog }   from './dialogs/alert-dialog.component';
import { WarningDialog } from './dialogs/warning-dialog.component';
import { ErrorDialog }   from './dialogs/error-dialog.component';
import { UploadDialog }  from './dialogs/upload/upload-dialog.component';

@Injectable()
export class DialogsService {

	constructor(private dialog: MatDialog) {}
	/*-------------------------------------------------------------------------------------------------*/
	public confirm(title: string, message: string): Observable<boolean> {

		let dialogRef: MatDialogRef<ConfirmDialog>;

		dialogRef = this.dialog.open(ConfirmDialog);
		dialogRef.componentInstance.title = title;
		dialogRef.componentInstance.message = message;

		return dialogRef.afterClosed();
	}
	/*-------------------------------------------------------------------------------------------------*/
	public prompt(title: string, message: string, type?: string): Observable<boolean> {

		let dialogRef: MatDialogRef<PromptDialog>;

		dialogRef = this.dialog.open(PromptDialog);
		dialogRef.componentInstance.title = title;
		dialogRef.componentInstance.message = message;
		if (type) {
			dialogRef.componentInstance.type = type;
		}

		return dialogRef.afterClosed();
	}
	/*-------------------------------------------------------------------------------------------------*/
	public alert(title: string, message: string): Observable<boolean> {

		let dialogRef: MatDialogRef<AlertDialog>;

		dialogRef = this.dialog.open(AlertDialog);
		dialogRef.componentInstance.title = title;
		dialogRef.componentInstance.message = message;

		return dialogRef.afterClosed();
	}
	/*-------------------------------------------------------------------------------------------------*//*-------------------------------------------------------------------------------------------------*/
	public upload(title: string, message: string, params: any): Observable<boolean> {

		let dialogRef: MatDialogRef<UploadDialog>;

		dialogRef = this.dialog.open(UploadDialog);
		dialogRef.componentInstance.title = title;
		dialogRef.componentInstance.message = message;
		dialogRef.componentInstance.params = params;

		return dialogRef.afterClosed();
	}
	/*-------------------------------------------------------------------------------------------------*/ 
	public warning(title: string, message: string): Observable<boolean> {

		let dialogRef: MatDialogRef<WarningDialog>;

		dialogRef = this.dialog.open(WarningDialog);
		dialogRef.componentInstance.title = title;
		dialogRef.componentInstance.message = message;

		return dialogRef.afterClosed();
	}
	/*-------------------------------------------------------------------------------------------------*/
	public error(title: string, message: string): Observable<boolean> {

		let dialogRef: MatDialogRef<ErrorDialog>;

		dialogRef = this.dialog.open(ErrorDialog);
		dialogRef.componentInstance.title = title;
		dialogRef.componentInstance.message = message;

		return dialogRef.afterClosed();
	}
	/*-------------------------------------------------------------------------------------------------*/
}
