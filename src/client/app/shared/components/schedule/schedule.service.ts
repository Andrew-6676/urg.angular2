import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Service }        from '../../services/service';
import { LoggerService }  from '../../services/logger.service';
import { AppService }     from '../../services/app.service';
import { UrgService }     from '../../services/urg.service';
import { ObjectIterator } from "../../models/object.iterator";

export enum Term {
	year  = 'y',
	month = 'm'
}
export interface GraphCfg {
    term: Term,
    period: Date,
    objects: ObjectIterator,
    groups: Object,
    headers: string[]
}

export interface Graph {
	term: Term,
    period: Date,
	cols: any,
	rows: any,
	headers: any
}
/**
 * This class provides the NameList service with methods to read names and add names.
 */
@Injectable()
export class ScheduleService extends Service {
	public baseUrl = this._AppService.baseURL+'schedules';

	constructor (public _http: HttpClient,
	             public _logger: LoggerService,
	             public _AppService: AppService) {
	             //public _urgService: UrgService) {
		super(_http,
			_logger,
			_AppService);
	}

	/*------------------------------------------------------------------------------------------------*/
	searchMaster(term: string): Observable<any> {
		//return Observable.of([{'name':'Vjhj0jd'}, {'name':'fghru'}]);
		let params = new HttpParams().set('filter[]', 'name=@'+term+'%');
		return this.getData('http://urg.backend/urgmasters/search', params);
	}
	/*------------------------------------------------------------------------------------------------*/
    /**
     * @param {GraphCfg} cfg - Параметры для построения графика.
     * @generateGraph
     */
    generate(cfg: GraphCfg): Graph {
        // формируем данные для построения графика
        this._logger.debug('start generate graph');

        let cols = new ObjectIterator();

        // заголовки столбцов
        switch (cfg.term) {
            case Term.month:
                // получаем количество дней в месяце
                let dim: number = new Date(cfg.period.getFullYear(), cfg.period.getMonth()+1, 0).getDate();
                for (let d=1; d<=dim; d++) {
                    cols[d]       = new Object(null);
                    cols[d].id    = d;
                    cols[d].name  = d;
                    cols[d].date  = new Date(cfg.period.getFullYear(), cfg.period.getMonth(), d, 12).toISOString().substr(0,10);
                    cols[d].month = new Date(cfg.period.getFullYear(), cfg.period.getMonth(), d, 1).toISOString().substr(0,10);
                    cols[d].data  = 'sdfdsf';
                }
                break;
            case Term.year:
                let  arr = ['Январь','Февраль',
                            'Март','Апрель','Май',
                            'Июнь','Июль','Август',
                            'Сентябрь','Октябрь','Ноябрь',
                            'Декабрь'];
                for (let d=0; d<=11; d++) {
                    cols[d]       = new Object(null);
                    cols[d].id    = d+1;
                    cols[d].name  = arr[d];
	                cols[d].date  = new Date(cfg.period.getFullYear(), d, 1, 12).toISOString().substr(0,10);
	                cols[d].month = new Date(cfg.period.getFullYear(), d, 1, 12).toISOString().substr(0,10);
                    cols[d].data  = null;
                }
                break;
            default:
        }

        // данные для строк
        let rows = new ObjectIterator();
        // цикл по списку объектов (УРГ)
        for (let o in cfg.objects) {
            // отслеживаем смену группы объектов
            if (!rows[cfg.objects[o].id_group]) {
                rows[cfg.objects[o].id_group] = [];
            }
            // формируем коллекцию для заголовков строк вида {<id_group>:{[object1,objectN...]}}
            rows[cfg.objects[o].id_group].push(cfg.objects[o]);
        }

        return {
            term: cfg.term,
            period: cfg.period,
            cols: cols,
            rows: rows,
            headers: cfg.headers
        };

    }
	/*------------------------------------------------------------------------------------------------*/
	// addWork() {
	// 	this._logger.debug('add work resp =');
	// }
    /*------------------------------------------------------------------------------------------------*/
    validate() {}
    /*------------------------------------------------------------------------------------------------*/
    load(id: number) {
	    return this.getData(this.baseUrl+'/'+<any>id);
    }
    /*------------------------------------------------------------------------------------------------*/
    getObjectsData() {
	    //return this.getData(this.baseUrl+'/'+<any>id);
    }
    /*------------------------------------------------------------------------------------------------*/
    getSavedList() {
	    let params = new HttpParams().set('fields', 'id, type, title, period, author, id_unit, edit_date');
	    return this.getData(this.baseUrl, params);
    }
	/*------------------------------------------------------------------------------------------------*/
	saveToDb(data: any): Observable<any> {
		return this.saveData(this.baseUrl, data);
	}
	/*------------------------------------------------------------------------------------------------*/
	/*------------------------------------------------------------------------------------------------*/
	loadWorksCfg() {
		return this.getData(this.baseUrl+'/getWorksCfg');
	}
	/*------------------------------------------------------------------------------------------------*/
	saveWorksCfg(data: any) {
		return this.saveData(this.baseUrl+'/saveWorksCfg', data);
	}
	/*------------------------------------------------------------------------------------------------*/
	getPreparedData(body: any) {
		let options = { headers: this.head, withCredentials: true };
		//let body = JSON.stringify(data);
		let resp: any;
		resp = this._http.post(this.baseUrl+'/prepareData', body, options);

		return resp.map(this.extractData).catch(this.handleError);
	}
	/*------------------------------------------------------------------------------------------------*/
}