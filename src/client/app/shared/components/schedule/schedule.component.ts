import { Component, Input } from '@angular/core';
import { MatTabGroup }      from '@angular/material';
import { ViewChild }        from "@angular/core";

import { ObjectIterator } from '../../models/object.iterator';

import { AppService }             from '../../services/app.service';
import { LoggerService }          from '../../services/logger.service';
import { DialogsService }         from "../dialogs/dialogs.service";
import { ScheduleDialogsService } from "./dialogs/schedule-dialogs.service";

import { ScheduleService, Term, GraphCfg, Graph } from './schedule.service';

import * as _ from "lodash";


@Component({
	moduleId: module.id,
	selector: 'u-schedule',
	templateUrl: 'schedule.component.html',
	styleUrls: ['schedule.component.css'],
})
export class ScheduleComponent {
	@ViewChild('tabs') tabs: MatTabGroup;
		// все данные для графика получаем "из-вне"
	@Input() id_unit: any = "0";        		// Подразделение
	@Input() groups: {[key: string]:any};		// группы объектов
    @Input() objects = new ObjectIterator();	// объекты
    @Input() obj_headers: string[] = [];	    // заголовки столбцов
    @Input() works :  {[key: string]:any} =  new ObjectIterator();		// работы, на которые составляется график

	Term = Term;

		// структура, из которой будет строится график
    graph: Graph = {
		term: null,
		period: null,
		cols: null,
		rows: null,
		headers: null
	};
	id: number = -1;
    title: string = '';
    author: string = '';

		// данные из ячеек графика
	cellsData: Map<string, Set<number>> = new Map();

	/*
	*
	*  {
	*  <row.id>: {<col.id>: [<data>]}
	*  }
	*
	*
	*
	* */

	mouseOver = {r:0, c:0}; 					// над какой ячейкой в данный момент курсор
	checked: Set<string> = new Set();			// множество выделенных ячеек - каждый элемент - <row.id>_<col.id>

		// компоненты формы настройки графика
    graphic_term: Term = Term.month;
	date1: Date = new Date();
	graphic_date = new Date();
	selectedWork = 1;

    datepickerOpts: any = {
        placeholder: 'Выберите дату',
        //assumeNearbyYear: true,
        startView: 'months',
        minViewMode: 'months',
        format: 'MM yyyy',
        //format: 'dd.mm.yyyy',
        weekStart: 1,
        todayHighlight: true,
        todayBtn: 'linked',
        language: 'ru',
        daysOfWeekHighlighted: [0,6],
        autoclose: true,
        icon: 'glyphicon glyphicon-calendar'
    };

    scheduleList: any[] = [];

    /*------------------------------------------------------------------------------------------------*/
	constructor(
		public _AppService: AppService,
		public _logger: LoggerService,
		public _scheduleService: ScheduleService,
		public _scheduleDialogs: ScheduleDialogsService,
		public _dialogService: DialogsService,
	) {
		this.getScheduleList();
	}
	/*------------------------------------------------------------------------------------------------*/
		// добавить объект в построенный график
	addObject(obj:any) {}
	/*------------------------------------------------------------------------------------------------*/
	// удалить объект из построенного графика
	deleteObject(obj:any) {}
	/*------------------------------------------------------------------------------------------------*/
	/*------------------------------------------------------------------------------------------------*/
	insertWork() {};
	/*------------------------------------------------------------------------------------------------*/
	removeWork() {}
	/*------------------------------------------------------------------------------------------------*/
	/*------------------------------------------------------------------------------------------------*/
		// сгенерировать структуру для построения графика (для вывода пустой таблицы, без работ)
    generateBtnClick() {
	    this.checked.clear();
	    this.graph = this._scheduleService
		    .generate({
			    term: this.graphic_term, 	    // месяц/год
			    period: this.graphic_date,  		    // на какой пиреиод строить график
			    objects: this.objects,		    // ГРП/УРГ
			    groups: this.groups,			// группы для объектов
			    headers: this.obj_headers 		// заголовки столбцов
		    });

        this._logger.debug('generated graph', this.graph);
	}
	/*------------------------------------------------------------------------------------------------*/
		// расставление работ в ячейки графика
	autoBuildBtnClick() {
		this._logger.debug('inserted works', this.cellsData);
		// так выглядит срока в графике:
		// this.objects['4442329'] = {
		// 	'id': 4442329,
		//  'id_type_urg': 1005,
		// 	'id_group': 1,
		// 	'name': 'КРД-31',
		// 	'data': [
		// 		'г. Витебк, ул.вапвапвап, 2',
		// 		'РДС-32',
		// 		'среднее',
		// 	],
		// 	'works': {
		// 		1184: [new Date('2017-12-12')],
		//      .....
		// 	}
		// };
		//  колонки:
		// "cols": {
		// 	"1": {
		// 		"id": 1,
		// 			"name": 1,
		// 			"data": "sdfdsf"
		// 	},
		// 	"2": {
		// 		"id": 2,
		// 			"name": 2,
		// 			"data": "sdfdsf"
		// 	},
		// .....
		//  настройки работ по объектам:
		// {
		// 	"1001": {
		// 	"1184": {
		// 		"p": "m",
		// 			"l": 1
		// 	},
		// .....

		/*
		 есть объекты (objects, строки в таблице), по каждому знаем дату последнего выполенния нужной работы
		 есть колонки таблицы (cols) - дни месяца/месяцы
		 что с этим делать?
		 выбираем вид работ. получаем по ней настройки, пепебираем объекты, смотрим последнюю дату по выбранной работе и вычисляем куда вставить новую работу
		* */

		this.cellsData.clear();
		this._logger.debug('###############################################################');
		this._logger.styledLog('%c START BUILD SCHEDULE:', 'background: #f00; color: #fff; font-weight: bold');

		let works: number[] = [1185,1186,1187,1188];
		for (let selectedWork of works) {
			this._logger.styledLog('%c Processed work:', 'background: none; color: #00f; font-weight: bold', selectedWork);
			for (let oid in this.objects) {
				this._logger.debug('obj:', this.objects[oid].name, '(' + this.objects[oid].id_type_urg + ')');
				let col = new Date(this.graphic_date.getFullYear(), this.graphic_date.getMonth()+1, 1).toISOString().substr(0,10);
				// если по ткущему объекту такая работа была
				if (this.objects[oid].works && this.objects[oid].works.hasOwnProperty(selectedWork)) {
					let d = this.objects[oid].works[selectedWork][0].toISOString().substr(0,10);
					col = d;
				} else {

				}
				this._logger.debug('----- add work:', oid + '_' + col, ' to ', selectedWork);
				let tmp_cell = this.cellsData.get(oid + '_' + col);
				if (tmp_cell)
					tmp_cell.add(selectedWork);
				else
					tmp_cell = new Set().add(selectedWork);
				this.cellsData.set(oid + '_' + col, tmp_cell);
			}
		}
		this._logger.styledLog('%c END BUILD SCHEDULE:', 'background: #009933; color: #fff; font-weight: bold');
	};
	/*------------------------------------------------------------------------------------------------*/
	clearSchedule() {
		this._dialogService
			.confirm('','Очистить все ячейки в графике?')
			.subscribe(
				(resp:boolean) => {
					if (resp) {
						this.cellsData.clear();
					}
				}
			);
	}
	/*------------------------------------------------------------------------------------------------*/
	totalClearSchedule() {
		this._dialogService
			.confirm('','Очистить содержимое графика?')
			.subscribe(
				resp => {
					if (resp) {
						this.id = -1;
						this.title = '';
						// this.author = '';
						this.cellsData.clear();
						this.graph = {
							term: null,
							period: null,
							cols: null,
							rows: null,
							headers: null
						};
					}
				}
			);
	}
	/*------------------------------------------------------------------------------------------------*/
	row_num(i: number, j:number):number {
		return 1;
	}
	/*------------------------------------------------------------------------------------------------*/
	isChecked(r: any, c: any) {
		return this.checked.has(r.id+'_'+c.date);
	}
	/*------------------------------------------------------------------------------------------------*/
	onCellClick(r: any, c: any) {
		this._logger.debug('cell click =', r,c);
		if (this.checked.has(r.id+'_'+c.date))
			this.checked.delete(r.id+'_'+c.date);
		else
			this.checked.add(r.id+'_'+c.date);

		//this._logger.debug(this.checked);
		// r.checked = !r.checked;
		// c.checked = !c.checked;
	}
		// добавляем работу в одну ячейку
	/*------------------------------------------------------------------------------------------------*/
    onCellDblClick(r: any, c: any) {
	    // let a = {
	    // 	"4440892": {
	    // 		'02.01.2018': [1185,1186],
	    // 		'05.01.2018': [1185],
	    // 	},
	    // };
	    this._scheduleDialogs
		    .editworkDialog('Добавит работу', this.works)
		    .subscribe(
			    resp => {
					if (resp !== false) {
						let work: number = <any>resp*1;
						let tmp = this.cellsData.get(r.id+'_'+c.date);
						if (tmp)
							tmp.add(work);
						else
							tmp = new Set().add(work);
						this.cellsData.set(r.id+'_'+c.date, tmp);
					}
			    }
		    )
	}
		// Добавляем работу во все выделенные яччейки
	/*------------------------------------------------------------------------------------------------*/
	onKeyPress(event: any) {
		this._logger.debug('key pressed:', event);
	}
	/*------------------------------------------------------------------------------------------------*/
	addWorksToSelectedCells() {
		this._scheduleDialogs
			.editworkDialog('Добавит работу', this.works)
			.subscribe(
				resp => {
					if (resp !== false) {
						this._logger.debug('selected work for checked cells:', resp);
						let work: number = <any>resp*1;
						this.checked.forEach((cell_id) => {
							let tmp = this.cellsData.get(cell_id);
							if (tmp)
								tmp.add(work);
							else
								tmp = new Set().add(work);
							this.cellsData.set(cell_id, tmp);
						});
					}
				}
			)
	}
	/*------------------------------------------------------------------------------------------------*/
	onObjectClick(r: any) {
		this._logger
			.debug('object click =', r);
	}
	/*------------------------------------------------------------------------------------------------*/
	onCellMouseOver(r: any, c:any) {
		this.mouseOver = {r: r, c: c};
		//this._logger.debug('cell mouse over =', this.mouseOver);
	}
	/*------------------------------------------------------------------------------------------------*/
	checkHoliday(d: number) {
		if (this.graph.term==Term.year) return false;
			// проверяем дату на выходной
		let dd = new Date(this.graph.period);
		dd.setDate(d);
		return dd.getDay()==0 || dd.getDay()==6;
	}
	/*------------------------------------------------------------------------------------------------*/
	// getDayInMonth(date: Date): number {
	// 	return new Date(date.getFullYear(), date.getMonth()+1, 0).getDate();
	// }
	/*------------------------------------------------------------------------------------------------*/
	/*------------------------------------------------------------------------------------------------*/
	/*------------------------------------------------------------------------------------------------*/
	getScheduleList() {
		this._scheduleService
			.getSavedList()
			.subscribe(
				resp => {
					this.scheduleList = resp;
					this._logger.debug('loaded schedules list:', resp);
				}
			);
	}
	/*------------------------------------------------------------------------------------------------*/
	loadSchedule(item: any) {
		this._dialogService
			.confirm('Загрузка', 'Загрузить график "'+item.title+'"')
			.subscribe(
				resp => {
					if (resp) {
						this._scheduleService
							.load(item.id)
							.subscribe(
								resp => {
									this._AppService.toastrOpen('Данные графика загружены', 'info');
									this._logger.debug('loaded schedule:', resp);
									this.tabs.selectedIndex = 0;
									this.id           = resp.id;
									this.author       = resp.data.author;
									this.title        = resp.data.title;
									this.date1        = new Date(resp.data.period);
									this.graphic_date = new Date(resp.data.period);
									this.graphic_term = resp.data.type;

									this.objects     = resp.data.objects;
									this.groups      = resp.data.groups;
									this.obj_headers = resp.data.obj_headers;

									//  term: this.graphic_term, 	    // месяц/год
									// 	period: this.date1,  		    // на какой пиреиод строить график
									// 	objects: this.objects,		    // ГРП/УРГ
									// 	groups: this.groups,			// группы для объектов
									// 	headers: this.obj_headers 		// заголовки столбцов
									this.generateBtnClick();

									// cellsData: Map<string, Set<number>> = new Map();
									this.cellsData = new Map();
									for (let cell in resp.data.cellsData) {
										this.cellsData.set(cell, new Set(resp.data.cellsData[cell]));
									}
								},
								error => {
									this._AppService.toastrOpen('Ошибка загрузки данных графика', 'error', 0);
								}
							);
					}
				}
			);
		return false;
	}
	/*------------------------------------------------------------------------------------------------*/
	saveScheduleToDb() {
		if (this.title  == '') this.title = this.getPeriod({period: this.graphic_date});
		if (this.author == '') this.author = this._AppService.user.name;

		this._scheduleDialogs
			.saveScheduleDialog(this.title, this.author, (this.id<0 ? 'Сохранить' : 'Пересохранить'))
			.subscribe(
				resp => {
					if (resp !== false) {
						this.author = resp.author;
						this.title = resp.title;

						this._logger.debug('save schedule', resp);
						let objects_arr = [];
						for (let o in this.objects) {
							objects_arr.push(this.objects[o].id);
						}
						//this._logger.debug('--------------------------obj ids ', objects_arr );
						let cells_data: {[key: string]: any} = {};
						this.cellsData.forEach((value, key) => {
							let arr: any[] = [];
							value.forEach(value => { arr.push(value) });
							cells_data[key] = arr;
						});
						//this._logger.debug('--------------------------cells dara ', cells_data );

						let data = {
							id: this.id,
							type: this.graphic_term,
							title: this.title,
							objects: JSON.stringify(objects_arr),
							period: this.graphic_date.toISOString().substr(0,10),
							data: JSON.stringify(cells_data),
							id_unit: this.id_unit,
							author: this.author
						};

						//this._logger.debug('-------------------------- data to save', data);
						this._scheduleService
							.saveToDb(data)
							.subscribe(
								resp => {
									if (resp.status=='ok') {
										this._AppService.toastrOpen(resp.message,'success');
										this.id = resp.id;
										this.scheduleList.push({
												id: this.id,
												type: this.graphic_term,
												title: this.title,
												period: this.graphic_date.toISOString().substr(0,10),
												id_unit: this.id_unit,
												author: this.author
										});
									} else {
										this._AppService.toastrOpen(resp.message,'error', 0);
									}
								},
								error => {
									this._AppService.toastrOpen(<any>error,'error', 0);
								}
							);
					}
				}
			);
	}
	/*------------------------------------------------------------------------------------------------*/
	deleteSavedSchedule(item: any) {
		this._dialogService
			.confirm('Удаление','Подтвердите удаление "'+item.title+'"')
			.subscribe(
				resp => {
					if (resp) {
						this._scheduleService
							.delete(item.id)
							.subscribe(
								resp => {
									if (resp.status=='ok') {
										_.remove(this.scheduleList, (e:any) => {return e.id == item.id});
									}
								}
							);
					}
				}
			);
	}
	/*------------------------------------------------------------------------------------------------*/
	/*------------------------------------------------------------------------------------------------*/
	/*------------------------------------------------------------------------------------------------*/
	/*------------------------------------------------------------------------------------------------*/
	getPeriod(item: any) {
		let m_arr = ['Янв','Фев',
			'Мар','Апр','Май',
			'Июн','Июл','Авг',
			'Сен','Окт','Ноя',
			'Дек'];
		let d = new Date(item.period);
		if (item.type && item.type=='y') {
			return d.getFullYear()+' год';
		} else {
			return m_arr[d.getMonth()+1]+' '+d.getFullYear();
		}
	}
	/*------------------------------------------------------------------------------------------------*/

}
