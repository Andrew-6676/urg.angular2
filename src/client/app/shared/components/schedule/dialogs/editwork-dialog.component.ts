import { Component }    from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
	moduleId: module.id,
	selector: 'editwork-dialog',
	template: `
        <div class="title">{{ title }}</div>
        <div class="body">
	        <div>
		        <mat-nav-list>
			        <mat-list-item *ngFor="let item of worksList">
				        <a matLine href="#" (click)="selectItem(item)">
					        <span style="display: inline-block; border: 1px solid #bcbcbc; width: 20px; height: 20px; border-radius: 2px;" [style.background]="item.color">&nbsp;</span>
					        {{ item.name }}
				        </a>
			        </mat-list-item>
		        </mat-nav-list>
	        </div>
        </div>
        <div class="footer">
            <button type="button" color="warn" mat-raised-button (click)="dialogRef.close(false)"><mat-icon>cancel</mat-icon>Отмена</button>
            <button type="button" 
                    [disabled]="!result"
                    color="primary" 
                    mat-raised-button 
                    (click)="dialogRef.close(result)"><mat-icon>check</mat-icon>Ok</button>
        </div>
	`,
	styleUrls: ['dialogs.css'],
})
export class EditworkDialog {
	public title: string;
	public worksList: {[key: string]:any};

	private result: any = false;

	constructor(public dialogRef: MatDialogRef<EditworkDialog>) {}

	selectItem(item: any) {
		this.dialogRef.close(item.id);
		return false;
	}
}