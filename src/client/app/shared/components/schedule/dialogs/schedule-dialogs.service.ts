import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { Observable } from 'rxjs/Rx';

import { EditworkDialog }     from './editwork-dialog.component';
import { SaveScheduleDialog } from './save-dialog.component';

@Injectable()
export class ScheduleDialogsService {

	constructor(private dialog: MatDialog) {}
	/*-------------------------------------------------------------------------------------------------*/
	public editworkDialog(title: string, worksList: {[key: string]:any} ): Observable<boolean> {

		let dialogRef: MatDialogRef<EditworkDialog>;

		dialogRef = this.dialog.open(EditworkDialog);
		dialogRef.componentInstance.title = title;
		dialogRef.componentInstance.worksList = worksList;

		return dialogRef.afterClosed();
	}
	/*-------------------------------------------------------------------------------------------------*/
	public saveScheduleDialog(title: string, author: string, dialog_title?: string): Observable<any> {

		let dialogRef: MatDialogRef<SaveScheduleDialog>;

		dialogRef = this.dialog.open(SaveScheduleDialog);
		dialogRef.componentInstance.title = title;
		dialogRef.componentInstance.author = author;

		if (dialog_title)
			dialogRef.componentInstance.dialog_title = dialog_title;

		return dialogRef.afterClosed();
	}
	/*-------------------------------------------------------------------------------------------------*/
}
