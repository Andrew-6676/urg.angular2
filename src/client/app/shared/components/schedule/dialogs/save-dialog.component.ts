import { Component }    from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
	moduleId: module.id,
	selector: 'save-dialog',
	template: `
        <div class="title">{{ dialog_title }}</div>
        <div class="body">
	        <label for="inp">Описание графика</label>
	        <input type="text" id="inp" class="form-control" [(ngModel)]="title">
	        <br>
	        <label for="aut">Автор</label>
	        <input type="text" id="aut" class="form-control" [(ngModel)]="author">
        </div>
        <div class="footer">
	        <button type="button" mat-raised-button color="warn"    (click)="dialogRef.close(false)"><mat-icon>cancel</mat-icon> Отмена</button>
            <button type="button" mat-raised-button color="primary" (click)="dialogRef.close({title:title, author:author})"><mat-icon>check</mat-icon> Сохранить</button>
        </div> 
	`,
	styleUrls: ['dialogs.css'],
})
export class SaveScheduleDialog {
	public dialog_title: string = 'Сохранить';
	public title:   string;
	public author:  string;

	constructor(public dialogRef: MatDialogRef<SaveScheduleDialog>) {}
}