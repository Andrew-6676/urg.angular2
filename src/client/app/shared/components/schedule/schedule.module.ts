import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule}                  from '@angular/common';
import {FormsModule}                   from '@angular/forms';

import {MatButtonModule}       from '@angular/material/button';
import {MatListModule}         from '@angular/material/list';
import {MatIconModule}         from '@angular/material/icon';
import {MatTooltipModule}      from '@angular/material/tooltip';
import {MatTabsModule}         from '@angular/material/tabs';
import {MatProgressBarModule}  from '@angular/material/progress-bar';
import {MatCheckboxModule}     from '@angular/material/checkbox';
// import { MatCardModule }            from '@angular/material/card';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {MatToolbarModule}      from '@angular/material/toolbar';
import {MatExpansionModule}    from '@angular/material/expansion';
import {MatButtonToggleModule} from '@angular/material/button-toggle';

import {SharedModule}     from '../../shared.module';
import {NKDatetimeModule} from '../ng2-datetime/ng2-datetime.module';

import {ScheduleService}        from './schedule.service';
import {ScheduleDialogsService} from './dialogs/schedule-dialogs.service';
import {EditworkDialog}         from './dialogs/editwork-dialog.component';
import {SaveScheduleDialog}     from './dialogs/save-dialog.component';

import {ScheduleComponent} from './schedule.component';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		SharedModule,

		MatListModule,
		MatButtonModule,
		MatButtonToggleModule,
		MatIconModule,
		MatTooltipModule,
		MatTabsModule,
		MatProgressBarModule,
		MatCheckboxModule,
		MatToolbarModule,
		MatExpansionModule,

		NKDatetimeModule,
	],
	declarations: [
		ScheduleComponent,
		EditworkDialog,
		SaveScheduleDialog
	],
	exports: [
		ScheduleComponent,
        EditworkDialog,
		SaveScheduleDialog
	],
    entryComponents : [
        EditworkDialog,
	    SaveScheduleDialog
	],
	providers: [
		ScheduleService,
		ScheduleDialogsService
	]
	//providers: [AppService]
})
export class ScheduleModule {
}
