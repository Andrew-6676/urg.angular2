import { Component } from '@angular/core';
declare let $:any;

@Component({
	moduleId: module.id,
    selector: 'scrolling-buttons',
    template: `<div class="scrolling-button__container-outer {{ pos }}">
	<div class="scrolling-button__container">
		<div class="scrolling-button__container-inner">
			<a href="#">
				<span class="scrolling-button scrolling-button_up js-scrolling-button js-scrolling-button-up scrolling-button_visible" 
					  (click)="up()"></span>
			</a>
			<a href="#">
				<span class="scrolling-button scrolling-button_down js-scrolling-button js-scrolling-button-down scrolling-button_visible" 
					  (click)="down()"></span>
			</a>
		</div>
	</div>
</div>`,
    styleUrls: ['scrolling-buttons.component.css'],
})

export class ScrollingButtonsComponent {

	t: any;
	pos: string = '';

	/*------------------*/
	up() {
		// var top = Math.max(document.body.scrollTop,document.documentElement.scrollTop);
		// if(top > 0) {
		// 	window.scrollBy(0,-100);
		// 	this.t = setTimeout(()=>{ this.up(); }, 15);
		// } else clearTimeout(this.t);

		$('html, body').animate({scrollTop: 0},500);
		return false;
	}

	down() {
		var height = $('body').height();
		$('html, body').animate({scrollTop: height},500);
		return false;
	}
}

