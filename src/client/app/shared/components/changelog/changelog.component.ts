import {Component, Input} from '@angular/core';

import { AppService }     from '../../services/app.service';
import { LoggerService }  from '../../services/logger.service';
import { DialogsService } from '../dialogs/dialogs.service';
import { ObjectIterator } from '../../models/object.iterator';


/**
 * This class represents the navigation bar component.
 */
@Component({
	moduleId: module.id,
	selector: 'change-log',
	templateUrl: 'changelog.component.html',
	styleUrls: ['changelog.component.css'],
})

export class ChangelogComponent {
	@Input() opened = false;
	log: any[] = [];

	inFuture = [
		{type:'add', text:'Печать паспорта узла учета'},
		{type:'add', text:'Печать режимной карты'},
		{type:'add', text:'Поиск по УРГ'},
		{type:'add', text:'Добавление к графику работ: ТО, ТР, ПН'},
		{type:'add', text:'Выдача рапорта, наряда-задания'},
	];

	constructor(
		public _AppService: AppService,
	    public _dialogs: DialogsService,
	    private _logger: LoggerService
	) {

		// this.log.push ({
		// 	'version': '0.6.0',
		// 	'date': '01.02.2018',
		// 	'content':[
		// 		{type: 'add', text: ''},
		// 	]}
		// );

		this.log.push (
			{
				'version': '0.5.0',
				'date': '01.02.2018',
				'content':[
					{type: 'add', text: 'Экспериментальная версия построения графиков обходов'},
					{type: 'fix', text: 'Изменён способ редактирвоания ответственного за эксплуатацию'},
				],
				// 'subversions': [
				// 	{
				// 		'version': '0.5.1',
				// 		'date': '01.12.2017',
				// 		'content':[
				// 			{type: 'add', text: 'Экспериментальная версия построения графиков обходов'},
				// 		]
				// 	}
				// ],
			}
		);

		this.log.push({
			'version': '0.4.0',
			'date': '31.10.2017',
			'content': [
				{type: 'add', text: 'Обновлён интерфейс'},
				{type: 'add', text: 'Добавлена индикация загрузки данных'},
				{type: 'add', text: 'Добавлена возможность сортировки и филтрации списка УРГ по некоторым полям'},
				{type: 'add', text: 'Добавлена возможность именения порядка оборудования перетаскиванием'},
				{type: 'add', text: 'Добавлена возможность прикрепления файлов к КИП'},
				{type: 'add', text: 'Добавлена статистика по количеству объектов (на главной странице)'},
				{
					type: 'add',
					text: 'Добавлена возможность отправить сообщение разработчику прямо из программы (из меню справа сверху) (необходимо php >=5.6 и настрока бэкэнда)'
				},
				{type: 'add', text: 'Добавлен ввод данных режимной карты (печать в следующей версии)'},
				{type: 'add', text: 'Частично заполнена контекстная справка'},
				{type: 'add', text: 'Добавлена возможность указать пломбу к счётчику'},
				{
					type: 'add',
					text: 'В контекстное меню УРГ добавлен пункт "Открыть на Панораме", так же перейти на панораму можно из редактирования/просмотра УРГ'
				},
				{type: 'add', text: 'В режиме просмотра показываются прикреплённые файлы к оборудованию'},
				{type: 'add', text: 'Реализовано ведение лога операций с БД'},
				{
					type: 'change',
					text: 'Изменение в поле "Ответственный за эксплуатацию" - можно вводить должность и дату, будет хранится история смены'
				},
				{type: 'change', text: 'Доработан механизм добавления в кольцо'},
				{type: 'fix', text: 'Исправлены недочёты в прикреплении файлов, сохранении адреса'},
			]}
		);

		this.log.push ({
			'version': '0.3.0',
			'date': '09.03.2017',
			'content':[
				{type: 'add', text: 'Базовый функционал'},
			]}
		);




	};
}
