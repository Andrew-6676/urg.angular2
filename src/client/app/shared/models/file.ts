export class File {
	public id: string;
	public title: string;
	public id_category: number = 0;
	public ext: string;
	public exist: boolean;
	public link: string;
}
