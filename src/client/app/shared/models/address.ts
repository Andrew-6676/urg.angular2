import { List } from './list';

export class Address {
	[key: string]: any;

	public id: string = '-1';
	public region: List = new List();
	public np: List = new List();
	public street: List = new List();
	public house: string = null;
	public building: string = null;
	public flat: string = null;
}
