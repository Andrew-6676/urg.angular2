export class UnitList {
    constructor (
        public id :number,
        public name :string,
        public subUnit? :UnitList) { }
}
