export class Room {
	public id: number = -1;
	public id_urg: string = '-1';
	public id_type: number = -2;
	public area: number = 0;
	public descr: string = null;
}
