import { File }  from './file';

export class Instrumentation {
	// public mount_point: string = '';
	// public schema_number: string = null;
	// public id_manufacturer: number = -2;
	// public diameter: number = 0;
	// public year_release: number = 0;
	// public date_install: Date = new Date();
	public serial_number: string = null;
	public id: string = "-1";
	public id_type: number = -2;
	public id_make = -2;
	public quantity: number = 1;
	public descr: string = null;

	public files: File[] = [];
}
