export class ObjectIterator {
	[key: string]: any;

	[Symbol.iterator] = function () {
		let keys = Object.keys(this);
		let self = this;
		let i = 0;
		return {
			next() {
				return {
					value: self[keys[i]],
					done: i++ >= keys.length
				};
			}
		}
	};

	// [Symbol.add] = function add(obj: any) {
	//     for (let o in obj) {
	//         this[o] = obj[o];
	//     }
	// };
}
