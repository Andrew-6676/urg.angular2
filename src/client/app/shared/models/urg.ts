import { List }      from './list';
import { Gps }       from './gps';
import { File }      from './file';
import { Room }      from './room';
import { Equipment } from './equipmen';
import { Address }   from './address';

export class Urg {
	public id_obj: string = '-1';
	public type_urg: number = -2;
	public num_unique: string = '0';
	public num: number = 0;
	public num_archiv: string = '-';
	public num_inventory: string = '-';
	public address_descr: string = null;
	public id_ring: number = null;
	public date_acceptance: Date = new Date();
	public date_starting: Date = new Date();
	public gps: Gps = new Gps();
	public telemetry: boolean = false;
	public lighting_protection: List = new List();
	public lighting_protection_resistance: number = null;
	public communication: List = new List();
	public floor_material: List = new List();
	public barrier: boolean = false;
	public extinguishing: List = new List();
	public descr: string = null;
	public address: Address = new Address();
	public type: List = new List();
	public building_type: List = new List();
	public manufacturer: List = new List();
	public heating: List = new List();
	public power_supply: List = new List();
	public owner: List = new List();
	public files: File[];
}

export class UrgTable {
	public id: string = '-1';
	public id_type_urg: number = -2;
	public num_unique: number = 0;
	public id_unit: number = -1;
	public num: string = '0';
	public num_archiv: string = null;
	public num_inventory: string = null;
	public num_inventory_eq: string = null;
	public address_descr: string = null;
	public id_ring: number = null;
	public date_acceptance: Date = null;
	public date_starting: Date = null;
	public date_startup_gas: Date = null;
	public gps: Gps = new Gps;
	public id_telemetry: number = -1;
	public id_building_type: number = -2;
	public id_power_supply: number = -2;
	public id_light: number = -2;
	public id_manufacturer: number = -2;
	public id_lighting_protection: number = -2;
	public lighting_protection_resistance: string = null;
	public id_heating: number = -2;
	public id_communication: number = -2;
	public id_floor_material: number = -2;
	public id_owner: number = -2;
	public owner: List = new List();
	public barrier: boolean = false;
	public id_extinguishing: number = -2;
	public id_ventilation: number = -2;
	public dist_enter: number = null;
	public dist_exit: number = null;
	public master: {
		id: number,
        id_urg: any,
		date: Date,
		name: string,
		post: string
	} = {
		id: -1,
        id_urg: null,
		date: new Date(),
		name: '- Введите ФИО -',
		post: ' - должность - '
	};
	public mastersHistory: {
        id: number,
        id_urg: any,
        date: Date,
        name: string,
        post: string
    }[] = [];
	public descr: string = null;
	public address: Address = new Address();
	public files: File[] = [];
	public rooms: Room[] = [];
	public equipment: Equipment[] = [];
	public ring: any[] = [];
	public calc_capacity: number = null;
	public pressure_in_1: number = null;
	public pressure_in_2: number = null;
	public pressure_in_3: number = null;
	public pressure_in_4: number = null;
	public pressure_out_1: number = null;
	public pressure_out_2: number = null;
	public pressure_out_3: number = null;
	public pressure_out_4: number = null;
	public refbookData?: any;
	public lastwork: Date = null;
}
