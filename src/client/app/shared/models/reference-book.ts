export class ReferenceBookShort {
	public id: number;
	public name: string;
}
export class ReferenceBookFull {
	public id: number;
	public parent: number;
	public name: string;
}
