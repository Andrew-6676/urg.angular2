import { File }  from './file';

export class Equipment {
	public id: string = "-1";
	public id_eq_type: number = -2;
	// public diameter: number = 0;
	// public year_release: number = 0;
	// public date_install: Date = new Date();
	// public lifetime: number = 0;
	id_make = -99;
	//public id_manufacturer: number = -2;
	public mount_point: string = '';
	public prev_mnt: string = '';
	//public quantity: number = 1;
	public schema_number: string = null;
	public descr: string = null;
	public files: File[] = [];
	public data: any = {
		date_starting: null,
		year_release: null,

		id_type:  -2,
		id_model: -2,
		id_manufacturer: -2,

		type: {},
		make: {},
		model: {},
		manufacturer: {id:-2, manufacturer:null},
		serial_number: '-',
		diameter: {}
	};
}
