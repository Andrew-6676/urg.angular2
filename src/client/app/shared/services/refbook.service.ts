import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Service }       from './service';
import { LoggerService } from './logger.service';
import { AppService }    from './app.service';

/**
 * This class provides the NameList service with methods to read names and add names.
 */
@Injectable()
export class RefbookService extends Service  {

	public loaded    = false;
	public eq_loaded = false;

	public baseURL    = this._AppService.baseURL+'refbooks';
	public objTypeURL = this._AppService.baseURL+'typeobjects';

	public alldata      : any[] = [];
	public refbooks     : { [id: number]: any; } = { };
	public indexedData  : { [id: number]: any; } = { };

	public objTypes     : { [id: number]: any; } = { };
	public equipment    : { [id: number]: any; } = { };

	public manufacturer : any[] = [];
	public diameter     : any[] = [];
	public diameter2    : { [id: number]: any; } = { };

	public instrumentationsprs : { [id: number]: any; } = { };

	public spr_data : {[type_obj: number] : {
							[spr_type: string] : {
								[id:number]:any}
						    }
					  } = {};

	errorMessage: any;
	/**
	 * @param {AppService} _AppService - The injected Http.
	 * @param {Http} _http - The injected Http.
	 * @param {LoggerService} _logger - The injected Http.
	 * @constructor
	 */
	constructor (public _http: HttpClient,
	             public _logger: LoggerService,
	             public _AppService: AppService) {
		super(_http,
			_logger,
			_AppService);

			// загрузить весь справочник в сервис
		this.loadAllData();
			// загрузить список импов объектов из gasb
		this.loadTypeObj();
			// загрузить заводы-изготовители
		this.loadManufacturers();
			// загрузить диаметры
		this.loadDiameters();
		// загрузить справочники КИП
		this.loadInstrumentationSpr();
	}

	/*-----------------------------------------------------------------------------------------*/
	getById(id: string): Observable<any> {
		return this.getData(this.baseURL+'/'+id);
	}
	/*-----------------------------------------------------------------------------------------*/
	loadTypeObj() {
		// let params = new HttpParams();
		// //params.set('filter[]', 'id_parent=<>0');
		// params.set('per-page', '1000000');
		// params.set('order', 'id');
		let eq = ['1','4','31','32','80','81','82'];
		this.getData(this.objTypeURL)
			.subscribe(
			(resp:any) => {
				for (let i in resp) {
					//this._logger.debug(resp[i]);
					this.objTypes[resp[i].id] = resp[i];
					if (eq.indexOf(<any>resp[i].id) >= 0) {
						this.equipment[resp[i].id] = resp[i];
					}
				}
				this.eq_loaded = true;
				this._logger.debug('refbook.equipment=',this.equipment);
			},
			(error:any) => this.errorMessage = <any>error
		);
	}
	/*-----------------------------------------------------------------------------------------*/
	// по мере надобности загружаем справочники для каждого оборудования, е сли уже загружен - отдаём загруженный
	get_spr_data(id_type_obj: number, reload: boolean = false): Observable<any> {

		if (reload || !(id_type_obj in this.spr_data)) {
			return this.getData(this._AppService.baseURL+'equipmentspr/getData/'+id_type_obj)
				.map((res)=>{
					this.spr_data[id_type_obj] = <any>res;
					return res;
				});
		} else {
			return Observable.of(this.spr_data[id_type_obj]);
		}

	}
	/*-----------------------------------------------------------------------------------------*/
	loadManufacturers(term?: string) {
		let params = new HttpParams().set('order', 'manufacturer');
		this.getData(this._AppService.baseURL+'manufacturers', params)
			.subscribe(
				(resp:any) => {
					this.manufacturer = resp;
					// for (let i in resp) {
					// 	//this._logger.debug(resp[i]);
					// 	this.manufacturer[resp[i].id] = resp[i];
					// }
					//this._logger.debug(this.manufacturer);
				},
				(error:any) => this.errorMessage = <any>error
			);
	}

	/*-----------------------------------------------------------------------------------------*/
	loadInstrumentationSpr() {
		let params = new HttpParams().set('per-page', '10000');
		this.getData(this._AppService.baseURL+'instrumentationsprs', params)
			.subscribe(
				(resp:any) => {
					let dd = resp;
					for (let item of dd) {
						this.instrumentationsprs[item.id] = item;
					}
					//this._logger.debug('instrumentationsprs =', this.instrumentationsprs);
				},
				(error:any) => this.errorMessage = <any>error
			);
	}
	/*-----------------------------------------------------------------------------------------*/
	loadDiameters(term?: string) {
		let params = new HttpParams().set('filter[]', 'id_material==1')
									 .set('order', 'diameter');
		this.getData(this._AppService.baseURL+'diameters', params)
			.subscribe(
				(resp:any) => {
					this.diameter = resp;
					for (let i in resp) {
						//this._logger.debug(resp[i]);
						this.diameter2[resp[i].id] = resp[i];
					}
				},
				(error:any) => this.errorMessage = <any>error
			);
	}
	/*-----------------------------------------------------------------------------------------*/
	/*-----------------------------------------------------------------------------------------*/
	loadAllData() {
		this.getRecords()
			.subscribe(
				records => {
					this.alldata = records;
					// индексирование результата
					for (let item of this.alldata) {
						if (item.id_parent!==0) {
							this.indexedData[item.id] = item;
						} else {
							this.refbooks[item.id] = item;
							//this.refbooks[item.id].data = []; //Object.create(null);
							this.refbooks[item.id].data = Object.create(null);
						}
					}

					for (let id in this.indexedData) {
						if (this.indexedData[id].id_parent > 0) {
							////this.refbooks[this.indexedData[id].id_parent].data.push(this.indexedData[id]);
							this.refbooks[this.indexedData[id].id_parent].data[id] = this.indexedData[id];
						}
					}

					this.loaded = true;

					this._logger.debug('refbooks all data loaded');
				},
				(error:any) => this.errorMessage = <any>error,
			);
	}
	/*-----------------------------------------------------------------------------------------*/
	getRecords() {
		let params = new HttpParams().set('per-page', '1000000')
									 .set('order', 'id_parent,name');
		return this.getData(this.baseURL, params);
	}
	/*-----------------------------------------------------------------------------------------*/
	getByParent(parent:any) {
		let params = new HttpParams().set('filter[1]', 'id_parent=='+parent);
		if (parent===0) {
			params = params.set('filter[2]', 'id=<>0');
		}
		params = params.set('per-page', '1000000')
					   .set('order', 'weight, name');
		return this.getData(this.baseURL, params);
	}
	/*-----------------------------------------------------------------------------------------*/
	save(item:any) {
		let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Accept':'*/*', 'token': this._AppService.user.token });
		let options = { headers: headers, withCredentials: true };

		if (item.id===null) {
			let body = JSON.stringify(item);
			return this._http.post(this.baseURL, body, options)
				.catch(this.handleError);
		} else {
			let body = JSON.stringify(item);
			return this._http.put(this.baseURL+'/'+item.id, body, options)
				.catch(this.handleError);
		}
	}
	/*-----------------------------------------------------------------------------------------*/
	delete(id:number) {
		//let headers = new Headers({ 'Content-Type': 'application/json', 'Accept':'*/*' });
		return this._http.delete(this.baseURL+'/'+id)
			.catch(this.handleError);
	}
	/*-----------------------------------------------------------------------------------------*/

}
