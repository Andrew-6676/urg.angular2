import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable }     from '@angular/core';
import { Observable }     from 'rxjs/Observable';

import { UnitList }      from '../models/unit';

import { Service }       from './service';
import { LoggerService } from './logger.service';
import { AppService }    from './app.service';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class UnitService extends Service {
	//items : any;
	public name: string = 'unit';

	//private companyUrl = 'http://192.168.101.230:5001/company';
	protected baseUrl = this._AppService.baseURL+'units';
	protected opghUrl = this.baseUrl+'?filter[0]=l_area==Y';  // URL to web api

	public loaded = false;

	public indexOPGH: { [i: string]: string; } = { };
	public indexOPGH2: { [id: string]: any; } = { };

	public indexOPGH_flat: { [id: string]: any; } = { };

	public indexUnit: any[] = [];
	public units: any[] = [];

	errorMessage : any;

	constructor (public _http: HttpClient,
	             public _logger: LoggerService,
	             public _AppService: AppService) {
		super(_http,
			_logger,
			_AppService);
		this._AppService._unit = this;
		this.reloadCompanies();
		// this.loadCompanies()
		// 	.subscribe(
		// 		()=>{
		// 			this._AppService.login()
		// 		}
		// 	);
	}

	/*--------------------------------------------------------------------------------------*/
	reloadCompanies() {
		this.loaded = false;
		// if (Object.keys(this.indexOPGH).length>0) {
		// 	this._logger.debug('units in service already loaded');
		// 	return 0;
		// }
		return this.getCompany()
			.subscribe(
				(resp:any) => {
					this.units = resp;
					let i: any;
					for (i in this.units) {
						this.indexOPGH[this.units[i].id] = <any>i;
						this.indexOPGH2[this.units[i].id] = this.units[i];
						//this.indexUnit.push(this.units[i].id);

						this.indexOPGH_flat[this.units[i].id] = {
							parent_gasb: -1,
							name: this.units[i].name
						};

						let c1: any;
						for (c1 in this.units[i].childs) {
						// 	this.indexUnit[this.units[i].id][this.units[i].childs[c1].id] = <any>c1*1;
							this.indexOPGH_flat[this.units[i].childs[c1].id_gasb] = {
								parent_gasb: this.units[i].id_gasb,
								name: this.units[i].childs[c1].name
							};

							for (let c2 in this.units[i].childs[c1].childs) {
								this.indexOPGH_flat[this.units[i].childs[c1].childs[c2].id_gasb] = {
									parent_gasb: this.units[i].childs[c1].id,
									name: this.units[i].childs[c1].childs[c2].name
								};
							}

						}
					}

					//this._logger.debug(this.units);
					// this.currentOrghIndex = this.indexedOPGH[this._AppService.app.currentOPGH.id];
					//this._logger.debug(this.indexOPGH2);
					this.loaded = true;
					this._logger.debug('units in service loaded');
					this._AppService.login();
				},
				(error:any) =>  this.errorMessage = <any>error
			);
	}
	/*--------------------------------------------------------------------------------------*/
	// loadCompanies(): Observable<any> {
	// 	this._logger.debug();
	// 	if (this.indexOPGH_flat=={}) {
	// 		this._logger.debug('units in service start load');
	// 		return this.reloadCompanies().map(()=>{return true});
	// 	} else {
	// 		return Observable.of(true);
	// 	}
	// }
	/*--------------------------------------------------------------------------------------*/
	// getById(id:string): Observable<any> {
	// 	let params = new HttpParams();
	// 	params.set('fields','id,name');
	// 	return this.getData(this.baseUrl+'/'+id, params);
	// }
	/*--------------------------------------------------------------------------------------*/
    getCompany(id: number = -1, dept: number = 0): Observable<UnitList[]> {
    	let params = new HttpParams();
	    if (dept>0) params = params.set('depth', dept+'');
	    let url = id>0 ? this.baseUrl+'/list/'+id : this.baseUrl+'/list';
	    return this.getData(url, params);
    }
	/*--------------------------------------------------------------------------------------*/
    getOPGH() {
        return this.getData(this.opghUrl);
    }
	/*--------------------------------------------------------------------------------------*/
    getUnits() {
        return this.getData(this.baseUrl);
    }
	/*--------------------------------------------------------------------------------------*/
	getOpghByUnit(unit_id: string): any {
		if (!this.indexOPGH_flat[unit_id] || this.indexOPGH_flat[unit_id].parent_gasb<0) {
			return unit_id;
		}

		let parent = this.indexOPGH_flat[unit_id].parent_gasb;
			// рекурсия
		return this.getOpghByUnit(parent);

	}
	/*--------------------------------------------------------------------------------------*/

}
