import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable }    from '@angular/core';
import { Observable }    from 'rxjs/Observable';

import { Service }       from './service';
import { LoggerService } from './logger.service';
import { AppService }    from './app.service';


@Injectable()
export class InstrumentationService extends Service {

	protected baseUrl = this._AppService.baseURL+'instrumentations';
	protected equipmentByUrgUrl = this.baseUrl+'/listByUrg';

	constructor (public _http: HttpClient,
	             public _logger: LoggerService,
	             public _AppService: AppService) {
		super(_http,
			_logger,
			_AppService);
	}
	/*-------------------------------------------------------------------------------*/
	getListByUrg(id_urg: string): Observable<any> {
		let params = new HttpParams().set('expand','files');
		return this.getData(this.equipmentByUrgUrl+'/'+id_urg, params);
	}
	getById(id_instr:string): Observable<any> {
		let params = new HttpParams().set('expand','files');
		return this.getData(this.baseUrl+'/'+id_instr, params);
	}
	/*-------------------------------------------------------------------------------*/
	save(data: any, id_urg:string) {
		data.id_urg = id_urg;
		return super.save(data);
	}
	/*-------------------------------------------------------------------------------*/
}

