import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Injectable   } from '@angular/core';
import { Router }       from '@angular/router';
import { Observable }   from 'rxjs/Observable';
import { Config }       from '../config/env.config';

import { LoggerService }  from './logger.service';
import { UnitService }    from './unit.service';
import { ToastrService }  from 'ngx-toastr';
//import { DialogsService }          from '../../components/dialogs/dialogs.service';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';


import { UrgTable }     from '../models/urg';

declare let $:any;

@Injectable()
export class AppService {

	public dev = false;
	public dbg = false;
	public baseURL = Config.API;
	//public baseURL = 'http://urg.oblgaz/backend/';
	public version = '<%= VERSION %>';
	public required_version = '0';
	public actual_version = true;

	public server_params:any = {};
	//public panorama_url = 'http://panorama2.oblgaz?';

	auth_in_progress = false;
	sid:string = null;
	app: App = new App();
	user: User = new User();

	redirectUrl: string = '';
	_unit: UnitService = null;

	filter: any = {
		general: {},
		urg: {
			page: {
				itemsPP: 20,        // строк на страницу
				currentPage: 1,    // текущая страница
			},
			date: new Date(),
			id_type_urg: -100,
			num: "",
			in_ring: null,
			sort: {},
		}
	};

	tmpUrg: {
		urg: UrgTable,
		equipment: any;
		fileToEquipment:  any;
		instrumentation: any;
	} = null;

	private _load_qeue: number = 0;
	/*-----------------------------------------------------------------------------------------*/

	/**
	 * @param {HttpClient} _http - The injected Http.
	 * @param {LoggerService} _logger - The injected LoggerService.
	 * @param {ToastrService} _toastr - The injected ToastrService.
	 //* @param {DialogsService} _dialogs - The injected DialogsService.
	 * @param {Router} _router - The injected Router.
	 * @constructor
	 */
	constructor(private _http: HttpClient,
	            private _logger: LoggerService,
	            public _toastr: ToastrService,
	            //public _dialogs: DialogsService,
	            public _router: Router) {
		this._logger.log('Environment config', Config);
	}

	/*-----------------------------------------------------------------------------------------*/
	public loadingInProgress(q?: number) {
		//this._logger.debug(q);
		if (q==0) {
			this._load_qeue = 0;
		} else if (q) {
			 this._load_qeue += q;
		}
		return this._load_qeue;
	};
	/*-----------------------------------------------------------------------------------------*/
	checkRole(role: string):boolean {
		/*
		* root - может вообще всё кроме debug
		* admin - может всё в своём профицентре
		* edit - может редактировать информацию по УРГ в своём профицентре
		* read - может только просматривать информацию в своём профицентре
		* */

		if (this.user.roles.indexOf('root') >= 0 && role!='debug') { return true;}

		if (role=='edit' && this.user.roles.indexOf('admin') >= 0) { return true;}
		//if (role=='gspr' && this.user.roles.indexOf('admin') >= 0) { return true;}

		// TODO: сделать возможность проверить более 1 роли за раз
		return (this.user.roles.indexOf(role) >= 0);
	}
	/*-----------------------------------------------------------------------------------------*/
	// loadUserData() {
	// 	if (this.user)
	// 		return Observable.of(this.user);
	// 	else
	// 		return this._http.get(this.baseURL+'getUserData'/*, {search: params, headers: this.head}*/)
	// 			.map((res:Response) => {
	// 				this.user = res.json();
	// 				//return res.json();
	// 				//this.user_checked = true;
	// 				return this.user;
	// 			})
	// 			.catch(this.handleError);
	// }
	// getUserData(): Observable<any> {
	// 	if (this.user)
	// 		return Observable.of(this.user);
	// 	else {
	// 		return this.loadUserData();
	// 	}
	// }
	/*-----------------------------------------------------------------------------------------*/
	/*-----------------------------------------------------------------------------------------*/


	login(user?: any): Observable<any> {
		if (this.user.isLoggedIn) {
			return Observable.of(this.user);
		}
		this.auth_in_progress = true;
		let headers = new HttpHeaders({'Content-Type': 'application/json', 'Accept': '*/*'});
		let body = {};

		if (!user) {
				// если есть sid - логинимся с помощью его
			if (this.sid) {
				this._logger.debug('auth by sid', this.sid);
				body = {sid: this.sid};
			} else {    // иначе пробуем с помощью токена
				let token = localStorage.getItem('auth_token');
				this._logger.debug('auth by token', token);
				body = {token: token};
				if (!!token) {

				} else {
					this._logger.debug('token auth result:', 'no saved token');
					this.auth_in_progress = false;
					return Observable.of(false);
				}
			}
		} else {
			body = user;
		}

		let login_request = this._http.post(this.baseURL + 'login', body, {headers: headers, withCredentials: true})
			//.delay(2500)
			.map((resp:any) => {

				this._logger.debug('login response:', resp);

				this.actual_version = this.version == resp.client_version;
				this.required_version = resp.client_version;

				if (!this.actual_version) {
					this.toastrOpen('Неактуальная версия клиента - нажмите Ctrl+F5', 'warning', 0);
				}

				//this._logger.debug('login response:', resp);
				if (resp.status == 'ok') {

					if (user) {
						localStorage.setItem('auth_token', resp.token);
					}

					this.server_params = resp.params;
					$('#admin_email').text(this.server_params.email);

					this.user.token = resp.user.token;
					this.user.isLoggedIn = true;
					this.user.isGuest = false;

					this.user.name = resp.user.fio;
					this.user.login = resp.user.login;
					this.user.roles = resp.user.roles;
					this.user.unit = new Unit(resp.user.id_unit);

					if (this.checkRole('debug')){
						this.dbg = true;
						//this._logger.debug('enable dev from role');
					}
					this.dev = (Config.ENV=='DEV');

					this.app.currentUnit = {
						id: resp.user.id_unit,
						name: '-'
						//name: this._unit.indexOPGH_flat[resp.user.id_unit] ? this._unit.indexOPGH_flat[resp.user.id_unit].name : this._unit.indexOPGH2[resp.user.id_unit].name
					};
					let id_opgh = this._unit.getOpghByUnit(resp.user.id_unit);
					this.app.currentOPGH = {
						id: id_opgh,
						name: '-'
						//name: this._unit.indexOPGH2[id_opgh] ? this._unit.indexOPGH2[id_opgh].name : '-'
					};

					this.auth_in_progress = false;
					return {status: true, message: 'Вы вошли как: ', user: resp.user};
				} else {
					//localStorage.removeItem('auth_token');
					this.auth_in_progress = false;
					return {status: false, message: (<any>resp).message}
				}
			})
			.catch(this.handleError);


		if (!user) {
			login_request
				//.delay(2500)
				.subscribe(
				(resp:any) => {
					this.auth_in_progress = false;
					if (this.sid) {
						this._logger.debug('auth by sid result:', resp);
					} else {
						this._logger.debug('auth by token result:', resp);
					}
					if (this.redirectUrl) {
						let redirect = this.redirectUrl;
						this.redirectUrl = '';
						// Redirect the user
						this._router.navigate([redirect]);
					}
				}
			);
			return Observable.of(true);
		} else {
			return login_request;
		}

		//this._logger.debug(user);
		//return Observable.of(false).delay(2000); //.do((val:any) => this.user.isLoggedIn = false);
	}

	/*-----------------------------------------------------------------------------------------*/
	toggleDebugMode() {
		this.dbg = !this.dbg;
		return false;
	}
	/*-----------------------------------------------------------------------------------------*/
	/*-----------------------------------------------------------------------------------------*/
	logout() {
		localStorage.removeItem('auth_token');
		this.user.isGuest = true;
		this.user.isLoggedIn = false;

		this.user.token = '';

		this.user.name = 'Гость';
		this.user.roles = ['urg_guest'];
		this.user.unit = null;

		this.app.currentUnit = null;
		this.app.currentOPGH = null;

		this._router.navigate(['/login']);

		let headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'Accept': '*/*',
			'urg_token': this.user.token
		});


		return this._http.post(this.baseURL + 'logout', {}, {headers: headers, withCredentials: true})
			.catch(this.handleError);
			//.delay(100)
	}

	/*-----------------------------------------------------------------------------------------*/

	sendMail(text:string, subject?: string) {
		this._logger.debug('send mail');

		let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Accept':'*/*' });
		let options = {headers: headers, withCredentials: true };

		let body = {
			subject: subject ? subject : 'urg_user_mail',
			text: text,
			user: this.user.name,
			unit:  this._unit.indexOPGH_flat[this.user.unit.id].name
		};
		return this._http.post(this.baseURL+'sendMail', JSON.stringify(body), options)
			.map(this.extractData)
			.catch(this.handleError)
			.subscribe(
				(resp:any) => {
					this._logger.debug(resp);
				},
				(error:any) => {
					this.toastrOpen('Ошибка отправки сообщения', error, 0);
					//this.errorMessage = <any>error
				},
				() => {
					this.toastrOpen('Сообщение отправлено', 'success');
				}
			);
	}
	/*-----------------------------------------------------------------------------------------*/
	toastrOpen(msg: string, type='info', duration?:number) {
		// success/error/warn/info/show
		let d = new Date;
		let time = '[' + d.getHours() + ':' + (d.getMinutes()>9 ? d.getMinutes() : '0'+d.getMinutes()) + ':' + (d.getSeconds()>9 ? d.getSeconds() : '0'+d.getSeconds()) + '] ';
		let options: any = {
			closeButton: true,
			enableHtml: true,
			progressBar: false
		};
		if (duration!==undefined) {
			options.timeOut = 600000;
			options.progressBar = false;
		}
		let t : any = this._toastr;
		t[type](msg, time, options);
	}

	/****************************************************************/

	/**
	 * Handle HTTP error
	 */
	protected handleError(error: any) {
		// In a real world app, we might use a remote logging infrastructure
		// We'd also dig deeper into the error to get a better message
		let errMsg = (error.message) ? error.message :
			error.status ? `${error.status} - ${error.statusText}` : 'Server error';
		console.error(errMsg); // log to console instead
		//TODO: сделать нормальную обработку ошибок
		alert(errMsg);
		this.toastrOpen(errMsg, 'error', 0);
		return Observable.throw(errMsg);
	}

	protected extractData(res: Response) {
		if (res.status < 200 || res.status >= 300) {
			alert('Bad response status: ' + res.status);
			throw new Error('Bad response status: ' + res.status);
		}
		return res || {};
	}

	/****************************************************************/
}

/*---------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------*/

class User {
	// аутентифицирован
	//isGuest: boolean = false;
	//isLoggedIn: boolean = true;
	// НЕ аутентифицирован
	isLoggedIn: boolean = false;
	isGuest: boolean = true;

	token: string = '';

	name: string = 'Гость';
	login: string = 'guest';
	unit: Unit = null;
	//unit: Unit = new Unit('1');
	roles: string[] = ['urg_guest'];
}

class App {
	pageTitle: string = '<%= APP_TITLE %>';

	// currentOPGH : Unit = new Unit('22');
	// currentUnit: Unit = new Unit('1');

	currentUnit: Unit = null;
	currentOPGH: Unit = null;
}

class Unit {
	id: string;
	name: string;

	constructor(id: string) {
		this.id = id;
	}
}
