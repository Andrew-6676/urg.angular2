import { Injectable }    from '@angular/core';
import { HttpClient, HttpParams }          from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Service }       from './service';
import { LoggerService } from './logger.service';
import { AppService }    from './app.service';

import { File }          from '../models/file';


@Injectable()
export class FilesService extends Service {

	public name: string = 'Urg';
	public baseUrl     = this._AppService.baseURL+'files';
	public listUrl     = this.baseUrl+'List';
	private delFileUrl = this.baseUrl+'/delete/';

	/**
	 * @param {AppService} _AppService - The injected Http.
	 * @param {Http} _http - The injected Http.
	 * @param {LoggerService} _logger - The injected Http.
	 * @constructor
	 */
	constructor (public _http: HttpClient,
	             public _logger: LoggerService,
	             public _AppService: AppService) {
		super(_http,
			_logger,
			_AppService);
	}

	/*-----------------------------------------------------------------------------------------*/
	public getFiles(id_obj: string) {
		let params = new HttpParams().set('order','id_category');
		return this.getData(this.listUrl+'/'+id_obj);
	}
	/*-----------------------------------------------------------------------------------------*/
	public saveFile(file: File) {
		this._logger.log('rename file', file);
		let data = {
			id: file.id,
			title: file.title,
			id_category: file.id_category
		};
		return this.saveData(this.baseUrl, data);
	}
	/*-----------------------------------------------------------------------------------------*/
	public deleteFile(id:string) {
		return this.deleteData(this.delFileUrl+id);
	}
	/*-----------------------------------------------------------------------------------------*/

}

