import { Injectable }       from '@angular/core';
import {
	CanActivate, Router,
	ActivatedRouteSnapshot,
	RouterStateSnapshot
}                        from '@angular/router';
import { AppService }    from './app.service';
import { LoggerService } from './logger.service';

@Injectable()
export class AuthGuard implements CanActivate {
	constructor(
		private _AppService: AppService,
		private _logger: LoggerService,
		private router: Router
	) {}

	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): boolean {
		let url: string = state.url;

		//this._logger.warn(state.url);
		// if (url.indexOf('edit')>-1 || url.indexOf('add')>-1) {
		// 	if (!this._AppService.checkRole('edit')) {
		// 		this.router.navigate(['/']);
		// 		return false;
		// 	}
		// }
		// if (this._AppService.auth_in_progress) {
		//
		// }
		if (!this._AppService.actual_version) {
			this.router.navigate(['/']);
			return false;
		}
		return this.checkLogin(url);
	}

	checkLogin(url: string): boolean {
		if (this._AppService.user.isLoggedIn) { return true; }


			// Store the attempted URL for redirecting
		this._AppService.redirectUrl = url;

			// Navigate to the login page with extras
		//let token = localStorage.getItem('auth_token');
		//if (!token) {
			this.router.navigate(['/login']);
		//}
		return false;
	}
}
