import { Injectable }             from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable }             from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { LoggerService } from './logger.service';
import { AppService }    from './app.service';
import { Service }       from './service';

@Injectable()
export class RingService extends Service {

	public name: string = 'Ring';
	public baseUrl = this._AppService.baseURL+'rings';

	/**
	 * @param {AppService} _AppService - The injected Http.
	 * @param {HttpClient} _http - The injected Http.
	 * @param {LoggerService} _logger - The injected Http.
	 * @constructor
	 */
	constructor (public _http: HttpClient,
	             public _logger: LoggerService,
	             public _AppService: AppService) {
		super(_http,
			_logger,
			_AppService);
	}
	/*-----------------------------------------------------------------------------------------*/
	public excludeFromRing(id:string) {
		return this.deleteData(this.baseUrl+'/'+id);
	}
	/*-----------------------------------------------------------------------------------------*/
	makeRing(id_current_urg: string, ids_other_urg: string[]): Observable<any> {
		this._logger.log('make ring: ', id_current_urg);
		let data = {
			id_current_urg: id_current_urg,
			id_other_urg: ids_other_urg
		};
		return this.addData(this.baseUrl+'/make', data);
	}
	/*-----------------------------------------------------------------------------------------*/
	public includeToRing(id_current_urg: string, id_other_urg:string): Observable<any> {
		this._logger.log('include to ring: ', id_current_urg, id_other_urg);
		let data = {
			id_current_urg: id_current_urg,
			id_other_urg: id_other_urg
		};
		return this.addData(this.baseUrl, data);
	}
	/*-----------------------------------------------------------------------------------------*/
	getUrgsInRing(id_urg: string): Observable<any> {
		let url = this._AppService.baseURL+'urgs/'+id_urg;
		let params = new HttpParams().set('fields','[]')
									 .set('expand','ring');
		return this.getData(url, params);
	}
	/*-----------------------------------------------------------------------------------------*/
	getUrgsInRingById(id_ring: string): Observable<any> {
		let params = new HttpParams();
		return this.getData(this.baseUrl+'/list/'+id_ring, params);
	}
	/*-----------------------------------------------------------------------------------------*/
	getUrgList(filter: any): Observable<any> {
		let params = new HttpParams()
		// исключить текущий УРГ из выборки
			.set('urg_type', filter.type)
			.set('id_unit', filter.unit);
		//params.set('per-page', "999999999");
		return this.getData(this.baseUrl+'/listAllUrg', params);
	}
	/*-----------------------------------------------------------------------------------------*/
	getRingsList(): Observable<any> {
		let params = new HttpParams()
			.set('expand', 'data');
		return this.getData(this.baseUrl, params);
	}
	/*-----------------------------------------------------------------------------------------*/

	// save(data: any) {
	// 	return this.saveData(this.baseUrl, data);
	// }
	/*-----------------------------------------------------------------------------------------*/

	/*-----------------------------------------------------------------------------------------*/

}

