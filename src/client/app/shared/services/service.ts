import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { LoggerService } from './logger.service';
import { AppService }    from './app.service';


@Injectable()
export class Service {
	public loaded: boolean = false;
	public name: string = 'service';
	public headers: any = {get: function(){return 0}};

	public totalCount: number = -1;

	protected baseUrl: string = this._AppService.baseURL;

	protected head = new HttpHeaders({
		'Content-Type': 'application/json',
		'Accept':'*/*',
		'token': this._AppService.user.token,
		'user': encodeURIComponent(this._AppService.user.login)
	});

	constructor (
		public _http: HttpClient,
		public _logger: LoggerService,
		public _AppService: AppService
	) {}

	/*--------------------------------------------------------------------------------------*/
	getBaseUrl() {
		return this.baseUrl;
	}
	/*--------------------------------------------------------------------------------------*/
	getById(id: string): Observable<any> {
		let params = new HttpParams().set('fields','id,name');
		return this.getData(this.baseUrl+'/'+id, params);
	}
	/*--------------------------------------------------------------------------------------*/
	getData (_url:string, param?:HttpParams): Observable<any> {
		//return Observable.of([]);
		let params = param ? param : new HttpParams();
		return this._http
			.get(_url, {observe: 'response', params: params, headers: this.head, withCredentials: true})
			.map(res => {
				//this._logger.debug('------------------- data',_url,res.body);
				//this._logger.debug('------------------- headers',_url,res.headers);
				this.headers = res.headers;
				//this.totalCount = <any>(res.headers.get('X-Pagination-Total-Count'));
					// проверка соответствия версии
				let version = this.headers.get('Client-Version');
				//this._logger.debug('version=',version);
				if (version) {
					this._AppService.actual_version = this._AppService.version == version;
					this._AppService.required_version = version;

					if (!this._AppService.actual_version) {
						this._AppService.toastrOpen('Неактуальная версия клиента - нажмите Ctrl+F5', 'warning', 0);
					}
				}

				return res.body;
			})
			//.catch(this.handleError);
	}
	/*--------------------------------------------------------------------------------------*/
	delete(id:any) {
		return this.deleteData(this.baseUrl + '/' + id);
	}
	/*--------------------------------------------------------------------------------------*/
	deleteData(_url:string): Observable<any> {
		let options = { headers: this.head, withCredentials: true };
		return this._http.delete(_url, options)
			//.map((res) => res.json())
			.catch(this.handleError);
	}
	/*--------------------------------------------------------------------------------------*/
	save(data: any, any?:any) {
		return this.saveData(this.baseUrl, data);
	}
	/*--------------------------------------------------------------------------------------*/
	saveData(_url:string, data: any): Observable<any> {
		let options = { headers: this.head, withCredentials: true };
		let body = JSON.stringify(data);
		let resp: any;
		if (data.id>0) {
			resp = this._http.put(_url+'/'+data.id, body, options).map(res => {
				return res;
			});
		} else {
			resp = this._http.post(_url, body, options);
		}

		return resp.map(this.extractData).catch(this.handleError);
	}
	/*--------------------------------------------------------------------------------------*/
	addData(_url:string, data: any): Observable<any[]> {
		let options = { headers: this.head, withCredentials: true };
		let body = JSON.stringify(data);
		let resp: any;
		resp = this._http.post(_url, body, options);

		return resp.map(this.extractData).catch(this.handleError);
	}
	/*--------------------------------------------------------------------------------------*/

	/**
	 * Handle HTTP error
	 */
	protected handleError(error:any) {
		// In a real world app, we might use a remote logging infrastructure
		// We'd also dig deeper into the error to get a better message
		let errMsg = (error.message) ? error.message :
			error.status ? `${error.status} - ${error.statusText}` : 'Server error';
		console.error(errMsg); // log to console instead
		//TODO: сделать нормальную обработку ошибок
		alert(errMsg);
		this._AppService.loadingInProgress(-1);
		this._AppService.toastrOpen(errMsg, 'error', 0);
		return Observable.throw(errMsg);


		// // In a real world app, we might send the error to remote logging infrastructure
		// let errMsg = error.message || 'Server error';
		// console.error(errMsg); // log to console instead
		// return Observable.throw(errMsg);
	}

	protected extractData(res: Response) {
		if (res.status < 200 || res.status >= 300) {
			alert('Bad response status: ' + res.status);
			throw new Error('Bad response status: ' + res.status);
		}
		return res || { };
	}
}
