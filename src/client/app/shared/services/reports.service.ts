import { Injectable }             from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable }             from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Service }       from './service';
import { LoggerService } from './logger.service';
import { AppService }    from './app.service';

import { ObjectIterator } from '../models/object.iterator';
/*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------*/
@Injectable()
export class ReportsService extends Service {

	public name: string = 'Reports';
	public baseUrl = this._AppService.baseURL+'printer/reports';

	public list: any[] = [];
	public indexedList: {[key: string]:any} = new ObjectIterator(); //Object.create(null);

	/**
	 * @param {AppService} _AppService - The injected Http.
	 * @param {HttpClient} _http - The injected Http.
	 * @param {LoggerService} _logger - The injected Http.
	 * @constructor
	 */
	constructor (public _http: HttpClient,
	             public _logger: LoggerService,
	             public _AppService: AppService) {
		super(_http,
			_logger,
			_AppService);

		this.getReportsList()
			.subscribe((resp)=>{});
	}
	/*-----------------------------------------------------------------------------------------*/
	getReportsList(): Observable<any> {
		if (this.list.length>0) {
			this._logger.debug('list is exists');
			return Observable.of(this.list);
		} else {

			//let params = new HttpParams();
			//params.set('expand', 'data');
			return this.getData(this.baseUrl)
					   .map((resp) => {
						   this._logger.debug('list is NOT exists', this.list);
					   	   this.list = <any>resp;

						   for(let i in this.list) {
							   this.indexedList[this.list[i].name] = this.list[i];
						   }
						   this._logger.debug(this.indexedList);

						   return resp;
					   });
		}

	}
	/*-----------------------------------------------------------------------------------------*/
	print(name:string, id: any) {
		if (!this.indexedList.hasOwnProperty(name)) {
			this._AppService.toastrOpen('Запрашиваемый отчёт не найден!', 'error',0)
			return false;
		}

		let reportUrl = '';
		for(let i of this.list) {
			if (name==i.name) {
				reportUrl = i.url;
				break;
			}
		}
		window.open(this._AppService.baseURL+''+reportUrl+'/'+id);
		return false;
	}
	// save(data: any) {
	// 	return this.saveData(this.baseUrl, data);
	// }
	/*-----------------------------------------------------------------------------------------*/
	makeLink(name: string):string {
		return this._AppService.baseURL+''+this.indexedList[name].url;
	}
	/*-----------------------------------------------------------------------------------------*/

}

