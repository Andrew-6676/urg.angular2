import { Injectable }            from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable }            from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { LoggerService } from './logger.service';
import { FilesService }  from './files.service';
import { AppService }    from './app.service';
import { Service }       from './service';

import { File }          from '../models/file';


@Injectable()
export class UrgService extends Service {

	public name: string = 'Urg';
	public baseUrl = this._AppService.baseURL+'urgs';

	//private delFileUrl        = this.baseUrl+'/files/delete/';
	//private getFilesUrl       = this.baseUrl+'/files/';
	private filesUrl          = this._AppService.baseURL+'files';
	private roomsUrl          = this._AppService.baseURL+'rooms';
	private masterUrl         = this._AppService.baseURL+'urgmasters';
	private getListByUnitUrl  = this.baseUrl+'/listByUnit';

	private searchOwnerUrl    = this._AppService.baseURL+'owners';

	//private getList   = this.baseUrl+'/listByUnit';
	//private _delFileUrl         = 'http://urg.backend/file/delete/';

	/**
	 * * @param {HttpClient} _http - The injected Http.
	 * @param {LoggerService} _logger - The injected LoggerService.
	 * @param {FilesService} _filesService - The injected FilesService.
	 * @param {AppService} _AppService - The injected AppService.
	 * @constructor
	 */
	constructor (public _http: HttpClient,
	             public _logger: LoggerService,
	             public _filesService: FilesService,
	             public _AppService: AppService) {
		super(_http,
			_logger,
			_AppService);
	}

	/*-----------------------------------------------------------------------------------------*/
	countAll() {
		return this._http.head(this.baseUrl, {observe: 'response', responseType: 'text'})
			.map(res => {
			 	//this._logger.debug('--------------------*', res);
			 	return res.headers.get('X-Pagination-Total-Count');
				// 	return 30;
			})
			.catch(this.handleError);
		//return Observable.of(50);
	}
	/* ----------------------------------------------------------- */
	getStat() {
		// return this._http.get(this.baseUrl+'/getStat')
		// 	.map(this.extractData)
		// 	.catch(this.handleError);
		return this.getData(this.baseUrl+'/getStat');
	}
	/*-----------------------------------------------------------------------------------------*/
	clearAddress(id1: string, id2: string) {
		return this.deleteData(this.baseUrl+'/clearAddress/'+id1+'/'+id2);
	}
	/*-----------------------------------------------------------------------------------------*/
	search(filter: any): Observable<any> {
		let params = new HttpParams();

		if (filter.expand.length>0) {
            params = params.set('expand',filter.expand.join(','));
		}

		if (filter.id_type_urg && filter.id_type_urg>0) {
			params = params.set('filter[0]','id_type_urg=='+filter.id_type_urg);
		}
		if (filter.id_unit && (filter.id_unit*1)>0) {
			params = params.set('filter[1]','id_unit=='+filter.id_unit);
		} else {
			// выборка по всем детям указанного облгаза
		}
		if (filter.num && filter.num!=='') {
			params = params.set('filter[2]','num=='+filter.num);
		}
        if (filter.master && filter.master.trim() !== '' && filter.master !== '-') {
            params = params.set('master', filter.master);
        }

            // сортировка
		let sort: string[] = [];
		for (let s in filter.sort ) {
			sort.push(s + ' '+ filter.sort[s]);
		}
		if (sort.length>0) {
			params = params.set('order', sort.join(','));
		}


			// исключить текущий УРГ из выборки
		if  (filter.page && filter.page.itemsPP > 0) {
			params = params.set('per-page', filter.page.itemsPP)
						   .set('page', filter.page.currentPage);
		} else {
			params = params.set('per-page', "999999999");
		}

		return this.getData(this.baseUrl+'/search', params);
	}
	/*-----------------------------------------------------------------------------------------*/
	getById(id_urg:string, mode?: string): Observable<any> {
		let params = new HttpParams();
		if (mode) {
			params = params.set('mode', mode);
		}
		params = params.set('expand','address,files,ring,owner,rooms,master,mastersHistory,lastwork');
		return this.getData(this.baseUrl+'/'+id_urg, params);
	}
	/*-----------------------------------------------------------------------------------------*/
	getByOPGH(id_opgh: string) {
		let params = new HttpParams()
			.set('','');
		return this.getData(this.baseUrl, params);
	}
	/*-----------------------------------------------------------------------------------------*/
	getByUnitId(id_unit: string, filter:any, mode?: string) {
		this._logger.styledLog('%c filter:', 'background: #222; color: #bada55' ,filter);
		let params = new HttpParams();
		if (mode) {
			params = params.set('mode', mode);
		}
		params = params.set('expand', 'address')
					   .set('per-page', filter.page.itemsPP)
					   .set('page', filter.page.currentPage)
					   .set('filter[0]', 'id_unit=='+id_unit);
		if (filter.id_type_urg > -10) {
			params = params.set('filter[1]', 'id_type_urg=='+filter.id_type_urg);
		}
		if (!!filter.num) {
			params = params.set('filter[2]', 'num=@'+filter.num);
		}
		let sort: string[] = [];
		for (let s in filter.sort ) {
			sort.push(s + ' '+ filter.sort[s]);
		}
		if (sort.length>0) {
			params = params.set('order', sort.join(','));
		}
		//params.set('expand', 'manufacturer,type,building_type');
		//return this.getData(this.getListByUnitUrl+'/'+id_unit, params);
		return this.getData(this.baseUrl, params);
	}
	/*-----------------------------------------------------------------------------------------*/
	public getFiles(id_urg: string) {
		//return this.getData(this.getFilesUrl+id_urg);
		return this._filesService.getFiles(id_urg);
	}
	/*-----------------------------------------------------------------------------------------*/
	public saveFile(file: File) {
		this._logger.log('rename file', file);
		let data = {
			id: file.id,
			title: file.title,
			id_category: file.id_category
		};
		return this.saveData(this.filesUrl, data);
	}
	/*-----------------------------------------------------------------------------------------*/
	public deleteFile(id:string) {
		//return this.deleteData(this.delFileUrl+id);
		return this._filesService.delete(id);
	}
	/*-----------------------------------------------------------------------------------------*/
    searchMaster(term: string, id_unit?: any): Observable<any> {
        let params: HttpParams;
        params = new HttpParams().set('filter[]', 'name=@%'+term+'%');
        if (id_unit) {
            params = params.set('id_unit', id_unit)
						   .set('filter[]', 'name=@%'+term+'%');
		}
        return this.getData(this.masterUrl+'/search', params);
    }
	/*-----------------------------------------------------------------------------------------*/
	saveMaster(master: any) {
		return this.saveData(this.masterUrl, master);
	}
	/*-----------------------------------------------------------------------------------------*/
    deleteMaster(master: any) {
        return this.deleteData(this.masterUrl+'/'+master);
    }
	/*-----------------------------------------------------------------------------------------*/
	saveRoom(room : any) {
		return this.saveData(this.roomsUrl, room);
	}
	/*-----------------------------------------------------------------------------------------*/
	deleteRoom(id_room: any) {
		return this.deleteData(this.roomsUrl+'/'+id_room);
	}
	/*-----------------------------------------------------------------------------------------*/
	getRooms(id_urg: string) {
		let params = new HttpParams()
			.set('filter[]', 'id_urg=='+id_urg);
		return this.getData(this.roomsUrl, params);
	}
	/*-----------------------------------------------------------------------------------------*/
	searchOwner(term:string): Observable<any[]> {
		let params = new HttpParams()
			.set('filter[]','owned=@%'+term+'%');
		return this.getData(this.searchOwnerUrl, params);
	}
	/*-----------------------------------------------------------------------------------------*/
	save(data: any) {
		return this.saveData(this.baseUrl, data);
	}
	/*-----------------------------------------------------------------------------------------*/
	generateUniqueNum(id:string) {
		return Math.floor(Math.random()*10000);
	}
	/*-----------------------------------------------------------------------------------------*/
	addLastWork(id_urg: any, date: Date) {
		let params = new HttpParams()
			.set('id_urg', id_urg)
			.set('date', date.toISOString().substr(0,10));
		return this.getData(this.baseUrl+'/addLastwork', params);
	}
	/*-----------------------------------------------------------------------------------------*/

}


