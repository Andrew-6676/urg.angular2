import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Service }       from './service';
import { LoggerService } from './logger.service';
import { AppService }    from './app.service';

/**
 * This class provides the NameList service with methods to read names and add names.
 */
@Injectable()
export class HelpService extends Service  {

	public loaded    = false;
	public eq_loaded = false;

	public baseURL    = this._AppService.baseURL+'helps';
	//public byIndexURL = this._AppService.baseURL+'typeobjects';

	menu: any = {};
		errorMessage: any;
	/**
	 * @param {AppService} _AppService - The injected Http.
	 * @param {HttpClient} _http - The injected Http.
	 * @param {LoggerService} _logger - The injected Http.
	 * @constructor
	 */
	constructor (public _http: HttpClient,
	             public _logger: LoggerService,
	             public _AppService: AppService) {
		super(_http,
			_logger,
			_AppService);

		this.loadMenu();
	}

	/*-----------------------------------------------------------------------------------------*/
	getById(id: string): Observable<any> {
		return this.getData(this.baseURL+'/'+id);
	}
	/*-----------------------------------------------------------------------------------------*/
	getByIndex(index: string): Observable<any> {
		let params = new HttpParams().set('filter[]', 'index=='+index);
		return this.getData(this.baseURL, params);
	}
	/*-----------------------------------------------------------------------------------------*/
	loadMenu() {
		// let params = new HttpParams();
		// //params.set('filter[]', 'id_parent=<>0');
		// params.set('per-page', '1000000');
		// params.set('order', 'id');
		// this.getData(this.baseURL+'/menu')
		// 	.subscribe(
		// 	(resp:any) => {
		// 		this.menu = resp;
		// 		this._logger.debug('help.menu=',this.menu);
		// 	},
		// 	(error:any) => this.errorMessage = <any>error
		// );
	}
	/*-----------------------------------------------------------------------------------------*/
	/*-----------------------------------------------------------------------------------------*/
	// getRecords() {
	// 	let params = new HttpParams();
	// 	//params.set('filter[]', 'id_parent=<>0');
	// 	params.set('per-page', '1000000');
	// 	//params.set('filter[0]', 'deleted==0');
	// 	params.set('order', 'id_parent,name');
	// 	return this.getData(this.baseURL, params);
	// }
	/*-----------------------------------------------------------------------------------------*/
	// getByParent(parent:any) {
	// 	let params = new HttpParams();
	// 	//params.set('filter[0]', 'deleted==0');
	// 	params.set('filter[1]', 'id_parent=='+parent);
	// 	if (parent===0) {
	// 		params.set('filter[2]', 'id=<>0');
	// 	}
	// 	params.set('per-page', '1000000');
	// 	params.set('order', 'weight, name');
	// 	return this.getData(this.baseURL, params);
	// }
	/*-----------------------------------------------------------------------------------------*/
	// save(item:any) {
	// 	let headers = new Headers({ 'Content-Type': 'application/json', 'Accept':'*/*', 'token': this._AppService.user.token });
	// 	let options = new RequestOptions({ headers: headers });
	//
	// 	if (item.id===null) {
	// 		//delete item.id;
	// 		let body = JSON.stringify(item);
	// 		return this._http.post(this.baseURL, body, options)
	// 			.map((res:Response) => res.json())
	// 			.catch(this.handleError);
	// 	} else {
	// 		let body = JSON.stringify(item);
	// 		return this._http.put(this.baseURL+'/'+item.id, body, options)
	// 			.map((res:Response) => res.json())
	// 			.catch(this.handleError);
	// 	}
	// }
	/*-----------------------------------------------------------------------------------------*/
	// delete(id:number) {
	// 	//let headers = new Headers({ 'Content-Type': 'application/json', 'Accept':'*/*' });
	// 	return this._http.delete(this.baseURL+'/'+id)
	// 		.map((res:Response) => res.json())
	// 		.catch(this.handleError);
	// }
	/*-----------------------------------------------------------------------------------------*/

}
