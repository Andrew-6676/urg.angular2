import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable }     from '@angular/core';
import { Observable }     from 'rxjs/Observable';

import { LoggerService } from './logger.service';
import { AppService }    from './app.service';

import { Service }    from './service';

@Injectable()
export class EquipmentService extends Service {

	protected baseUrl = this._AppService.baseURL+'equipment';
	protected equipmentByUrgUrl = this.baseUrl+'/listByUrg';

	constructor (public _http: HttpClient,
	             public _logger: LoggerService,
	             public _AppService: AppService) {
		super(_http,
			_logger,
			_AppService);
	}
	/*-------------------------------------------------------------------------------*/
	getListByUrgForRegimCard(id_urg: string): Observable<any> {
		let params = new HttpParams().set('expand','data')
									 .set('order','mount_point,order,id_obj');
		return this.getData(this.baseUrl+'/listForRegimCard/'+id_urg, params);
	}
	/*-------------------------------------------------------------------------------*/
	getListByUrg(id_urg: string): Observable<any> {
		let params = new HttpParams().set('expand','files,data')
									 .set('order','mount_point,order,id_obj');
		return this.getData(this.equipmentByUrgUrl+'/'+id_urg, params);
	}
	/*-------------------------------------------------------------------------------*/
	getById(id_instr:string): Observable<any> {
		let params = new HttpParams().set('expand','files');
		return this.getData(this.baseUrl+'/'+id_instr, params);
	}
	/*-------------------------------------------------------------------------------*/
	saveOrder(data: any[]) {
		return this.saveData(this.baseUrl+'/saveOrder', data);
	}
	/*-------------------------------------------------------------------------------*/
	saveRegimCard(data: any[]): Observable<any>{
		return this.saveData(this.baseUrl+'/saveRegimCard', data);
	}
	/*-------------------------------------------------------------------------------*/
	save(data: any, id_urg:string) {
		data.id_urg = id_urg;
		return super.save(data);
	}

}

