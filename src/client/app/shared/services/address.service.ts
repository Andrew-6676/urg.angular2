import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable }    from '@angular/core';
import { Observable }    from 'rxjs/Observable';

import { Service }       from './service';
import { LoggerService } from './logger.service';
import { AppService }    from './app.service';

@Injectable()
export class AddressService extends Service {

	//protected baseUrl = this._AppService.baseURL+'address';
	protected regionUrl = this.baseUrl+'regions';
	protected npUrl     = this.baseUrl+'nps';
	protected streetUrl = this.baseUrl+'streets';

	constructor (public _http: HttpClient,
	             public _logger: LoggerService,
	             public _AppService: AppService) {
		super(_http,
			_logger,
			_AppService);
	}
	/*-------------------------------------------------------------------------------*/
	getAllRegions(): Observable<any> {
		let params = new HttpParams().set('fields','id,name')
									 .set('per-page','1000');

		return this.getData(this.regionUrl, params);
	}
	/*-------------------------------------------------------------------------------*/
	getRegionsByName(term: string, id_opgh?:string): Observable<any> {
		let params = new HttpParams().set('fields','id,name')
									 .set('filter[0]','region=@'+term+'%');
		if (id_opgh)
			params = params.set('filter[1]','id_unit=='+id_opgh);

		return this.getData(this.regionUrl, params);
	}
	/*-------------------------------------------------------------------------------*/
	getNpByName(term: string, region?: number): Observable<any> {
		let params = new HttpParams().set('fields','id,name')
									 .set('filter[0]','np=@'+term+'%');
		if (region && region>0) {
			params = params.set('filter[1]','id_region=='+region);
		}
		return this.getData(this.npUrl, params);
	}
	/*-------------------------------------------------------------------------------*/
	getStreetByName(term: string, np?: number): Observable<any> {
		let params = new HttpParams().set('fields','id,name')
									 .set('filter[0]','street=@%'+term+'%');
		if (np && np>0) {
			params = params.set('filter[1]','id_np=='+np);
		}
		return this.getData(this.streetUrl, params);
	}
}

