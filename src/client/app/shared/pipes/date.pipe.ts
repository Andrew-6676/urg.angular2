import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'datePHP'})
export class DatePHPPipe implements PipeTransform {
	transform(value: string): string {
		let d = value.split('-');
		return d[2]+'.'+d[1]+'.'+d[0];
	}
}
