import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'asNumber'})
export class AsNumberPipe implements PipeTransform {
	transform(value: string): number {
		return <any>(value)*1;
	}
}
