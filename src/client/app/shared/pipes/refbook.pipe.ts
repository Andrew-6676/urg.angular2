import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'byParent'})
export class ByParentPipe implements PipeTransform {
	transform(value: any, id_parent: number) {
		return value.filter((record: any) => {return record.id_parent===id_parent || record.id_parent<0;} );
	}
}
