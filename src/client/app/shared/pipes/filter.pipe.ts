import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'modelFilter'})
export class FilterModelPipe implements PipeTransform {
	transform(value: any, id: number) {
		return value.filter((record: any) => {return record.id!==id;} );
	}
}
