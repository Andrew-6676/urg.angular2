import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule }                  from '@angular/common';
import { FormsModule }                   from '@angular/forms';
import { RouterModule }                  from '@angular/router';

import { BrowserModule }            from '@angular/platform-browser';

import { MatButtonModule }          from '@angular/material/button';
import { MatInputModule }           from '@angular/material/input';
import { MatDialogModule }          from '@angular/material/dialog';
import { MatListModule }            from '@angular/material/list';
import { MatIconModule }            from '@angular/material/icon';
import { MatCardModule }            from '@angular/material/card';
import { MatTooltipModule }         from '@angular/material/tooltip';
import { MatTabsModule }            from '@angular/material/tabs';
import { MatProgressBarModule }     from '@angular/material/progress-bar';
import { MatCheckboxModule }        from '@angular/material/checkbox';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatToolbarModule }         from '@angular/material/toolbar';
import { MatExpansionModule }       from '@angular/material/expansion';
import { MatButtonToggleModule }    from '@angular/material/button-toggle';

 import { /*BrowserAnimationsModule*/ NoopAnimationsModule} from '@angular/platform-browser/animations';

import { PaginationModule, PopoverModule, TypeaheadModule, AccordionModule, ModalModule } from 'ngx-bootstrap';

import { ToolbarComponent }              from './components/toolbar/toolbar.component';
import { NavbarComponent }               from './components/navbar/navbar.component';

import { NKDatetimeModule }              from './components/ng2-datetime/ng2-datetime.module';
import { HelpComponent }                 from './components/help/help';

import { ScrollingButtonsComponent }     from './components/scrolling-buttons/index';
import { AppService }                    from './services/app.service';
import { LoggerService }                 from './services/logger.service';
import { ReportsService }                from './services/reports.service';

import { DebugComponent }                from './components/debug/debug.component';
import { KeysPipe }                      from './pipes/keys.pipe';

import { DialogsModule }  from './components/dialogs/dialogs.module';
import { ToastrModule }   from 'ngx-toastr';
import { DndModule }      from 'ng2-dnd';
import { ByParentPipe }   from './pipes/refbook.pipe';
import { SelectModule }   from './components/ng2-select/select.module';

import { ChangelogComponent } from './components/changelog/changelog.component';

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		RouterModule,
        NoopAnimationsModule,
		//BrowserAnimationsModule,

		PaginationModule.forRoot(),
		TypeaheadModule.forRoot(),
		PopoverModule.forRoot(),
		AccordionModule.forRoot(),
		ModalModule.forRoot(),

		BrowserModule,

		MatButtonModule,
		MatInputModule,
		MatDialogModule,
		MatListModule,
		MatIconModule,
		MatCardModule,
		MatTooltipModule,
		MatTabsModule,
		MatProgressBarModule,
		MatCheckboxModule,
		MatProgressSpinnerModule,
		MatToolbarModule,
		MatExpansionModule,
		MatButtonToggleModule,

		NKDatetimeModule,

		SelectModule,
		DialogsModule,
		ToastrModule.forRoot({
			timeOut: 7000,
			positionClass: 'toast-bottom-right',
			newestOnTop: false,
			//preventDuplicates: true,
		}),

		DndModule.forRoot(),
	],
	declarations: [
		ToolbarComponent,
		NavbarComponent,
		ToolbarComponent,
		NavbarComponent,
		ScrollingButtonsComponent,
		HelpComponent,
		DebugComponent,
		KeysPipe,
		ByParentPipe,
		ChangelogComponent
	],
	entryComponents: [],
	exports: [
		ToolbarComponent,
		ChangelogComponent,
		NavbarComponent,
		CommonModule, FormsModule, RouterModule,
		HelpComponent,
		ScrollingButtonsComponent,

		PaginationModule,
		TypeaheadModule,
		PopoverModule,
		AccordionModule,
		ModalModule,

		MatButtonModule,
		MatInputModule,
		MatDialogModule,
		MatListModule,
		MatIconModule,
		MatCardModule,
		MatTooltipModule,
		MatTabsModule,
		MatProgressBarModule,
		MatCheckboxModule,
		MatProgressSpinnerModule,
		MatToolbarModule,
		MatExpansionModule,
		MatButtonToggleModule,

		NKDatetimeModule,
		DebugComponent,
		KeysPipe,
		ByParentPipe,

		SelectModule,
		DialogsModule,
		ToastrModule,
		DndModule,
	]
})
export class SharedModule {
	static forRoot(): ModuleWithProviders {
		return {
			ngModule: SharedModule,
			providers: [
				AppService,
				LoggerService,
				ReportsService,
			]
		};
	}
}
