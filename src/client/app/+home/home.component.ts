import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { AppService }     from '../shared/services/app.service';
import { LoggerService }  from '../shared/services/logger.service';
import { UrgService }     from '../shared/services/urg.service';
import { UnitService }    from '../shared/services/unit.service';
/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
	moduleId: module.id,
	selector: 'sd-home',
	templateUrl: 'home.component.html',
	styleUrls: ['home.component.css'],
	providers: [UrgService]
})

export class HomeComponent implements OnInit {

	newName: string = '';
	errorMessage: string;
	urgsCount: number;

	data: any;

	chrome = true;
	/**
	 * Creates an instance of the HomeComponent with the injected
	 * AppService.
	 *
	 * @param {AppService} _AppService - The injected AppService.
	 * @param {LoggerService} _logger - The injected AppService.
	 * @param {UrgService} _urgService - The injected AppService.
	 * @param {UnitService} _unit - The injected UnitService.
	 * @param {ActivatedRoute} _route - The injected ActivatedRoute.
	 */
	constructor(public _AppService: AppService,
	            private _logger: LoggerService,
	            private _urgService: UrgService,
	            private _unit: UnitService,
	            private _route: ActivatedRoute
	) {
		this._AppService.app.pageTitle = '<%= APP_TITLE %>';
		this.chrome = navigator.userAgent.indexOf('Chrome')>-1;
	}

	/**
	 * Get the names OnInit
	 */
	ngOnInit() {
		//this.getUrgsCount();
		this._route
			.queryParams
			.subscribe(params => {
				if (params['sid']) {
					this._AppService.sid = params['sid'];
					this._logger.debug('sid =', params['sid']);
					//this._AppService.login();
				}

				//this.accesstoken = params['#access_token'];
				//this.code = params['code'];
			});
		this.getStat();
	}

	/**
	 * Handle the nameListService observable
	 */
	getUrgsCount() {
		this._urgService
			.countAll()
		    .subscribe(
		        (resp:any) => this.urgsCount = resp,
		        (error:any) =>  this.errorMessage = <any>error
		 );
	}

	getStat() {
		this._AppService.loadingInProgress(1);
		this._urgService
			.getStat()
			.subscribe(
				data => {
                    this._AppService.loadingInProgress(-1);
					//this._logger.debug(JSON.stringify(data));
					this.data = data;
				},
				(error:any) => {
                    this._AppService.loadingInProgress(-1);console.log(error);},
				() => {
					//this._AppService.loadingInProgress(-1);
				}
			)
	}
}
