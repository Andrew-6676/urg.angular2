import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';

import { SharedModule }   from '../shared/shared.module';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent }     from './home.component';


//import { AppService }     from '../shared/services/app.service';


@NgModule({
	imports: [CommonModule, SharedModule.forRoot(), HomeRoutingModule],
	declarations: [HomeComponent],
	exports: [HomeComponent],
	//providers: [AppService]
})
export class HomeModule {
}
