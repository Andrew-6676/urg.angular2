import { NgModule }      from '@angular/core';
import { RouterModule }  from '@angular/router';
import { HomeComponent } from './home.component';
//import { AuthGuard }     from '../shared/services/auth-guard.service';

@NgModule({
	imports: [
		RouterModule.forChild([
			{path: '', component: HomeComponent},
			// {
			// 	path: ':sid',
			// 	component: HomeComponent,
			// 	//canActivate: [AuthGuard]
			// },
		])
	],
	exports: [RouterModule]
})
export class HomeRoutingModule {
}
