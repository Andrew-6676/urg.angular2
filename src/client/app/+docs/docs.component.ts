import { ActivatedRoute }    from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';

import { AppService }     from '../shared/services/app.service';
import { LoggerService }  from '../shared/services/logger.service';
import { UrgService }     from '../shared/services/urg.service';
import { UnitService }    from '../shared/services/unit.service';
import { RefbookService } from "../shared/services/refbook.service";
import { ObjectIterator } from "../shared/models/object.iterator";
import { NgTableHeader }  from "../shared/components/ng-table/ng-table.component";

import { ScheduleService, Term } from "../shared/components/schedule/schedule.service";
import { ScheduleComponent }     from "../shared/components/schedule/schedule.component";

import * as _ from "lodash";

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
	moduleId: module.id,
	selector: 'sd-docs',
	templateUrl: 'docs.component.html',
	styleUrls: ['docs.component.css'],
	providers: [UrgService]
})

export class DocsComponent implements OnInit {
	@ViewChild('schedule') scheduleComponent: ScheduleComponent;
	Term = Term;

	newName: string = '';
	errorMessage: string;
	urgsCount: number;

	groups: { [key: string]: any } = {};		// группы объектов
	objects = new ObjectIterator();		// объекты
	obj_headers: { [key: string]: any } = new ObjectIterator();	// заголовки столбцов
	works: { [key: string]: any } = new ObjectIterator();		// работы, на которые составляется график

	//worksCfg: Map<number, Map<number, any>> = new Map();
	worksCfg: any;
	// для выборки объектов
	filter: any = new Object(null);
	unit: any = [];
	masters: any = [];
	search_result: { headers: NgTableHeader[], data: any[], totalCount: number } = {
		headers: [
			{field: null, title: '-', sort: false},
			{field: 'type.name', title: 'Тип', sort: true},
			{field: 'num', title: 'Номер', sort: true},
			{field: 'master.name', title: 'Ответственный', sort: true},
		],
		data: null,
		totalCount: 0
	};

	inProgress: boolean = false;
	selected: Map<number, any> = new Map();
	graphObj: Map<number, any> = new Map();
	graphObj_arr : any[] = [];

	datepickerOpts: any = {
		placeholder: 'Выберите дату',
		//assumeNearbyYear: true,
		startView: 'months',
		minViewMode: 'months',
		format: 'MM yyyy',
		//format: 'dd.mm.yyyy',
		weekStart: 1,
		todayHighlight: true,
		todayBtn: 'linked',
		language: 'ru',
		daysOfWeekHighlighted: [0,6],
		autoclose: true,
		icon: 'glyphicon glyphicon-calendar'
	};

	id_unit_schedule: any = this._AppService.app.currentUnit.id;
	dataPrepared: boolean = false;

	showSelected: boolean = false;

	/**
	 * Creates an instance of the HomeComponent with the injected
	 * AppService.
	 *
	 * @param {AppService} _AppService - The injected AppService.
	 * @param {LoggerService} _logger - The injected AppService.
	 * @param {UrgService} _urgService - The injected AppService.
	 * @param {UnitService} _unitService - The injected UnitService.
	 * @param {ActivatedRoute} _route - The injected ActivatedRoute.
	 * @param {RefbookService} _refbookService - The injected RefbookService.
	 * @param {ScheduleService} _scheduleService - The injected ScheduleService.
	 */
	constructor(public _AppService: AppService,
	            private _logger: LoggerService,
	            private _urgService: UrgService,
	            private _unitService: UnitService,
	            private _route: ActivatedRoute,
	            public _refbookService: RefbookService,
	            public _scheduleService: ScheduleService) {
		this._AppService.app.pageTitle = '<%= APP_TITLE %>';

		this.filter = {
			id_unit: this._AppService.app.currentUnit ? this._AppService.app.currentUnit.id : null,
			id_type_urg: -1,
			expand: [],
			sort: new Object(null),
			page: {
				currentPage: 1,
				itemsPP: 50,
			}
		};

		this.getUnit(<any>this._AppService.app.currentUnit.id);
		this.getMasters(this._AppService.app.currentUnit.id);

			// работы
		this._refbookService
			.getByParent(61)
			.subscribe(
				resp => {
						// TODO: пока покажу только обходы - 1185
					//this._logger.debug('works', resp);
					this.getWorksCfg();
					for (let i in resp) {
						if (resp[i].id != 1185) continue;
						this.works[resp[i].id] = {
							id: resp[i].id,
							name: resp[i].name,
							color: resp[i].descr,
						};
					}
				},
			);
	}

	ngOnInit() {
		//this.getUrgsCount();
		this.groups = {
			1: {name: 'Виноградов В.О.'},
			2: {name: 'Корнеев А.А.'},
		};
		/*---------------------------------------*/
		this.obj_headers = [
			'ФИО',
			'Номер КРД',
			'Адрес',
			'Регулятор',
			'Выходное давление',
		];
		/*---------------------------------------*/
		this.objects['4440892'] = {
			id: 4440892,
			id_group: 1,
			id_type_urg: 1005,
			name: 'КРД-1',
			data: [
				'г. Витебк, ул.Уличная, 22',
				'РДНК-400',
				'низкое',
			],
			// по нужным видам работ 3-5 последних дат выполнения работ, отсортированных по убыванию - {id_work : [last_date, pre-last_date,...]}
			// 1184 - id работы из справочника
			'works': {
				1185: [new Date('2017-12-15'), new Date('2017-11-16')],
			}
		};

		this.objects['4442329'] = {
			id: 4442329,
			id_group: 1,
			id_type_urg: 1005,
			name: 'КРД-31',
			data: [
				'г. Витебк, ул.вапвапвап, 2',
				'РДС-32',
				'среднее',
			],
			works: {
				1185: [new Date('2017-12-12')],
			}
		};
		this.objects['44'] = {
			id: 44,
			id_group: 2,
			id_type_urg: 1003,
			name: 'ШРП-55',
			data: [
				'г. Витебк, ул.Сатанинская, 666',
				'РДНК-400',
				'низкое',
			],
			works: null
		};

	}

	/*------------------------------------------------------------------------------------------------*/
	typeaheadMasterOnSelect(event: any) {
		this._logger.debug('master select =', event);
		this.filter.master = event.item.name;
	}

	/*------------------------------------------------------------------------------------------------*/
	getUnit(id: any) {
		this._unitService
			.getCompany(<any>this._AppService.user.unit.id)
			.subscribe(
				(resp: any) => {
					this.unit = resp;
					this._logger.log(resp);
					this._logger.debug('units loaded');
				},
				(error: any) => this.errorMessage = <any>error
			);
	}

	/*------------------------------------------------------------------------------------------------*/
	getMasters(id_unit: any) {
		this._AppService.loadingInProgress(1);
		this.filter.master = '-';
		this._urgService
			.searchMaster('', id_unit)
			.subscribe(
				resp => {
					this.masters = resp;
					this.masters.unshift({name: '-', post: null});
					this._AppService.loadingInProgress(-1);
					//this._logger.debug('-------------- masters', this.masters);
				},
				error => {
					console.log('ERROR!', error);
					this.masters = [{name: '-', post: null}];
					this._AppService.loadingInProgress(-1);
					this._AppService.toastrOpen('Ошибка получения списка мастеров', 'error')
				}
			);
	}

	/*------------------------------------------------------------------------------------------------*/
	search() {
		this.inProgress = true;
		this._AppService.loadingInProgress(1);
		this.filter.expand = ['master', 'type', 'full_address'];
		this._urgService
			.search(this.filter)
			.subscribe(
				resp => {
					this._AppService.toastrOpen('Данные получены', 'info');
					this._AppService.loadingInProgress(-1);
					this.search_result.data = resp.length > 0 ? resp : null;
					this.inProgress = false;
					this.search_result.totalCount = this._urgService.headers.get('X-Pagination-Total-Count');
					this.selected.clear();
				},
				error => {
					this.inProgress = false;
					console.log('ERROR!', error);
					this.search_result.data = [];
					this._AppService.loadingInProgress(-1);
					this._AppService.toastrOpen('Ошибка получения данных', 'error')
				},
			)
	}

	/*------------------------------------------------------------------------------------------------*/
	clearSearch() {
		this.search_result.data = [];
	}
	/*------------------------------------------------------------------------------------------------*/
	sortSearchResult(event: any) {
		this._logger.debug('onSort:', event);
		this.filter.sort = event;
		this.search();
	}

	/*------------------------------------------------------------------------------------------------*/
	searchPageChanged(event: any) {
		this.filter.page.currentPage = event.page;
		this.filter.page.itemsPP = event.itemsPerPage;
		this.search();
	}
	/*------------------------------------------------------------------------------------------------*/
	addToGraph() {
		this._logger.debug('add to graph');
		this.selected.forEach((val, key) => {
			this.graphObj.set(key, val);
		});
	}

	/*------------------------------------------------------------------------------------------------*/
	showGraphObj() {
		this.showSelected = !this.showSelected;
		this.graphObj_arr = [];
		if (this.showSelected) {
			this.graphObj.forEach((value, key) => {
				this.graphObj_arr.push(value);
			});
		}

		this._logger.debug('graph_obj', this.graphObj);
		return false;
	}
	/*------------------------------------------------------------------------------------------------*/
	deleteFromGraphObj(id: number) {
		this.graphObj.delete(id);
		_.remove(this.graphObj_arr, (e:any) => {return e.id == id});
		return false;
	}
	/*------------------------------------------------------------------------------------------------*/
	clearGraphObj() {
		this.showSelected = false;
		this.graphObj.clear();
		this.graphObj_arr = [];
		return false;
	}

	/*------------------------------------------------------------------------------------------------*/
	getWorksCfg() {
		this._AppService.loadingInProgress(1);
		this._scheduleService
			.loadWorksCfg()
			.subscribe(
				resp => {
					this._AppService.loadingInProgress(-1);
					if (resp.status=='ok') {
						this.worksCfg = resp.worksCfg;
						// this.worksCfg = new Map();
						// for (let w in resp.worksCfg) {
						// 	let map: Map<number, any> = new Map();
						// 	for (let u in resp.worksCfg[w]) {
						// 		map.set(<any>u*1, JSON.parse(resp.worksCfg[w][u]));
						// 	}
						// 	this.worksCfg.set(<any>w*1, map);
						// }
						this._logger.debug('loaded workCfg', this.worksCfg);
						//this._logger.debug('loaded workCfg', this.worksCfg.get(1002).get(1184).p    );
						this._AppService.toastrOpen('Параметры работ по УРГ загружены', 'info');
					} else {
						this._AppService.toastrOpen('Ошибка загрузки параметров работ по УРГ', 'error', 0);
					}
				},
				error => {
					this._AppService.loadingInProgress(-1);
					this._AppService.toastrOpen(<any>error, 'error', 0);
				}
			);
	}
	/*------------------------------------------------------------------------------------------------*/
	saveWorksCfg() {
		this._AppService.loadingInProgress(1);
		let data: any = new Object(null);
		data = this.worksCfg;
		this._scheduleService
			.saveWorksCfg(data)
			.subscribe(
				resp => {
					this._AppService.loadingInProgress(-1);
					if (resp.status=='ok') {
						this._AppService.toastrOpen('Параметры работ по УРГ сохранены', 'success');
					} else {
						this._AppService.toastrOpen('Ошибка сохранения параметров работ по УРГ', 'error', 0);
					}
				},
				error => {
					this._AppService.loadingInProgress(-1);
					this._AppService.toastrOpen(<any>error, 'error', 0);
				}
			);
	}
	/*------------------------------------------------------------------------------------------------*/
	prepareData() {
		// передать параметры на сервер для формирования структур для построения графика

		/*<u-schedule
		    [id_unit]="id_unit_schedule"
			[objects]="objects"
			[obj_headers]="obj_headers"
			[groups]="groups"
			[works]="works
		*/

		let data = {
			objs: <any>[],
			period: new Date().toISOString().substr(0,10),
			term: this.Term.month,
			group: 'master.name',
		};

		this.graphObj.forEach((value, key) => {
			data.objs.push(value.id);
		});

		this._scheduleService
			.getPreparedData(data)
			.subscribe(
				(resp:any) => {
					if (resp.status=='ok') {
						this.objects = resp.data.objects;
						this.groups = resp.data.groups;
						this.obj_headers = resp.data.obj_headers;
						this.dataPrepared = true;
						setTimeout(()=>{
							this.scheduleComponent.generateBtnClick();
						}, 100);
					} else {
						this._AppService.toastrOpen(resp.message, 'error', 0);
					}
				},
				(error:any) => {
					this._AppService.toastrOpen(error, 'error', 0);
				}
			);
	}
	/*------------------------------------------------------------------------------------------------*/
	openSavedSchedule() {
		// перейти на вкладку загрузки сохранённого графика
		this.dataPrepared = true;
		setTimeout(() => {
			this.scheduleComponent.tabs.selectedIndex = 1;
		}, 100);

	}
	/*------------------------------------------------------------------------------------------------*/
	trackByFn(index:any, item:any) {
		return item.id;
		//return index;
	}
}