import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';

import { SharedModule }      from '../shared/shared.module';
import { DocsRoutingModule } from './docs-routing.module';
import { DocsComponent }     from './docs.component';
import { ScheduleModule }    from '../shared/components/schedule/schedule.module';
import { NgTableComponent }  from '../shared/components/ng-table/ng-table.component';


@NgModule({
	imports: [
		CommonModule, SharedModule.forRoot(), DocsRoutingModule,
		ScheduleModule
	],
	declarations: [DocsComponent, NgTableComponent],
	exports: [DocsComponent],
	//providers: [AppService]
})
export class DocsModule {}
