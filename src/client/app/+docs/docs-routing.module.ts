import { NgModule }      from '@angular/core';
import { RouterModule }  from '@angular/router';
import { DocsComponent } from './docs.component';
//import { AuthGuard }     from '../shared/services/auth-guard.service';

@NgModule({
	imports: [
		RouterModule.forChild([
			{path: 'docs', component: DocsComponent},
			// {
			// 	path: ':sid',
			// 	component: HomeComponent,
			// 	//canActivate: [AuthGuard]
			// },
		])
	],
	exports: [RouterModule]
})
export class DocsRoutingModule {
}
