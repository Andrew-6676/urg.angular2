import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';

import { SharedModule }   from '../shared/shared.module';
import { AppService }     from '../shared/services/app.service';

import { RefbookComponent }     from './refbook.component';
import { RefbookRoutingModule } from './refbook-routing.module';

@NgModule({
	imports: [CommonModule, SharedModule, RefbookRoutingModule],
	declarations: [RefbookComponent],
	exports: [RefbookComponent],
	providers: [AppService]
})
export class RefbookModule {
}
