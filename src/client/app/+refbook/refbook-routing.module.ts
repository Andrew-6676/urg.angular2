import { NgModule }         from '@angular/core';
import { RouterModule }     from '@angular/router';
import { RefbookComponent } from './refbook.component';
import { AuthGuard }        from '../shared/services/auth-guard.service';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'refbooks',
        component: RefbookComponent,
        canActivate: [AuthGuard]
      }
    ])
  ],
  exports: [RouterModule]
})
export class RefbookRoutingModule { }
