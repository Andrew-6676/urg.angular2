import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';

import { AppService }     from '../shared/services/app.service';
import { LoggerService }  from '../shared/services/logger.service';
import { RefbookService } from '../shared/services/refbook.service';
import { DialogsService } from '../shared/components/dialogs/dialogs.service';

@Component({
	moduleId: module.id,
	selector: 'sd-home',
	templateUrl: 'refbook.component.html',
	styleUrls: ['refbook.component.css']
})

export class RefbookComponent implements OnInit {

	@ViewChild('modal_save') modal_save: ModalDirective;
	@ViewChild('modal_delete') modal_delete: ModalDirective;

	refbooks: any[] = [];
	items: Item[] = [];

	eqItems: { [spr: string]: any; } = { };

	reloadInProgress = false;

	currentTab: number = 0;
	currentRefbookName: string = '';
	currentRefbook: number = 0;
	currentEq : number = -1;
	currentOtherRefbook: string = '';

	edit_item: Item = new Item(this._logger, this._refbookService);

	errorMessage: string;

	constructor(public _AppService: AppService,
	            public _refbookService: RefbookService,
	            private _logger: LoggerService,
	            private _dialogService: DialogsService
	) {
		//this._AppService.app.pageTitle = '<%= APP_TITLE %>';
	}

	/**
	 * получить список справочников при инициализации
	 */
	ngOnInit() {
		this._AppService.app.pageTitle = 'Справочники';
		this.getRefs();
	}
	/*-----------------------------------------------------------------------------------------------*/
	getRefs() {
		this._refbookService
			.getByParent(0)
			.subscribe(
				(resp:any) => this.refbooks = resp,
				(error:any) => this.errorMessage = error,
				() => {
					this._logger.log('refbooks list loaded');
				}
			);
	}
	/*-----------------------------------------------------------------------------------------------*/
	eqRefSelect(eq: any) {
		this.reloadInProgress = true;
		this.currentEq = eq;
		this.currentRefbookName = this._refbookService.equipment[this.currentEq] ? this._refbookService.equipment[this.currentEq].name : '-';
		this._refbookService
			.get_spr_data(this.currentEq, true)
			.subscribe(
				res => {
					//this.eqItems = {};
					this.eqItems = res;
					this.reloadInProgress = false;
					this._logger.debug('eqSpr loaded =', this.currentEq);
				}
			);
		return false;
	}
	/*-----------------------------------------------------------------------------------------------*/
	refOtherSelect(ref: string) {
		this.currentOtherRefbook = ref;
	}
	/*-----------------------------------------------------------------------------------------------*/
	refSelect(parent:any) {
		this.reloadInProgress = true;
		this.currentRefbook = parent;
		this._logger.log('select '+parent);
		this._refbookService
			.getByParent(this.currentRefbook)
			.subscribe(
				(resp:any) => {
					let data: any;
					data = resp;
					//this.data2 = data.items;
					this.items = [];
					for (let item of data) {
						this.items.push(new Item(this._logger, this._refbookService, item));
					}
					this.reloadInProgress = false;
					this.currentRefbookName = this._refbookService.refbooks[this.currentRefbook] ? this._refbookService.refbooks[this.currentRefbook].name : '-';
				},
				(error:any) => this.errorMessage = error,
				() => {
					this._logger.debug('subRefbook loaded');
				}
			);
		return false;
	}
	/*-----------------------------------------------------------------------------------------------*/
	showEditDialog(item:any) {
		if (!this._AppService.checkRole('edit')) {
			return false;
		}
		this._logger.debug(item.data.id+' - '+item.data.name);
		this.edit_item = item;
		this.modal_save.show();
		return false;
	}
	/*-----------------------------------------------------------------------------------------------*/
	tabChange(tab:any) {
		this.currentTab = tab.index;
	}
	/*-----------------------------------------------------------------------------------------------*/
	showDeleteDialog(item:any) {
		this.edit_item = item;
		//this.modal_delete.show();

		this._dialogService
			.confirm('Удаление', 'Вы уверены, что хотите удалить запись?')
			.subscribe(
				(resp:any) => {
					if (resp) {
						this.deleteItem();
					}
				}
			);

	}
	/*-----------------------------------------------------------------------------------------------*/
	refreshData() {
		switch (this.currentTab) {
			case 0:
					// обновить если общие справочники
				this.getRefs();
				this.refSelect(this.currentRefbook);
				break;
			case 1:
					// обновить спрвочники оборудования
				this._refbookService.loadTypeObj();
				break;
		}

	}
	/*-----------------------------------------------------------------------------------------------*/
	addItem() {
		this._logger.debug('add new record');
		this.edit_item = new Item(this._logger, this._refbookService);
		this.edit_item.data.id_parent = this.currentRefbook;
		this.modal_save.show();
	}
	deleteItem() {
		this._logger.warn('удалить ', this.edit_item.data);
		this._refbookService
			.delete(this.edit_item.id)
			.subscribe(
				(resp:any) => {
					this._logger.log(JSON.stringify(resp));
					this.refSelect(this.currentRefbook);
					this._logger.debug('ref deleted');
					this._refbookService.loadAllData();
				},
				(error:any) => this.errorMessage = error
			);
	}
	saveItem() {
		this._logger.warn('сохранить ', this.edit_item.data);
		this._refbookService
			.save(this.edit_item.data)
			.subscribe(
				(resp:any) => {
					this._logger.log(JSON.stringify(resp));
					this.refSelect(this.currentRefbook);
					this._logger.debug('ref saved');
					this._refbookService.loadAllData();
				},
				(error:any) => this.errorMessage = error
			);
	}

	/*---------------------------------------------------------*/
	/*---------------------------------------------------------*/
	/*---------------------------------------------------------*/
	showEqDeleteDialog() {

	}
	/*---------------------------------------------------------*/
	showEqEditDialog() {

	}
	/*---------------------------------------------------------*/
	addType() {

	}
	/*---------------------------------------------------------*/
	addMake() {

	}
	/*---------------------------------------------------------*/
	addModel(id_eq_type: number) {
		return false;
	}
	/*---------------------------------------------------------*/
	addInstruction(id_model: number) {
		return false;
	}
}

/*---------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------*/

class Item {
	public id: number;
	public data: ItemData = new ItemData();
	public errorMessage: string;

	constructor (
		private _logger: LoggerService,
		private _refbookService: RefbookService,
		data?: ItemData
	) {
		if (data !== undefined) {
			//this._logger.log(data);
			this.data = data;
			this.id = data.id;
		} else {
			//console.log('null');
		}

	}
}

class ItemData {
	id: number = null;
	id_parent: number = 0;
	name: string = '';
	descr: string = '';
}
