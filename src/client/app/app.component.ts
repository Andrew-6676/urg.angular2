import { Component }     from '@angular/core';
import { Config }        from './shared/config/env.config';
import { LoggerService } from './shared/services/logger.service';
import { AppService }    from './shared/services/app.service';

import './operators';

/**
 * This class represents the main application component.
 */
@Component({
  moduleId: module.id,
  selector: 'urg-app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
})
export class AppComponent {
  //env: string = Config.ENV;
  constructor(
	  public _AppService: AppService,
	  public _logger: LoggerService
  ) {
    console.log('Environment config', Config);
  }

}
