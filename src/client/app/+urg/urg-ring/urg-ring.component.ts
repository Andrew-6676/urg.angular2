import { Component }     from '@angular/core';
import { ActivatedRoute, Router }  from '@angular/router';
import 'rxjs/add/observable/of';

import { AppService }     from '../../shared/services/app.service';
import { LoggerService }  from '../../shared/services/logger.service';
import { UrgService }     from '../../shared/services/urg.service';
import { UnitService }    from '../../shared/services/unit.service';
import { RingService }    from '../../shared/services/ring.service';
import { RefbookService } from '../../shared/services/refbook.service';
import { AddressService } from '../../shared/services/address.service';

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
	moduleId: module.id,
	selector: 'urg-ring',
	templateUrl: 'urg-ring.component.html',
	styleUrls: ['urg-ring.component.css'],
	providers: [UrgService, AddressService, RingService],
})

export class UrgRingComponent /*implements OnInit */ {

	rings: {[key:number]:any[]} = {};
	rings2:any[] = [];

	errorMessage: string;

	constructor(public _AppService: AppService,
	            private _logger: LoggerService,
	            private _urgService: UrgService,
	            private _aroute: ActivatedRoute,
	            private _router: Router,
	            private _refbookService: RefbookService,
	            private _addressService: AddressService,
	            private _unitService: UnitService,
	            private _ringService: RingService
	) {
		this._AppService.app.pageTitle = 'Закольцованные УРГ';
		this.getAllRings();
	}
	/*------------------------------------------------------------------------------------*/
	getAllRings() {
		this._AppService.loadingInProgress(1);
		this._ringService
			.getRingsList()
			.subscribe(
				(resp:any) => {
                    this._AppService.loadingInProgress(-1);
					//this.rings = resp;
					this.rings2 = [];
					this.rings = {};
					for (let i in resp) {
						if (!this.rings[resp[i].id_ring]) this.rings[resp[i].id_ring] = [];
						this.rings[resp[i].id_ring].push(resp[i]);
					}

					for (let key in this.rings) {
						this.rings2.push(this.rings[key]);
					}
				},
				(error:any) => {
                    this._AppService.loadingInProgress(-1);this._logger.error(error)},
				() => {
					//this._AppService.loadingInProgress(-1);
				}
			);
	}
	/*------------------------------------------------------------------------------------*/
	/*------------------------------------------------------------------------------------*/
		// TODO: эта функция есть в urg-form
	viewUrg(id: string) {
		this._logger.log('view urg', id);
		this._router.navigateByUrl('/urg/view/'+id);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
		// TODO: эта функция есть в urg-form
	editUrg(id: string) {
		this._logger.log('edit urg', id);
		this._router.navigateByUrl('/urg/edit/'+id);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
		// TODO: эта функция есть в urg-form
	excludeFromRing(urg: any) {
		if (!confirm('Действительно хотите исключить '+urg.urg+'?')) return false;
		this._logger.log('exclude from ring id =', urg.id_urg);
		this._ringService
			.excludeFromRing(urg.id_urg)
			.subscribe(
				(resp:any) => {
					this._logger.debug(urg.urg+' excluded from ring');
					this.getAllRings();
				},
				(error:any) => this.errorMessage = <any>error
			);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	trackByFn(index:any, item:any) {
		return index;
	}
	/*------------------------------------------------------------------------------------*/
}

