import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule }           from '@angular/core';
import { CommonModule }       from '@angular/common';

import { UrgRoutingModule }   from './urg-routing.module';
import { SharedModule }       from '../shared/shared.module';
import { SelectModule }       from '../shared/components/ng2-select/select.module';

import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';

import { UrgComponent }       from './urg.component';
import { UrgTableComponent }  from './urg-table/urg-table.component';
import { UrgFormComponent }   from './urg-form/urg-form.component';
import { UrgViewComponent }   from './urg-view/urg-view.component';
import { RegimCardComponent } from './urg-view/regim-card/regim-card.component';
import { UrgRingComponent }   from './urg-ring/urg-ring.component';

import { UrgEquipmentComponent }       from './urg-equipment/urg-equipment.component';
import { UrgInstrumentationComponent } from './urg-instrumentation/urg-instrumentation.component';
import { FilePreviewComponent }        from '../shared/components/file-preview/file-preview.component';

import { AsNumberPipe }       from '../shared/pipes/number.pipe';
import { FilterModelPipe }    from '../shared/pipes/filter.pipe';

import { FilesService }       from '../shared/services/files.service';



//import { B4WComponent }       from '../shared/components/b4w/b4w';


@NgModule({
	imports: [
		CommonModule,
		SharedModule,
		SelectModule,
		FormsModule,
		ReactiveFormsModule,
        UrgRoutingModule,
		MultiselectDropdownModule
	],
	declarations: [
		UrgComponent,
		UrgTableComponent,
		UrgFormComponent,
		UrgViewComponent,
		RegimCardComponent,
		UrgEquipmentComponent,
		UrgInstrumentationComponent,
		UrgRingComponent,
		//ByParentPipe,
		AsNumberPipe,
		FilterModelPipe,
		FilePreviewComponent,
		// B4WComponent
	],
	exports: [UrgComponent],
	providers: [
		FilesService
	]
})

export class UrgModule {}

