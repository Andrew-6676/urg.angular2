/**
 * This barrel file provides the export for the lazy loaded HomeComponent.
 */
export * from './urg.component';
export * from './urg-view/urg-view.component'
export * from './urg-form/urg-form.component';
export * from './urg-ring/urg-ring.component';
