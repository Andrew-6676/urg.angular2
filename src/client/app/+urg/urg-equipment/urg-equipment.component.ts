import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ModalDirective }   from 'ngx-bootstrap';

import { AppService }     from '../../shared/services/app.service';
import { LoggerService }  from '../../shared/services/logger.service';
import { UrgService }     from '../../shared/services/urg.service';
import { RefbookService } from '../../shared/services/refbook.service';
import { DialogsService } from '../../shared/components/dialogs/dialogs.service';

import { UrgFormComponent } from '../urg-form/urg-form.component';
import { Equipment }        from '../../shared/models/equipmen';


import * as _ from "lodash";
/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
	moduleId: module.id,
	selector: 'urg-equipment',
	templateUrl: 'urg-equipment.component.html',
	styleUrls: ['urg-equipment.component.css'],
	providers: [UrgService]
})

export class UrgEquipmentComponent implements OnInit {
	@Input() equipment: any = [];
	@Input() type: string;
	@Input() parent: UrgFormComponent;

	@ViewChild('modal_equipment') modal_equipment: ModalDirective;

	errorMessage: string;

	types = {
		enter: 'Вход',
		line: 'Линия редуцирования',
		exit: 'Выход',
		bypass: 'Байпас',
		heating: 'Линия отопления'
	};

	tmp_equipment: Equipment = new Equipment();

	typeRefbook : any = {};
	makeRefbook : any = {};
	modelRefbook : any = {};

	sortMode = true;
	sorted: {[key:string]: boolean} = {};
	/**
	 * Creates an instance of the EquipmentComponent with the injected
	 * services.
	 *
	 * @param {AppService} _AppService - The injected AppService.
	 * @param {LoggerService} _logger - The injected LoggerService.
	 * @param {RefbookService} _refbookService - The injected RefbookService.
	 * @param {DialogsService} _dialogsService - The injected DialogsService.
	 */
	constructor(public _AppService: AppService,
	            private _logger: LoggerService,
	            public  _refbookService: RefbookService,
	            private  _dialogsService: DialogsService,
	) {}
	/*------------------------------------------------------------------------------------*/
	ngOnInit() {
		//this.getUnits();
		//this.getUrgs();
		//this.getItems(this.unit_id);
	}
	/*------------------------------------------------------------------------------------*/
	/**
	 * @param  parent - к чему добавляется оборудование: линия редуцирования, вход или выход
	 * @param  type - тип добавляемого оборудования.
	 * @param  i - номер входа, линии, выхода, к которому добавляется оборудование.
	 */
	addEquipment(parent: any, type: any, i:number=0) {
		this._logger.log('add equipment('+type+') to ', parent.type+'#'+(i+1));

		// this.selectEqType(type);

		this._refbookService.get_spr_data(type)
			.subscribe(
				res => {
					this.makeRefbook = res.make;
					this.modelRefbook = res.model;
					this.typeRefbook = res.type;
					//this._logger.debug(this.modelRefbook);
				}
		);

		this.tmp_equipment = new Equipment();
		this.tmp_equipment.id_eq_type = type;
		// //this.selectEqType(type);
		this.tmp_equipment.mount_point = parent.type+'#'+(i+1);
        this.tmp_equipment.prev_mnt = this.tmp_equipment.mount_point;
		this.parent.equipment[parent.type][i].devices.push(this.tmp_equipment);
		this.modal_equipment.show();

		return false;
	}
	/*------------------------------------------------------------------------------------*/
	deleteFile(item: any, file: any) {
		this.parent.deleteFile(file, () => {
			_.remove(item.files, (e:any) => {return e.id == file.id});
		})
	}
	/*------------------------------------------------------------------------------------*/
	addFileEquipment(device:Equipment) {
		// this.parent.fileToEquipment = {eq_id: device.id};
		// this.parent.modal_upload_equipment.show();
		this._dialogsService
			.upload('Загрузка файла',
				'',
				{
					id_obj: device.id,
					id_unit: this._AppService.app.currentUnit.id,
					url: this._AppService.baseURL+'file/upload',
				})
			.subscribe(
				(resp:any) => {
					this._logger.debug('confirm result =', resp);
					if (resp)
						this.parent._equipmentService
							.getById(device.id)
							.subscribe(
								(resp:any) => {
									device.files = resp.files;
								}
							);
				}
			);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	editEquipment(equipment: any) {
		//this.selectEqType(equipment.id_eq_type);
		if (equipment.data==null) {
			this._dialogsService
				.warning('Предупреждение','Оборудование было сохранено не полностью<br>Удалите его и добавте заново.')
				.subscribe();
			return false;
		}
		this._refbookService
			.get_spr_data(equipment.id_eq_type)
			.subscribe(
				res => {
					this.makeRefbook  = res.make;
					this.modelRefbook = res.model;
					this.typeRefbook  = res.type;
					//this._logger.debug(this.modelRefbook);
				}
			);
		this._logger.log('edit equipment=', equipment);
		this.tmp_equipment = equipment;
        this.tmp_equipment.prev_mnt = equipment.mount_point;
		if (equipment.id_eq_type==4) {
			this.tmp_equipment.id_make = -99;
		} else {
			this.tmp_equipment.id_make = equipment.data.model.id_make;
		}
		this.modal_equipment.show();
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	cancelSaveEquipment(equipment: Equipment) {
		this._logger.log('cancel save equipment', equipment);

        this.tmp_equipment.mount_point = this.tmp_equipment.prev_mnt;
		// удалить оборудование с id=-1
		let mountPoint = equipment.mount_point.split('#');
		let eq = this.parent.equipment[mountPoint[0]][<any>mountPoint[1]-1].devices;
        _.remove(eq, (e:any) => {return e.id<0});
	}
	/*------------------------------------------------------------------------------------*/
	saveEquipment(equipment: Equipment) {
		this._AppService.loadingInProgress(1);
		this.parent._equipmentService
			.save(equipment, this.parent.urg.id)
			.subscribe(
				(resp:any) => {
                    this._AppService.loadingInProgress(-1);
					this.parent.errors = [];
					if ((<any>resp).status==='ok') {
						this.tmp_equipment.id = (<any>resp).id;
						this.tmp_equipment.data = (<any>resp).equipment;
						if (this.tmp_equipment.data.date_starting ) {
							this.tmp_equipment.data.date_starting = new Date(this.tmp_equipment.data.date_starting);
						}
						if ('last_clean_date' in this.tmp_equipment.data && this.tmp_equipment.data.last_clean_date != null) {
							this.tmp_equipment.data.last_clean_date = new Date(this.tmp_equipment.data.last_clean_date);
						}

							// переместить в другую линию, если изменилось
						if (this.tmp_equipment.mount_point != this.tmp_equipment.prev_mnt) {
                            let first = this.tmp_equipment.prev_mnt.split('#');
                            let last  = this.tmp_equipment.mount_point.split('#');
                            	// убираем оборудование из исходной линии
                            _.remove(this.parent.equipment[first[0]][<any>first[1]-1].devices, (e:any) => {return e.id == this.tmp_equipment.id});
                            	// помещаем оборудование в нужную линию
                            this.parent
								.equipment[last[0]][<any>last[1]-1]
								.devices
								.push(this.tmp_equipment);
						}

						this._AppService.toastrOpen('Оборудование сохранено', 'success');
						this._logger.log('saved equipment =', this.tmp_equipment.id);
					} else {
						this._AppService.toastrOpen((<any>resp).message, 'error', 10000);
						let errors = (<any>resp).errors;
						for (let error in errors) {
							this.parent.errors.push(errors[error][0]);
							//this._logger.warn(errors[error][0]);
						}
						this.errorMessage = '';
					}


				},
				(error:any) => {
					this.errorMessage = error;
                    this._AppService.loadingInProgress(-1);
				}
			);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	deleteEquipment(equipment: Equipment) {
		//if (!confirm('Действительно хотите удалить "'+this._refbookService.equipment[equipment.id_eq_type].name+'"?')) return false;
		this._dialogsService
			.confirm('Удаление','Действительно хотите удалить "'+this._refbookService.equipment[equipment.id_eq_type].name+'"?')
			.subscribe((resp:any) => {
				if (resp) {
					this._logger.log('delete equipment id =', equipment.id);
					this.parent._equipmentService
						.delete(equipment.id)
						.subscribe(
							(resp:any) => {
								this._AppService.toastrOpen('Оборудование удалено');
								this._logger.debug('deleted equipment =', equipment.id);

								let mountPoint = equipment.mount_point.split('#');
								let eq = this.parent.equipment[mountPoint[0]][<any>mountPoint[1]-1].devices;
								for (let i in eq) {
									this._logger.debug(i, eq[i].id);
									if (eq[i].id === equipment.id) {
										eq.splice(<any>i, 1);
										break;
									}
								}
							},
							(error:any) => this.errorMessage
						);
				}
			});

		return false;
	}
	/*------------------------------------------------------------------------------------*/
	// selectEqType(id_eg_type: any) {
	// 	this._logger.log('select', id_eg_type);
	// 	//this._logger.log(this._refbookService.indexedData[id_eg_type].dependence);
	// 	let d = this._refbookService.indexedData[id_eg_type].dependence;
	// 	let dep = d.split(',');
	// 	let arr : any = {};
	// 	let tmp = dep[0].split('#');
	// 	arr[tmp[0]] = tmp[1];
	// 	let tmp2 = dep[1].split('#');
	// 	arr[tmp2[0]] = tmp2[1];
	// 	//this._logger.log(arr);
	// 	this.markRefbook = [];
	// 	this.typeRefbook = [];
	// 	for (let t in this._refbookService.refbooks[arr['type']].data) {
	// 		this.typeRefbook.push(this._refbookService.refbooks[arr['type']].data[t]);
	// 	}
	// 	for (let m in this._refbookService.refbooks[arr['mark']].data) {
	// 		this.markRefbook.push(this._refbookService.refbooks[arr['mark']].data[m]);
	// 	}
	//
	// 	//this._logger.log(this._refbookService.refbooks[arr['mark']].data);
	//
	// }
	/*------------------------------------------------------------------------------------*/
	selectManufacturer(event: any) {
		this._logger.log('select manufacturer =', event, this.tmp_equipment.data.manufacturer);
		this.tmp_equipment.data.id_manufacturer = event.id;
		this.tmp_equipment.data.manufacturer.id = event.id;
		this.tmp_equipment.data.manufacturer.manufacturer = event.text;
	}
	/*------------------------------------------------------------------------------------*/
	dropSuccess(event:any) {
		this._logger.debug('dropSuccess', event);
		// event.mount_point - куда перенесли event.device
		let mount_point = event.mount_point.split('#');
		let devices = this.equipment[mount_point[1]-1].devices;
		this.sorted[event.mount_point] = true;
		for (let i in devices) {
		 	devices[i].order = i;
			devices[i].mount_point = event.mount_point;
			this._logger.debug(devices[i]);
		}
		//this._logger.debug(this.equipment[mount_point[1]-1]);
	}
	/*------------------------------------------------------------------------------------*/
	dragEnd($event:any) {
		//this._logger.debug(event);
	}
	/*------------------------------------------------------------------------------------*/
	saveOrder(mount_point: string) {
		let a = mount_point.split('#');
		let devices = this.equipment[<any>a[1]-1].devices;
		this.sorted[mount_point] = false;
		let data: any[] = [];
		for (let d of devices) {
			data.push({id: d.id, mount_point: d.mount_point, order: d.order});
		}
		this._logger.debug('save order', data);
		this._AppService.loadingInProgress(1);
		this.parent
			._equipmentService
			.saveOrder(data)
			.subscribe(
				(resp:any) => {
					if (resp.status=='ok')
						this._AppService.toastrOpen(resp.message, 'success');
				},
				(error:any) => {
                    this._AppService.loadingInProgress(-1);
					this._AppService.toastrOpen('Ошибка сохранения очерёдности', 'error');
				},
				() => {

					//this.sorted[mount_point] = false;
				}
			)
	}
	/*------------------------------------------------------------------------------------*/
	trackByFn(index:any, item:any) {
		return index;
	}
	/*------------------------------------------------------------------------------------*/
	refreshData() {
		this.parent.loadEquipment(this.parent.urg.id);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	do_nothing() {
		return false;
	}
}
