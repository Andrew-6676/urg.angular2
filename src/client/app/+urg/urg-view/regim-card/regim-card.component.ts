import { Component, OnInit, Input }       from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { DomSanitizer }   from '@angular/platform-browser';
import { MatIconRegistry, MatTabChangeEvent } from '@angular/material';

import { AppService }       from '../../../shared/services/app.service';
import { LoggerService }    from '../../../shared/services/logger.service';
import { UrgService }       from '../../../shared/services/urg.service';
import { UnitService }      from '../../../shared/services/unit.service';
import { RefbookService }   from '../../../shared/services/refbook.service';
import { EquipmentService } from '../../../shared/services/equipment.service';
import { ReportsService }   from '../../../shared/services/reports.service';
import { DialogsService }   from '../../../shared/components/dialogs/dialogs.service';

import { UrgComponent }   from '../../urg.component';
import { UrgTable }       from '../../../shared/models/urg';
import { Equipment }      from '../../../shared/models/equipmen';

import { ObjectIterator } from '../../../shared/models/object.iterator';

import * as _ from 'lodash';

@Component({
	moduleId: module.id,
	selector: 'regim-card',
	templateUrl: 'regim-card.component.html',
	styleUrls: ['regim-card.component.css'],
	providers: [ReportsService]
})

export class RegimCardComponent  {
	@Input() urg: any;
	@Input() equipment: any;
	@Input() parent: any;

	types = {
		enter: 'Вход',
		line: 'Линия редуцирования',
		exit: 'Выход',
		bypass: 'Байпас',
		heating: 'Линия отопления'
	};

	constructor(public _AppService: AppService,
	            private _logger: LoggerService,
	            public  _reports: ReportsService,
	            public _refbookService: RefbookService,
	            private _urgService: UrgService,
	            public _equipmentService: EquipmentService,
	            public _dialogService: DialogsService,
	) {}
	/*----------------------------------------------------------------*/
	saveRegimCard() {
		let data: any = [];
		for (let mnt in this.equipment) {
			for (let l of this.equipment[mnt]) {
				//this._logger.debug(l);
				for (let e of l.devices) {
					//this._logger.debug(e);
					data.push({id_obj: e.id, id_eq_type: e.id_eq_type, pressure_data: JSON.stringify(e.data.pressure_data)});
				}
			}

		}
		this._logger.debug('save card', data);

		this._equipmentService
			.saveRegimCard(data)
			.subscribe(
				(resp:any) => {
					this._logger.debug(resp);
					if (resp.status=='ok')
						this._AppService.toastrOpen(resp.message, 'success');
					else
						this._AppService.toastrOpen(resp.message, 'warn');
				},
				(error:any) => {
					this._AppService.toastrOpen('Ошибка сохраенния режимной карты', 'error', 0)
				}
			);

		return false;
	}
	/*----------------------------------------------------------------*/
	trackByFn(index:any, item:any) {
		return index;
	}
	/*----------------------------------------------------------------*/

	/*----------------------------------------------------------------*/
}