import { Component, OnInit, Input }       from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { DomSanitizer }   from '@angular/platform-browser';
import { MatIconRegistry, MatTabChangeEvent } from '@angular/material';

import { AppService }       from '../../shared/services/app.service';
import { LoggerService }    from '../../shared/services/logger.service';
import { UrgService }       from '../../shared/services/urg.service';
import { UnitService }      from '../../shared/services/unit.service';
import { RefbookService }   from '../../shared/services/refbook.service';
import { EquipmentService } from '../../shared/services/equipment.service';
import { ReportsService }   from '../../shared/services/reports.service';
import { DialogsService }   from '../../shared/components/dialogs/dialogs.service';

import { UrgComponent }   from '../urg.component';
import { UrgTable }       from '../../shared/models/urg';
import { Equipment }      from '../../shared/models/equipmen';

import { ObjectIterator } from '../../shared/models/object.iterator';

import * as _ from 'lodash';
import {Observable} from 'rxjs/Observable';
/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
	moduleId: module.id,
	selector: 'urg-view',
	templateUrl: 'urg-view.component.html',
	styleUrls: ['urg-view.component.css'],
	providers: [UnitService, UrgService, EquipmentService]
})

export class UrgViewComponent implements OnInit {
	@Input() parent: UrgComponent;
	loaded: boolean = false;
	errorMessage: string;
	urg: UrgTable;

	equipment_load_in_progress: boolean =  false;
	equipment_loaded: boolean =  false;
	equipment_loaded2: boolean =  false;
	//equipment: any = {};
	equipment: {[key: string]:any} = new ObjectIterator();
	equipmentForRegimGard: {[key: string]:any} = new ObjectIterator();

	current_file:string = null;

	f_categories = {
		0:"Другое",
		1:"Схема",
		2:"Фотография",
		3:"Документ"
	};
	types = {
		enter: 'Вход',
		line: 'Линия редуцирования',
		exit: 'Выход',
		bypass: 'Байпас',
		heating: 'Линия отопления'
	};
	constructor(public _AppService: AppService,
	            private _logger: LoggerService,
	            private _aroute: ActivatedRoute,
	            private _router: Router,
	            public  _reports: ReportsService,
	            public _refbookService: RefbookService,
	            private _urgService: UrgService,
	            public _equipmentService: EquipmentService,
	            public _dialogService: DialogsService,
	            iconRegistry: MatIconRegistry,
	            sanitizer: DomSanitizer
	) {
		this._AppService.app.pageTitle = 'Просмотр';
		this._aroute.params.forEach((params: Params) => {
			let id = params['id'];
			this._logger.debug('view urg id =', id);
			this.loadUrg(id);
		});

		iconRegistry.addSvgIcon('attachment',sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/attachment.svg'));
		iconRegistry.addSvgIcon('txt', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/txt.svg'));
		iconRegistry.addSvgIcon('odt', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/odt.svg'));
		iconRegistry.addSvgIcon('doc', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/doc.svg'));
		iconRegistry.addSvgIcon('docx',sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/docx.svg'));
		iconRegistry.addSvgIcon('pdf', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/pdf.svg'));
		iconRegistry.addSvgIcon('xls', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/xls.svg'));
		iconRegistry.addSvgIcon('xlsx',sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/xlsx.svg'));
		iconRegistry.addSvgIcon('ods', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/ods.svg'));
		iconRegistry.addSvgIcon('jpg', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/jpg.svg'));
		iconRegistry.addSvgIcon('jpeg',sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/jpeg.svg'));
		iconRegistry.addSvgIcon('tiff',sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/tiff.svg'));
		iconRegistry.addSvgIcon('gif', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/gif.svg'));
		iconRegistry.addSvgIcon('png', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/png.svg'));
		iconRegistry.addSvgIcon('dwg', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/dwg.svg'));
		iconRegistry.addSvgIcon('zip', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/zip.svg'));
		iconRegistry.addSvgIcon('sql', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/sql.svg'));
	}
	/*------------------------------------------------------------------------------------*/
	ngOnInit() {
		//this.getUnits();
		//this.getUrgs();
		//this.getItems(this.unit_id);
	}
	/*------------------------------------------------------------------------------------*/
	refbook(id: number) {
		return this._refbookService.indexedData[id] ? this._refbookService.indexedData[id].name : '<Ошибка!>';
	}
	/*------------------------------------------------------------------------------------*/
	loadUrg(id: string) {
		this._AppService.loadingInProgress(1);
		this.loaded = false;
		this._urgService
			.getById(id)
			.subscribe(
				(resp:any) => {
                    this._AppService.loadingInProgress(-1);
					this.urg = <UrgTable>resp;
				},
				(error:any) => {
                    this._AppService.loadingInProgress(-1);this.errorMessage = <any>error;},
				() => {
					let da = (<any>this.urg.date_acceptance).split('-');
					let ds = (<any>this.urg.date_starting).split('-');
					this.urg.date_acceptance = new Date(Date.UTC(<any>da[0], <any>da[1]-1, <any>da[2]));
					this.urg.date_starting = new Date(Date.UTC(<any>ds[0], <any>ds[1]-1, <any>ds[2]));
					this.loaded = true;
					this._logger.debug('urg '+id+' loaded');
					//this._AppService.loadingInProgress(-1);
					this._AppService.toastrOpen('Данные УРГ загружены.');
				}
			);
	}
	/*------------------------------------------------------------------------------------*/
	viewTabChange(tab: MatTabChangeEvent) {
		this._logger.log('change tab to ', tab.index);
		if (tab.index==1) {
			if (!this.equipment_loaded2) {
				this._logger.log('load equipmentForRegimCard');
				this.loadEquipment(this.urg.id, true);
			}
		}
		if (tab.index==2) {
			if (!this.equipment_loaded) {
				this._logger.log('load equipment');
				this.loadEquipment(this.urg.id);
			}
		}
	}
	/*------------------------------------------------------------------------------------*/
	goToPanorama() {
		this._logger.log('go to Panorama', this.urg.id);
		//windov.location
	}
	/*------------------------------------------------------------------------------------*/
	editUrg() {
		this._logger.debug('view urg', this.urg.id);
		this._router.navigateByUrl('/urg/edit/'+this.urg.id);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
		//TODO: эта функция дублирует функцию из form
	getAddress(addr: any) {
		let res: string = '';
		if (addr) {
			if (addr.np.id>0) res += addr.np.name;
			if (addr.street.id>0) res += ', '+addr.street.name;
			if (addr.house) res += ', д.'+addr.house;
			if (addr.building) res += ', к.'+addr.building;
			//if (addr.flat) res += ', кв.'+addr.flat;
			//addr.np.name+', '+addr.street.name+', д.'+addr.house+'-'+addr.building+', '+addr.flat
		} else {
			res = '- не указан -';
		}

		return res;
	}
	/*------------------------------------------------------------------------------------*/
		//TODO: эта функция дублирует функцию из form
	deleteFile(file: any) {
		// this.confirm_message = 'Действительно хотите удалить этот файл?';
		// this.modal_confirm.open();

		//if (!confirm('Действительно хотите удалить этот файл?')) return false;

		this._dialogService
			.confirm('Удаление','Действительно хотите удалить файл "'+file.title+'"? ')
			.subscribe((resp:any) => {
				if (resp) {
					this._logger.log('delete file id =', file.id);
					this._urgService
						.deleteFile(file.id)
						.subscribe(
							(resp:any) => this._logger.warn(resp),
							(error:any) => this.errorMessage = <any>error,
							() => {
								//this.reloadFiles();
								_.remove(this.urg.files, (e:any) => {return e.id == file.id});
								this._logger.debug('delete completed id =', file.id);
							}
						);
				}
			});

		return false;
	}
	/*------------------------------------------------------------------------------------*/
		//TODO: эта функция дублирует функцию из form
	reloadFiles() {
		this._AppService.loadingInProgress(1);
		this._logger.log('reload files', this.urg.id);
		this._urgService
			.getFiles(this.urg.id)
			.subscribe(
				(resp:any) => {
                    this._AppService.loadingInProgress(-1);this.urg.files = resp;},
				(error:any) => {
                    this._AppService.loadingInProgress(-1);this.errorMessage = <any>error;},
				() => {
					//this._AppService.loadingInProgress(-1);
					this._logger.debug('files reloaded');
				}
			);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
		//TODO: эта функция дублирует функцию из form
	loadEquipment(id_urg:string, forRegimCard?:boolean) {
		this._AppService.loadingInProgress(1);
		let req : Observable<any> ;
		if (forRegimCard)
			req = this._equipmentService.getListByUrgForRegimCard(id_urg);
		else
			req = this._equipmentService.getListByUrg(id_urg);

		req.subscribe(
			(resp:any) => {
                this._AppService.loadingInProgress(-1);
					//this.equipment = Object.create(null);   // создаём объект без всяких прототипов
					let equipment:{[key: string]:any} = new ObjectIterator();
					equipment.line = [];
					equipment.enter = [];
					equipment.exit = [];
					equipment.bypass = [];
					equipment.heating = [];

					equipment.tmp_line  = Object.create(null);
					equipment.tmp_enter = Object.create(null);
					equipment.tmp_exit  = Object.create(null);
					equipment.tmp_bypass  = Object.create(null);
					equipment.tmp_heating  = Object.create(null);

					//this._logger.warn(resp);
					// проходимся по полученным данным и формируем удобную структуру для вывода
					for (let id_obj in <any>resp) {
						let dev = resp[id_obj];
						//this._logger.log(dev);
						//let di = (<any>dev.date_install).split('-');
						//dev.date_install = new Date(Date.UTC(<any>di[0], <any>di[1]-1, <any>di[2]));
						let point = dev.mount_point.split('_')[0].split('#');

						let tmp_dev = dev;
						if (tmp_dev.data) {
							tmp_dev.data.date_starting = new Date(tmp_dev.data.date_starting);
							if ('last_clean_date' in tmp_dev.data && tmp_dev.data.last_clean_date != null) {
								tmp_dev.data.last_clean_date = new Date(tmp_dev.data.last_clean_date);
							}
							if ('pressure_data' in tmp_dev.data && tmp_dev.data.pressure_data == null) {
								tmp_dev.data.pressure_data = {w:{'in':'', 'out':''}, s:{'in':'', 'out':''}};
							}
						} else {
							tmp_dev.id = -1;
							tmp_dev.schema_number = 'ОШИБКА!';
						}

						if (equipment['tmp_'+point[0]][point[1]] === undefined) {
							equipment['tmp_'+point[0]][point[1]] = Object.create(null);
							equipment['tmp_'+point[0]][point[1]].devices = [];
						}
						equipment['tmp_'+point[0]][point[1]].type = point[0];
						equipment['tmp_'+point[0]][point[1]].devices.push(tmp_dev);

					}


					// преобразовать в массив, что бы можно было вывести в цикле *ngFor
					for (let rl  in equipment.tmp_line) {
						equipment.line.push(equipment.tmp_line[rl]);
					}
					if (equipment.line.length===0) {
						let to = Object.create(null);
						to.type = 'line';
						to.devices = [];
						equipment.line.push(to);
					}
					for (let en  in equipment.tmp_enter) {
						equipment.enter.push(equipment.tmp_enter[en]);
					}
					if (equipment.enter.length===0) {
						let to = Object.create(null);
						to.type = 'enter';
						to.devices = [];
						equipment.enter.push(to);
					}
					for (let ex  in equipment.tmp_exit) {
						equipment.exit.push(equipment.tmp_exit[ex]);
					}
					if (equipment.exit.length===0) {
						let to = Object.create(null);
						to.type = 'exit';
						to.devices = [];
						equipment.exit.push(to);
					}
					for (let ex  in equipment.tmp_bypass) {
						equipment.bypass.push(equipment.tmp_bypass[ex]);
					}
					if (equipment.bypass.length===0) {
						let to = Object.create(null);
						to.type = 'bypass';
						to.devices = [];
						equipment.bypass.push(to);
					}
					for (let ex  in equipment.tmp_heating) {
						equipment.heating.push(equipment.tmp_heating[ex]);
					}
					if (equipment.heating.length===0) {
						let to = Object.create(null);
						to.type = 'heating';
						to.devices = [];
						equipment.heating.push(to);
					}

					delete equipment.tmp_line;
					delete equipment.tmp_enter;
					delete equipment.tmp_exit;
					delete equipment.tmp_bypass;
					delete equipment.tmp_heating;

					this._logger.warn('equipment = ', equipment);
					//this._logger.warn('equipment2 = ', this.equipment2);
					if (forRegimCard) {
						this.equipmentForRegimGard = equipment;
						this.equipment_loaded2 = true;
						this.equipment_load_in_progress = false;
					} else {
						this.equipment = equipment;
						this.equipment_loaded = true;
						this.equipment_load_in_progress = false;
					}

				},
				(error:any) => {
                    this._AppService.loadingInProgress(-1);
					this.errorMessage = <any>error;
					this.equipment_load_in_progress = false;
					this._AppService.toastrOpen('Ошибка загрузки оборудования.', 'error', 0);
				},
				() => {
					//this._AppService.loadingInProgress(-1);
					this._AppService.toastrOpen('Список оборудования загружен.');
					this._logger.debug('equipment '+id_urg+' loaded');
				}
			);
		return true;
	}
	/*------------------------------------------------------------------------------------*/
	refreshData() {
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	do_nothing() {
		return false;
	}
}
