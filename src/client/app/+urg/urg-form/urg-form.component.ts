import { Component, Input, ViewChild }     from '@angular/core';
import { ActivatedRoute, Params, Router }  from '@angular/router';
import { DomSanitizer }                    from '@angular/platform-browser';
import { MatIconRegistry }                 from '@angular/material';

import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
import { ModalDirective }  from 'ngx-bootstrap';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { AppService }             from '../../shared/services/app.service';
import { LoggerService }          from '../../shared/services/logger.service';
import { UrgService }             from '../../shared/services/urg.service';
import { UnitService }            from '../../shared/services/unit.service';
import { RingService }            from '../../shared/services/ring.service';
import { EquipmentService }       from '../../shared/services/equipment.service';
import { InstrumentationService } from '../../shared/services/instrumentation.service';
import { RefbookService }         from '../../shared/services/refbook.service';
import { AddressService }         from '../../shared/services/address.service';
import { DialogsService }         from '../../shared/components/dialogs/dialogs.service';

import { UrgInstrumentationComponent } from '../urg-instrumentation/urg-instrumentation.component';
import { UrgComponent }                from '../urg.component';

import { UrgTable }       from '../../shared/models/urg';
import { File }           from '../../shared/models/file';
import { Room }           from '../../shared/models/room';

import * as _ from "lodash";

import { ObjectIterator } from '../../shared/models/object.iterator';
import { List }           from '../../shared/models/list';
import { Address }        from '../../shared/models/address';

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
	moduleId: module.id,
	selector: 'urg-form',
	templateUrl: 'urg-form.component.html',
	styleUrls: ['urg-form.component.css'],
	providers: [UrgService, AddressService, RingService, EquipmentService, InstrumentationService],
})

export class UrgFormComponent /*implements OnInit */ {
	@Input() parent: UrgComponent;
	@ViewChild('modal_upload') modal_upload: ModalDirective;
	@ViewChild('modal_room') modal_room: ModalDirective;
	@ViewChild('modal_upload_equipment') modal_upload_equipment: ModalDirective;
	@ViewChild('urg_instrumentation') urg_instrumentation: UrgInstrumentationComponent;


	optionsModel: any[] = [];
	myOptions: IMultiSelectOption[] = [];
	mySettings: IMultiSelectSettings = {
		pullRight: false,
		enableSearch: true,
		checkedStyle: 'glyphicon',
		buttonClasses: 'btn btn-default btn-block',
		itemClasses: '', // CSS classes to apply to items
		containerClasses:'dropdown-inline', // CSS classes to apply to container div
		selectionLimit: 0,
		closeOnSelect: false,
		showCheckAll: true,
		showUncheckAll: true,
		dynamicTitleMaxItems: 10,
		maxHeight: '500px',
		displayAllSelectedText: true
	};
	myTexts: IMultiSelectTexts = {
		checkAll: 'Отметить все',
		uncheckAll: 'Снять все отметки',
		checked: 'выбрано',
		checkedPlural: 'отмечено',
		searchPlaceholder: 'Поиск...',
		defaultTitle: 'Выбрать УРГ для объединения в кольцо',
		allSelected: 'Выбраны все',
	};

	value: any;
	value2: any;

	confirm_message: string = '';

	add_file = false;
	active = true;

	title: string = '';
	mode: string = undefined;

	urg = new UrgTable();
	//urg_r: any[] = [];

	equipment: any = {};
	fileToEquipment: any = null;
	instrumentation: any = [];
	fileToInstrumentation: any = null;

	types = {
		enter: 'Вход',
		line: 'Линия редуцирования',
		exit: 'Выход',
		bypass: 'Байпас',
		heating: 'Линия отопления'
	};

	searchRing: any = {
		current_urg: null,
		opgh: null,
		unit: null,
		type: null,
		num: null,
		ring_search_res: [],
		res_ring_selected: 0
	};

	tmp: any = {};
	tmp_room: Room = new Room();
	errorMessage: string;
	errors: string[] = [];

	regions: any[] = [];
	unit:    any = [];
	units:   any[] = [];

	loaded = false;

	masterInEdit = false;
	tmpMaster: any = null;

	equipment_load_in_progress: boolean =  false;
	instrumentation_load_in_progress: boolean =  false;

	public typeaheadLoading: { [key: string]: boolean; } = { };
	public typeaheadNoResults: { [key: string]: boolean; } = { };
	public typeaheadSelected: { [key: string]: boolean; } = { };

	public region_search:string = '';

	public regionSource: Observable<any>;
	public npSource: Observable<any>;
	public streetSource: Observable<any>;

	datepickerOpts: any = {
		placeholder: 'Выберите дату',
		//assumeNearbyYear: true,
		format: 'dd.mm.yyyy',
		weekStart: 1,
		todayHighlight: true,
		todayBtn: 'linked',
		language: 'ru',
		daysOfWeekHighlighted: [0,6],
		autoclose: true,
		icon: 'glyphicon glyphicon-calendar'
	};

	constructor(public _refbookService: RefbookService,
				public _AppService: AppService,
				public _equipmentService: EquipmentService,
				public _instrumentationService: InstrumentationService,
	            private _logger: LoggerService,
	            private _urgService: UrgService,
	            private _aroute: ActivatedRoute,
	            private _router: Router,
	            private _addressService: AddressService,
	            private _unitService: UnitService,
	            private _ringService: RingService,
	            private _dialogsService: DialogsService,
	            iconRegistry: MatIconRegistry,
	            sanitizer: DomSanitizer
	) {
		this._AppService.app.pageTitle = 'Редактирование';
		this._aroute.params.forEach((params: Params) => {
			let id = params['id'];
			if (id !== undefined) {
				this.title = 'Редактирование объекта';
				this.mode = 'edit';
				this._logger.log('edit urg id =',id);
				this.loadUrg(id);
				this.loadEquipment(id);
				this.loadInstrumentations(id);
				this.searchRing.current_urg = id;
				this.getRingList();
			} else {
				this.title = 'Добавление объекта';
				this.mode = 'add';
				this.urg.id_unit = <any>this._AppService.app.currentUnit.id;
				this.urg.num_unique = this._urgService.generateUniqueNum(<any>this._AppService.app.currentUnit.id);

				this.equipment = Object.create(null);   // создаём объект без всяких прототипов
				this.equipment.line    = [];
				this.equipment.enter   = [];
				this.equipment.exit    = [];
				this.equipment.bypass  = [];
				this.equipment.heating = [];

				this.equipment.tmp_line    = Object.create(null);
				this.equipment.tmp_enter   = Object.create(null);
				this.equipment.tmp_exit    = Object.create(null);
				this.equipment.tmp_bypass  = Object.create(null);
				this.equipment.tmp_heating = Object.create(null);

				this._logger.log('add urg id = -1');
			}
		});
		// загрузка юнитов в верхний выпадающий список
		this.getUnit(<any>this._AppService.app.currentUnit.id);
		this.searchRing.opgh = this._AppService.app.currentOPGH.id;
		//this._logger.warn(this._unitService.indexOPGH2[this.searchRing.opgh].childs);
		this.searchInit();

		iconRegistry.addSvgIcon('attachment',sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/attachment.svg'));
		iconRegistry.addSvgIcon('txt', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/txt.svg'));
		iconRegistry.addSvgIcon('odt', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/odt.svg'));
		iconRegistry.addSvgIcon('doc', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/doc.svg'));
		iconRegistry.addSvgIcon('docx',sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/docx.svg'));
		iconRegistry.addSvgIcon('pdf', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/pdf.svg'));
		iconRegistry.addSvgIcon('xls', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/xls.svg'));
		iconRegistry.addSvgIcon('xlsx',sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/xlsx.svg'));
		iconRegistry.addSvgIcon('ods', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/ods.svg'));
		iconRegistry.addSvgIcon('jpg', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/jpg.svg'));
		iconRegistry.addSvgIcon('jpeg',sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/jpeg.svg'));
		iconRegistry.addSvgIcon('tiff',sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/tiff.svg'));
		iconRegistry.addSvgIcon('gif', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/gif.svg'));
		iconRegistry.addSvgIcon('png', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/png.svg'));
		iconRegistry.addSvgIcon('dwg', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/dwg.svg'));
		iconRegistry.addSvgIcon('zip', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/zip.svg'));
		iconRegistry.addSvgIcon('sql', sanitizer.bypassSecurityTrustResourceUrl('assets/img/svg/sql.svg'));
	}
	/*------------------------------------------------------------------------------------*/
	getUnit(id: number) {
		this._unitService
			.getCompany(<any>this._AppService.user.unit.id)
			.subscribe(
				(resp:any) => {
					this.unit = resp;
					this._logger.log(resp);
					this._logger.debug('units loaded');
				},
				(error:any) =>  this.errorMessage = <any>error
			);
	}
	/*------------------------------------------------------------------------------------*/
	getAllRegions(id_region:string) {
		this._addressService.
		getAllRegions()
			.subscribe(
				(resp:any) => this.regions = resp,
				(error:any) => this.errorMessage = <any>error,
				() => {
					this._logger.debug('all regions loaded');
				}
			);
	}
	/*------------------------------------------------------------------------------------*/
	getRegionsByName(term: string) {
		this._addressService.
		getRegionsByName(term)
			.subscribe(
				(resp:any) => this.regions = resp,
				(error:any) => this.errorMessage = <any>error,
				() => {
					this._logger.debug('regions by name = '+term+' loaded');
				}
			);
	}
	/*------------------------------------------------------------------------------------*/
	deleteAddr(id?: any) {
		this._dialogsService
			.confirm('Удаление адреса', 'Уверены, что хотите удалить адрес?')
			.subscribe(
				(resp:any) => {
					if (resp) {
						this._logger.debug('delete address', id);
						this._urgService
							.clearAddress(this.urg.id, this.urg.address.id)
							.subscribe(
								(resp:any) => {
									this._AppService.toastrOpen(resp.message, 'success');
									this.urg.address = new Address();
								},
								(error:any) => {
									this._AppService.toastrOpen(<any>error, 'error', 0);
								}
							);
					}
				}
			);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	searchUrgForRing() {
		return false;
	}
	/*------------------------------------------------------------------------------------*/
		// получить список всех УРГ и УРГ в кольцах для выбора в закольцовку
	getRingList() {
		this._logger.log('search urg for ring', this.searchRing);
		//this.tmp.ring_search_res = [{id:12, id_type_urg: 1005, num: 234}];
		this._ringService
			.getUrgList(this.searchRing)
			.subscribe(
				res => {
					//this._logger.debug(res);
					this.searchRing.ring_search_res = res;
					this.optionsModel = [];
					this.myOptions    = [];
					this.myOptions.push({
						id: 0,
						name: 'Кольца',
						isLabel: true
					});
					//let label = 0;
					let labels = [0];
					for (let ri in res) {
							// создаём категорию в списке (подразделение) - если такой ещё не создали и если текущий пункт одиночный УРГ
						if (labels.indexOf(res[ri].id_unit) < 0 && res[ri].id_unit>0) {
							this.myOptions.push({
								id: res[ri].id_unit,
								name: this._unitService.indexOPGH_flat[res[ri].id_unit] ? this._unitService.indexOPGH_flat[res[ri].id_unit].name : res[ri].id_unit,
								isLabel: true
							});
							//label = res[ri].id_unit;
							labels.push(res[ri].id_unit);

							this.myOptions.push({
								id: res[ri].id_unit*1000,
								name: '-', // '   ('+ this._unitService.indexOPGH_flat[res[ri].id_unit].name +')'
								parentId: res[ri].id_unit
							});
						}
							// вставляем УРГ в соответствующую категорию (в кольца или соответствующее подразделение)
						this.myOptions.push({
							id: res[ri].id_urgs,
							name: res[ri].urgs, // '   ('+ this._unitService.indexOPGH_flat[res[ri].id_unit].name +')'
							parentId: (res[ri].urgs.search(/,/) > 0 ? 0 : res[ri].id_unit)
						});
					}
				},
				(error:any) => this.errorMessage = <any>error,
				() => {
					this._logger.debug('search complete');
				}
			);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	makeRing(ids: any[], mode: string)
	{
		// разбиваем кольца на отдельные УРГ в id_other_urg
		let ids2: any[] = [];
		for (let i of ids) {
			if (typeof i == 'string') {
				//this._logger.debug(i.split(','));
				let is: string[] = i.split(',');
				for (let id of is) {
					this._logger.debug(id);
					ids2.push(id);
				}
			}
		}

		if (mode=='ring') {
			// добавить для объединения УРГ из текущего кольца
			for (let i of this.urg.ring) {
				ids2.push(i.id);
			}
		}

		this._ringService
			.makeRing(this.urg.id, ids2)
			.subscribe(
				(resp:any) => {
					//this._logger.warn(resp);
				},
				(error:any) => this.errorMessage = <any>error,
				() => {
					this.refreshRing();
					this._logger.debug('ring was maked');
				}
			);

		return false;
	}
	/*------------------------------------------------------------------------------------*/
	includeToRing(id: string) {
		this._ringService
			.includeToRing(this.urg.id, this.searchRing.res_ring_selected)
			.subscribe(
				(resp:any) => {
					//this._logger.warn(resp);
				},
				(error:any) => this.errorMessage = <any>error,
				() => {
					this.refreshRing();
					this._logger.debug('included to ring');
				}
			);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	excludeFromRing(urg: any) {
		//if (!confirm('Действительно хотите исключить '+this._refbookService.indexedData[urg.id_type_urg].name+'-'+urg.num+'?')) return false;
		this._dialogsService
			.confirm('Удалить','Действительно хотите исключить '+this._refbookService.indexedData[urg.id_type_urg].name+'-'+urg.num+' из кольца?')
			.subscribe((resp:any) => {
				if (resp) {
					this._logger.log('exclude from ring id =', urg.id);
					this._ringService
						.excludeFromRing(urg.id)
						.subscribe(
							(resp:any) => {
								//this._logger.warn(resp);
								this._logger.debug(this._refbookService.indexedData[urg.id_type_urg].name+'-'+urg.num+' excluded from ring');
								this.refreshRing();
							},
							(error:any) => this.errorMessage = <any>error
						);
				}
			});

		return false;
	}
	/*------------------------------------------------------------------------------------*/
	loadInstrumentations(id_urg:string) {
		this._AppService.loadingInProgress(1);
		this.instrumentation_load_in_progress = true;
		this._instrumentationService
			.getListByUrg(id_urg)
			.subscribe(
				(resp:any) => {
                    this._AppService.loadingInProgress(-1);
					this.instrumentation = resp;
				},
				(error:any) => {
                    this._AppService.loadingInProgress(-1);
					this.errorMessage = <any>error;
					this.instrumentation_load_in_progress = false;
					this._AppService.toastrOpen('Ошибка загрузки списка КИП.', 'error');
				},
				() => {
					//this._AppService.loadingInProgress(-1);
					//this._AppService.snackbarOpen('Список КИП загружен.', 5000);
					this._AppService.toastrOpen('Список КИП загружен.');
					this.equipment_load_in_progress = false;
					//this._logger.debug('instrumentation '+id_urg+' loaded');
				}
			);
	}
	/*------------------------------------------------------------------------------------*/
	loadEquipment(id_urg:string) {
		this._AppService.loadingInProgress(1);
		this._equipmentService
			.getListByUrg(id_urg)
			.subscribe(
				(resp:any) => {
                    this._AppService.loadingInProgress(-1);
					this.equipment = new ObjectIterator();
					this.equipment.line = [];
					this.equipment.enter = [];
					this.equipment.exit = [];
					this.equipment.bypass = [];
					this.equipment.heating = [];

					this.equipment.tmp_line  = Object.create(null);
					this.equipment.tmp_enter = Object.create(null);
					this.equipment.tmp_exit  = Object.create(null);
					this.equipment.tmp_bypass  = Object.create(null);
					this.equipment.tmp_heating  = Object.create(null);

					//this._logger.warn(resp);
						// проходимся по полученным данным и формируем удобную структуру для вывода
					for (let id_obj in <any>resp) {
						let dev = resp[id_obj];
						//this._logger.log(dev);
						//let di = (<any>dev.date_install).split('-');
						//dev.date_install = new Date(Date.UTC(<any>di[0], <any>di[1]-1, <any>di[2]));
						let point = dev.mount_point.split('_')[0].split('#');

						let tmp_dev = dev;
						if (tmp_dev.data) {
							if (tmp_dev.data.date_starting && tmp_dev.data.date_starting!='0000-00-00') {
								tmp_dev.data.date_starting = new Date(tmp_dev.data.date_starting);
							} else {
								tmp_dev.data.date_starting = null;
							}
							if ('last_clean_date' in tmp_dev.data && tmp_dev.data.last_clean_date != null) {
								tmp_dev.data.last_clean_date = new Date(tmp_dev.data.last_clean_date);
							}
						} else {
							tmp_dev.id = -1;
							tmp_dev.schema_number = 'ОШИБКА!';
						}

						if (this.equipment['tmp_'+point[0]][point[1]] === undefined) {
							this.equipment['tmp_'+point[0]][point[1]] = Object.create(null);
							this.equipment['tmp_'+point[0]][point[1]].devices = [];
						}
						this.equipment['tmp_'+point[0]][point[1]].type = point[0];
						this.equipment['tmp_'+point[0]][point[1]].devices.push(tmp_dev);

					}


						// преобразовать в массив, что бы можно было вывести в цикле *ngFor
					for (let rl  in this.equipment.tmp_line) {
						this.equipment.line.push(this.equipment.tmp_line[rl]);
					}
					// if (this.equipment.line.length===0) {
					// 	let to = Object.create(null);
					// 	to.type = 'line';
					// 	to.devices = [];
					// 	this.equipment.line.push(to);
					// }
					for (let en  in this.equipment.tmp_enter) {
						this.equipment.enter.push(this.equipment.tmp_enter[en]);
					}
					// if (this.equipment.enter.length===0) {
					// 	let to = Object.create(null);
					// 	to.type = 'enter';
					// 	to.devices = [];
					// 	this.equipment.enter.push(to);
					// }
					for (let ex  in this.equipment.tmp_exit) {
						this.equipment.exit.push(this.equipment.tmp_exit[ex]);
					}
					// if (this.equipment.exit.length===0) {
					// 	let to = Object.create(null);
					// 	to.type = 'exit';
					// 	to.devices = [];
					// 	this.equipment.exit.push(to);
					// }
					for (let ex  in this.equipment.tmp_bypass) {
						this.equipment.bypass.push(this.equipment.tmp_bypass[ex]);
					}
					// if (this.equipment.bypass.length===0) {
					// 	let to = Object.create(null);
					// 	to.type = 'bypass';
					// 	to.devices = [];
					// 	this.equipment.bypass.push(to);
					// }
					for (let ex  in this.equipment.tmp_heating) {
						this.equipment.heating.push(this.equipment.tmp_heating[ex]);
					}
					// if (this.equipment.heating.length===0) {
					// 	let to = Object.create(null);
					// 	to.type = 'heating';
					// 	to.devices = [];
					// 	this.equipment.heating.push(to);
					// }

					delete this.equipment.tmp_line;
					delete this.equipment.tmp_enter;
					delete this.equipment.tmp_exit;
					delete this.equipment.tmp_bypass;
					delete this.equipment.tmp_heating;

					this._logger.warn('equipment = ', this.equipment);
					//this._logger.warn('equipment2 = ', this.equipment2);

				},
				(error:any) => {
                    this._AppService.loadingInProgress(-1);
					this.errorMessage = <any>error;
					this.equipment_load_in_progress = false;
					this._AppService.toastrOpen('Ошибка загрузки оборудования.', 'error', 0);
				},
				() => {
					//this._AppService.loadingInProgress(-1);
					this._AppService.toastrOpen('Список оборудования загружен.');
					this.equipment_load_in_progress = false;
					this._logger.debug('equipment '+id_urg+' loaded');
				}
			);
		return true;
	}
	/*------------------------------------------------------------------------------------*/
	addMountPoint(point:string) {
		let to = Object.create(null);
		to.type = point;
		to.devices = [];
		this.equipment[point].push(to);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	deleteMountPoint(type: string, i:number) {
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	loadUrg(id: string) {
		this._AppService.loadingInProgress(1);
		this._urgService
			.getById(id)
			.subscribe(
				(resp:any) => {
                    this._AppService.loadingInProgress(-1);
					this.urg = <UrgTable>resp;
					this._AppService.toastrOpen('Данные загружены');
					this.loaded = true;
					if (this.urg.date_acceptance != null) {
						let da = (<any>this.urg.date_acceptance).split('-');
						this.urg.date_acceptance = new Date(Date.UTC(<any>da[0], <any>da[1]-1, <any>da[2]));
					}
					if (this.urg.date_starting != null) {
						let ds = (<any>this.urg.date_starting).split('-');
						this.urg.date_starting = new Date(Date.UTC(<any>ds[0], <any>ds[1]-1, <any>ds[2]));
					}
					if (this.urg.date_startup_gas != null) {
						let dsg = (<any>this.urg.date_startup_gas).split('-');
						this.urg.date_startup_gas = new Date(Date.UTC(<any>dsg[0], <any>dsg[1]-1, <any>dsg[2]));
					}
					if (this.urg.master.date != null) {
						let dsg = (<any>this.urg.master.date).split('-');
						this.urg.master.date = new Date(Date.UTC(<any>dsg[0], <any>dsg[1]-1, <any>dsg[2]));
					}
					if (this.urg.lastwork) {
						this.urg.lastwork = new Date(this.urg.lastwork);
					}

					this._logger.debug('urg '+id+' loaded');
				},
				(error:any) => {
                    this._AppService.loadingInProgress(-1);
                    this.errorMessage = <any>error;
				},
				() => {
					//this._AppService.loadingInProgress(-1);
				}
			);
	}
	/*------------------------------------------------------------------------------------*/
	deleteFile(file: any, callback?:any) {
		// this.confirm_message = 'Действительно хотите удалить этот файл?';

		//if (!confirm('Действительно хотите удалить этот файл?')) return false;
		this._dialogsService
			.confirm('Удаление','Действительно хотите удалить файл "'+file.title+'"?')
			.subscribe(
				(resp:any) =>{
				if (resp) {
					this._logger.log('delete file id =', file.id);
					this._urgService
						.deleteFile(file.id)
						.subscribe(
							(resp:any) => {
								this._logger.warn(resp);
								if (resp.status=='ok') {
									this._AppService.toastrOpen(resp.message,'success');
								} else {
									this._AppService.toastrOpen(resp.message,'error');
								}
							},
							(error:any) => this.errorMessage = <any>error,
							() => {
								if (callback)
									callback();
								else {
									_.remove(this.urg.files, (e:any) => {return e.id == file.id});
								}

								this._logger.debug('delete completed id =', file.id);
							}
						);
				}
			});

		return false;
	}
	/*------------------------------------------------------------------------------------*/
	// confirm_close() {
	// 	return false;
	// }
	/*------------------------------------------------------------------------------------*/
	viewUrg(id: string) {
		this._logger.log('view urg', id);
		this._router.navigateByUrl('/urg/view/'+id);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	editUrg(id: string) {
		this._logger.log('edit urg', id);
		this._router.navigateByUrl('/urg/edit/'+id);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	editFile(file: File) {
		this._logger.log('edit file id =', file.id, file.title);
		let new_title = prompt('Введите новое описание файла', file.title);
		if (new_title) {
			this._logger.log('new title =', new_title);
			file.title = new_title;
			this._urgService
				.saveFile(file)
				.subscribe(
					(resp:any) => this._logger.debug(<any>resp),
					(error:any) => this.errorMessage = <any>error,
					() => {
						this._logger.debug('file renamed');
					}
				);
		}

		return false;
	}
	/*------------------------------------------------------------------------------------*/
	reloadEquipment() {
		this._logger.log('reload equipment', this.urg.id);
		this.equipment_load_in_progress = true;
		this.loadEquipment(this.urg.id);
		//this._AppService.snackbarOpen('Список оборудования обновлён.');
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	reloadInstrumentations() {
		this._logger.log('reload instrumentations', this.urg.id);
		this.instrumentation_load_in_progress = true;
		this.loadInstrumentations(this.urg.id);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	reloadFiles() {
		this._logger.log('reload files', this.urg.id);
		this._urgService
			.getFiles(this.urg.id)
			.subscribe(
				(resp:any) => this.urg.files = resp,
				(error:any) => this.errorMessage = <any>error,
				() => {
					this._logger.debug('files reloaded');
					this._AppService.toastrOpen('Список файлов загружен.');
				}
			);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	addInstrumentations() {
		//this._logger.log('add instrumentations', this.urg.id);
		this.urg_instrumentation.addInstrumentationShowDialog();
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	addFile() {
		// this._logger.log('add file', this.urg.id);
		// this.add_file = true;
		// this.modal_upload.show();
		// this._logger.log('add file to', this.urg.id);
		this._dialogsService
			.upload('Загрузка файла',
				'',
				{
					id_obj: this.urg.id,
					id_unit: this._AppService.app.currentUnit.id,
					url: this._AppService.baseURL+'file/upload',
				})
			.subscribe(
				(resp:any) => {
					this._logger.debug('confirm result =', resp);
					if (resp)
						this.reloadFiles();
				}
			);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	showRoomDialog(room?: Room) {
		if (!room) {
			this._logger.log('add room', this.urg.id);
			this.tmp_room = new Room();
			this.tmp_room.id_urg = this.urg.id;
			this.urg.rooms.push(this.tmp_room);
		} else {
			this._logger.log('edit room', this.urg.id, room.id);
			this.tmp_room = room;
		}

		this.modal_room.show();
		return false;
	}
	cancelSaveRoom(room: Room) {
		// удалить помещения с id=-1
		for (let i in this.urg.rooms) {
			//this._logger.debug(i, eq[i].id);
			if (this.urg.rooms[i].id < 0) {
				this.urg.rooms.splice(<any>i, 1);
				break;
			}
		}
		return false;
	}
	deleteRoom(room: Room) {
		//if (!confirm('Действительно хотите удалить это помещение?')) return false;
		this._dialogsService
			.confirm('Удаление','Действительно хотите удалить это помещение?')
			.subscribe( (resp:any) => {
				if (resp) {
					this._urgService
						.deleteRoom(room.id)
						.subscribe(
							(resp:any) => {
								this._logger.debug('delete room '+room.id+' =', resp);
								this.refreshRooms();
								this._AppService.toastrOpen('Помещение удалено');
							},
							(error:any) => this.errorMessage = <any>error
						);
				}
			});

		return false;
	}
	saveRoom(room: Room) {
		this._urgService
			.saveRoom(room)
			.subscribe(
				(resp:any) => {
					this._logger.debug('save rooms =', resp);
					this.refreshRooms();
					this._AppService.toastrOpen('Помещение сохранено', 'success');
				},
				(error:any) => this.errorMessage = <any>error
			);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	refreshRooms(showMessage: boolean = false) {
		this._urgService
			.getRooms(this.urg.id)
			.subscribe(
				(resp:any) => {
					this._logger.debug('loaded rooms =', resp);
					this.urg.rooms = resp;
					if (showMessage) this._AppService.toastrOpen('Помещения перезагружены');
				},
				(error:any) => this.errorMessage = <any>error
			);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	refreshData() {
		this._logger.log('refresh data...');
		this.loadUrg(this.urg.id);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	refreshRing() {
		this._logger.log('refresh ring...');

		this._ringService
			.getUrgsInRing(this.urg.id)
			.subscribe(
				(resp:any) => {
					//this._logger.warn(resp);
					this.urg.ring = resp.ring;

				},
				(error:any) => this.errorMessage = <any>error,
				() => {
					this._logger.debug('urg in ring refreshed');
				}
			);

		this.getRingList();

		return false;
	}
	/*------------------------------------------------------------------------------------*/
	saveFile(file?:File) {
		if (file) {
			this._logger.log('save file =', file.id);
			this._urgService
				.saveFile(file)
				.subscribe(
					(resp:any) => this._logger.debug(resp),
					(error:any) => this.errorMessage = <any>error,
					() => {
						this._logger.debug('file renamed');
					}
				);
		} else {
			this._logger.log('save all files');
			for (let f of this.urg.files ) {
				this.saveFile(f);
			}
		}

		return false;
	}
	/*------------------------------------------------------------------------------------*/
	// saveEquipment() {
	// 	this._logger.log('save equipment =', this.urg.id);
	// 	return false;
	// }
	/*------------------------------------------------------------------------------------*/
	saveInstrumentations() {
		this._logger.log('save instrumentations =', this.urg.id);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	saveUrg() {
		this._logger.log('save urg =', this.urg.id);
		this._urgService.save(this.urg)
			.subscribe(
				(resp:any) => {
					this.errors = [];
					this.urg.id = (<any>resp).id;
					this._logger.debug('saved urg =', this.urg.id);
					if ((<any>resp).status==='ok') {
						this.errorMessage = null;
						this._AppService.toastrOpen((<any>resp).message, 'success');
					} else {
						this._AppService.toastrOpen((<any>resp).message, 'error', 0);
						let errors = (<any>resp).errors;
						for (let error in errors) {
							this.errors.push(errors[error][0]);
						}
						this._dialogsService.error('Ошибка сохранения УРГ', this.errors.join("<br>"));
						this.errorMessage = '';
					}

				},
				(error:any) => this.errorMessage
			);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	checkVisible(row:string, check: any) {
		//this._logger.log();
		let data: {[key:string]:any[]} = {
			'floor_material': [1001,1002],
			'building_type': [1001,1002],
			'heating': [1001,1002],
		};
		//this._logger.log(row, data[row], check, data[row].indexOf(check));
		return (data[row].indexOf(check)>-1);
		//return row;
	}
	/*------------------------------------------------------------------------------------*/
	unitSelect(id: any) {
		this._logger.log('select unit =', id);
		this._AppService.app.currentUnit.id = id;
		this.urg.id_unit = id;
	}
	/*------------------------------------------------------------------------------------*/
	changeListForRing(event: any) {
		this._logger.log('for ring =', event);
	}
	/*------------------------------------------------------------------------------------*/
    typeaheadMasterOnSelect(event: any) {
        this._logger.debug('master select =', event);
        this.urg.master.post = event.item.post;
    }
	/*------------------------------------------------------------------------------------*/
	getThis() {
		return this;
	}
	/*------------------------------------------------------------------------------------*/
	cancelMaster() {
		this.urg.master = _.cloneDeep(this.tmpMaster);
		this.masterInEdit = false;
	}
	/*------------------------------------------------------------------------------------*/
    addMaster() {
        this.tmpMaster = _.cloneDeep(this.urg.master);
        this.urg.master = {
            id: -1,
            id_urg: this.urg.id,
            date: new Date(),
            name: '',
            post: ''
        };
        this.masterInEdit = true;
    }
	/*------------------------------------------------------------------------------------*/
	editMaster(master: any) {
		this.tmpMaster = _.cloneDeep(this.urg.master);
        this.urg.master = _.cloneDeep(master);
        this.urg.master.date = new Date(this.urg.master.date);
		this.masterInEdit = true;
	}
	/*------------------------------------------------------------------------------------*/
	saveMaster(master: any) {
		this._logger.debug('change master -', master);
		this._urgService
		 	.saveMaster(master)
			.subscribe(
				(resp:any) => {
                    if (master.id<0) {
                        this.urg.mastersHistory.push(master);
                    } else {
                        for(let i in this.urg.mastersHistory) {
                    		if (this.urg.mastersHistory[i].id == master.id ) {
                                this.urg.mastersHistory[i].name = master.name;
                                this.urg.mastersHistory[i].post = master.post;
                                this.urg.mastersHistory[i].date = new Date(master.date);
							}
						}
					}
					//_.sortBy(this.urg.mastersHistory, ['date']);
                    this.urg.mastersHistory.sort((a: any, b: any) => {
                    	return (new Date(a.date))<(new Date(b.date)) ? 1 : 0;
					});
					this.urg.master.id = resp.id;
					this._AppService.toastrOpen('Ответственное лицо изменено', 'success');
				},
				(error:any) => {
					this._AppService.toastrOpen('Ошибка сохранения ответственного лица', 'error', 0);
				}
			);
		this.masterInEdit = false;
	}
    /*------------------------------------------------------------------------------------*/
    deleteMaster(master: any) {
    	this._dialogsService
			.confirm('Удаление ответственного','Подтвердите удаление "'+master.name+'"')
			.subscribe(
				resp => {
					if (resp) {
                        this._urgService
                            .deleteMaster(master.id)
                            .subscribe(
                                (resp:any) => {
                                    this._logger.debug('delete master ', resp);
                                    _.remove(this.urg.mastersHistory, (e:any) => {return e.id == master.id});
                                    this._AppService.toastrOpen('Ответственное лицо удалено', 'success');
                                },
                                (error:any) => {
                                    this._AppService.toastrOpen('Ошибка удаления ответственного лица', 'error', 0);
                                }
                            );
					}
				}
			);
        return false;
    }
	/*------------------------------------------------------------------------------------*/
	clearAddresPart(part: List) {
		this._logger.debug("DELETE PRESS");
		if (part.name.trim() == '') {
			part.id   = null;
			part.name = null;
		}
	}
	/*------------------------------------------------------------------------------------*/
	getUrgCountFromSelected() {
		//this._logger.debug(this.optionsModel);
		let c = 0;
		for (let o of this.optionsModel) {
			if (typeof o == 'string')
				c += o.split(',').length;
		}
		return c;
	}
	/*------------------------------------------------------------------------------------*/
	/*------------------------------------------------------------------------------------*/
			/* Поиск в справочниках прямо на серваке  */

	public searchInit() {
			// для поиска региона
		this.regionSource = Observable.create((observer:any) => {
			// Runs on every search
			observer.next(this.urg.address.region.name);
		}).mergeMap((term:string) => this._addressService.getRegionsByName(term));

			// для поиска населённого пункта
		this.npSource = Observable.create((observer:any) => {
			observer.next(this.urg.address.np.name);
		}).mergeMap((term:string) => this._addressService.getNpByName(term, this.urg.address.region.id));

			// для поиска улицы
		this.streetSource = Observable.create((observer:any) => {
			observer.next(this.urg.address.street.name);
		}).mergeMap((term:string) => this._addressService.getStreetByName(term, this.urg.address.np.id));
	}

	public getRegionsAsObservable(term:string):Observable<any> {
		return this._addressService.getRegionsByName(term);
	}

	public changeTypeaheadLoading(key: string, e:boolean):void {
		this.typeaheadLoading[key] = e;
		this.typeaheadSelected[key] = false;
	}

	public changeTypeaheadNoResults(key: string, e:boolean):void {
		this.typeaheadNoResults[key] = e;
	}

		// установить id при выборе из результатов поиска (район, город)
	public typeaheadOnSelect(field: string, e:any): void {
		this.urg.address[field].id = e.item.id;
		this._logger.log('Selected value: ', e.item);
		this.typeaheadSelected[field] = true;
	}

	public typeaheadOwnerOnSelect(key: string, e:any): void {
		this._logger.log('Selected value: ', e.item);
		this.urg.id_owner = e.item.id;
		this.typeaheadSelected[key] = true;
	}

	/*----------------------------------------*/
	public selected(value:any):void {
		this._logger.log('Selected value is: ', value);
	}

	public removed(value:any):void {
		this._logger.log('Removed value is: ', value);
	}

	public typed(value:any):void {
		this._logger.log('New search input: ', value);
	}

	public refreshValue(value:any):void {
		this._logger.log(value);
		this.value = value;
	}
	/*----------------------------------------*/
	addLastWork(id_urg: any, date: Date) {
		//this._logger.debug(id_urg, date);id_urg;
		this._urgService
			.addLastWork(id_urg, date)
			.subscribe(
				resp => {
					this._logger.debug('save last work date:', resp);
					if (resp.status=='ok') {
						this._AppService.toastrOpen('Дата последнего обхода сохранена', 'success');
					} else {
						this._AppService.toastrOpen('Ошибка сохранения даты последнего обхода', 'error');
					}
				}
			);
	}
	/*----------------------------------------*/
}

