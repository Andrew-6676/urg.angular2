import { NgModule }     from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuard }    from '../shared/services/auth-guard.service';

import { UrgComponent, UrgFormComponent, UrgViewComponent, UrgRingComponent } from './index';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'urg',
        component: UrgComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'urg/add',
        component: UrgFormComponent,
        canActivate: [AuthGuard],
	    canDeactivate: []
      },
      {
        path: 'urg/edit/:id',
        component: UrgFormComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'urg/view/:id',
        component: UrgViewComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'ring',
        component: UrgRingComponent,
        canActivate: [AuthGuard]
      }
    ])
  ],
  exports: [RouterModule]
})
export class UrgRoutingModule { }
