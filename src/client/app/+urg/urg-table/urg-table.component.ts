import { Component, OnInit, Input } from '@angular/core';
import { Router }                   from '@angular/router';
import { Observable }               from 'rxjs/Observable';

import { UrgComponent }    from '../urg.component';
import { Address }         from '../../shared/models/address';

import { AppService }      from '../../shared/services/app.service';
import { LoggerService }   from '../../shared/services/logger.service';
import { RefbookService }  from '../../shared/services/refbook.service';
import { DialogsService }  from '../../shared/components/dialogs/dialogs.service';
import { ReportsService }  from '../../shared/services/reports.service';

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
	moduleId: module.id,
	selector: 'urg-table',
	templateUrl: 'urg-table.component.html',
	styleUrls: ['urg-table.component.css'],
})

export class UrgTableComponent implements OnInit {
	@Input() unit_id: string;
	@Input() urgs: any[] = [];
	@Input() total: number;
	@Input() parent: UrgComponent;

	errorMessage: string;

	lastDialogResult: string;

	public inCurrentRing: Observable<any>;
	/**
	/**
	 * Creates an instance of the HomeComponent with the injected
	 * AppService.
	 *
	 * @param {AppService} _AppService - The injected AppService.
	 * @param {LoggerService} _logger - The injected LoggerService.
	 * @param {Router} _router - The injected Router.
	 * @param {RefbookService} _refbookService - The injected RefbookService.
	 * @param {ReportsService} _reports - The injected ReportsService.
	 * @param {DialogsService} _dialogService - The injected DialogsService.
	 */
	constructor(public _AppService: AppService,
	            private _logger: LoggerService,
	            private _router: Router,
	            private _refbookService: RefbookService,
	            public _reports: ReportsService,
	            public _dialogService: DialogsService) {}
	/*------------------------------------------------------------------------------------*/
	ngOnInit() {
		//this.getUnits();
		//this.getUrgs();
		//this.getItems(this.unit_id);
	}
	/*------------------------------------------------------------------------------------*/
	// getItems(id: string) {
	// 	this._urgService
	// 		.getByUnitId(id, 'view')
	// 		.subscribe(
	// 			(resp:any) => this.parent.urgs = resp,
	// 			(error:any) =>  this.errorMessage = <any>error,
	// 			() => {
	// 				//this._logger.log(this.units);
	// 				this._logger.debug('items loaded');
	// 			}
	// 		);
	// }
	/*------------------------------------------------------------------------------------*/
	view(id: string) {
		this._logger.debug('view urg', id);
		this._router.navigateByUrl('/urg/view/'+id);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	print(report: string, id: string) {
		window.open(this._AppService.baseURL+'print/'+report+'/'+id);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	refreshData() {
		this.parent.loadUrgs();
		this._AppService.toastrOpen('Данные перезагружены');
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	applyFilter() {
		this.parent.loadUrgs();
	}
	/*------------------------------------------------------------------------------------*/
	sortUrg(column:string, event:MouseEvent) {
		//this._logger.debug("click sort:", column, event);

		let tmp:any = {};
		Object.assign(tmp, this._AppService.filter.urg.sort);

		this._logger.debug("tmp",tmp);

		if (event.ctrlKey) {
				// зажат Ctrl - возможна сортировка по нескольким столбцам

		} else {
				// сортировка только по одному столбцу
			this._AppService.filter.urg.sort = {};


		}

		if (!(column in tmp)) {
			this._AppService.filter.urg.sort[column] = 'asc';
		} else {
			switch (tmp[column]) {
				case "asc":
					this._AppService.filter.urg.sort[column] = 'desc';
					break;
				case "desc":
					delete this._AppService.filter.urg.sort[column];
			}
		}

		this.parent.loadUrgs();

		return false;
	}
	/*------------------------------------------------------------------------------------*/
	getUrgHeadClass(column:string) {
		return {
			'desc': this._AppService.filter.urg.sort[column]=='desc',
			'asc': this._AppService.filter.urg.sort[column]=='asc'
		};
	}
	/*------------------------------------------------------------------------------------*/
	getAddress(addr: Address) {
		let res: string = '';
		if (addr) {
			if (addr.np.id>0) res += addr.np.name;
			if (addr.street.id>0) res += ', '+addr.street.name;
			if (addr.house) res += ', д.'+addr.house;
			if (addr.building) res += ', к.'+addr.building;
			//if (addr.flat) res += ', кв.'+addr.flat;
			//addr.np.name+', '+addr.street.name+', д.'+addr.house+'-'+addr.building+', '+addr.flat
		} else {
			res = '- не указан -';
		}

		return res;
	}
	/*------------------------------------------------------------------------------------*/
	deleteUrg(urg: any) {
		this._dialogService
			.confirm('удаление УРГ', 'Уверены, что хотите удалить '+this._refbookService.indexedData[urg.type.id].name + ' №'+urg.num+'?')
			.subscribe(
				(resp:any) => {
					if (resp) {
						this._logger.debug('delete urg id =', urg.id);
						this.parent._urgService
							.delete(urg.id)
							.subscribe(
								(resp:any) => {
									this._logger.debug('resp', resp);
								},
								(error:any) => {
									this.errorMessage = <any>error;
									this._AppService.toastrOpen('[ОШИБКА] '+this.errorMessage, 'error', 10000);
								},
								() => {
									this.parent.getUrgsCount();
									let str_urg: string = this._refbookService.indexedData[urg.type.id].name + ' №'+urg.num;
									// del from list
									for (let i in this.urgs) {
										if (this.urgs[i].id === urg.id) {
											this.urgs.splice(<any>i, 1);
											break;
										}
									}
									this._AppService.toastrOpen(str_urg+' удален');
									this._logger.debug('deleted urg =', urg.id);
								}
							);
					}
				}
			);

		// let dialogRef = this._dialog.open(
		// 	DialogComponent,
		// 	{
		// 		role: 'dialog',
		// 		disableClose: true  // запретить закрытие по клику вне окна и по ESC
		// 	}
		// );
		//
		// dialogRef.afterClosed()
		// 	.subscribe(
		// 		result => {
		// 			//this.lastDialogResult = <any>result;
		// 			if (result===true) {
		// 				this._logger.debug('delete urg id =', urg.id);
		// 				this.parent._urgService
		// 					.delete(urg.id)
		// 					.subscribe(
		// 						(resp:any) => {
		// 							this._logger.debug('resp', resp);
		// 						},
		// 						(error:any) => {
		// 							this.errorMessage = <any>error;
		// 							this._AppService.snackbarOpen('[ОШИБКА] '+this.errorMessage, 5000);
		// 						},
		// 						() => {
		// 							this.parent.getUrgsCount();
		// 							let str_urg: string = this._refbookService.indexedData[urg.type.id].name + ' №'+urg.num;
		// 							// del from list
		// 							for (let i in this.urgs) {
		// 								if (this.urgs[i].id === urg.id) {
		// 									this.urgs.splice(<any>i, 1);
		// 									break;
		// 								}
		// 							}
		// 							this._AppService.snackbarOpen(str_urg+' удален', 5000);
		// 							this._logger.debug('deleted urg =', urg.id);
		// 						}
		// 					);
		// 			}
		// 			dialogRef = null;
		// 		}
		// 	);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	countPages() {
		return this.total/this._AppService.filter.urg.page.itemsPP;
	}
	/*------------------------------------------------------------------------------------*/
	pageChanged(event :any) {
		if (event.page !== this._AppService.filter.urg.page.currentPage) {
			this._logger.log('change page', event);
			this._AppService.filter.urg.page.currentPage = event.page;
			this.parent.loadUrgs();
		}

	}
	/*------------------------------------------------------------------------------------*/
	perPageChanged(ppp:any) {
		this._logger.log('change itemsPP', ppp);
		this._AppService.filter.urg.page.itemsPP = ppp;
		this._AppService.filter.urg.page.currentPage = 1;
		this.parent.loadUrgs();
	}
	/*------------------------------------------------------------------------------------*/
	trackByFn(index:any, item:any) {
		return index;
	}
	/*------------------------------------------------------------------------------------*/
	do_nothing() {
		return false;
	}
}
/*-----------------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------------*/


