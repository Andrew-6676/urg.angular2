import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTabGroup, MatTabChangeEvent } from '@angular/material';
import { Router }            from '@angular/router';
import { Observable }        from 'rxjs/Observable';

import { UrgTableComponent } from './urg-table/urg-table.component';

import { AppService }        from '../shared/services/app.service';
import { LoggerService }     from '../shared/services/logger.service';
import { UrgService }        from '../shared/services/urg.service';
import { UnitService }       from '../shared/services/unit.service';
import { RingService }       from '../shared/services/ring.service';


/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
	moduleId: module.id,
	selector: 'sd-home',
	templateUrl: 'urg.component.html',
	styleUrls: ['urg.component.css'],
	providers: [UrgService, RingService, UnitService]
})

export class UrgComponent implements OnInit {
	@ViewChild('opghTabs') opghTabs: MatTabGroup;
	@ViewChild('urgTable') urgTable: UrgTableComponent;

	newName: string = '';
	errorMessage: string;

	editMode: boolean = false;

	//units: any[] = [];
	//currentUnit: any = '-1';

	//indexedOPGH: { [id: number]: any; } = { };
	currentOrghIndex: any = 0;

	urgs: any[] = [];
	urgsCount: number = 0;

	/**
	 * Creates an instance of the HomeComponent with the injected
	 * AppService.
	 *
	 * @param {AppService} _AppService - The injected AppService.
	 * @param {LoggerService} _logger - The injected LoggerService.
	 * @param {UrgService} _urgService - The injected UrgService.
	 * @param {UnitService} _unitService - The injected UnitService.
	 * @param {UnitService} _ringService - The injected RingService.
	 * @param {Router} _router - The injected Router.
	 */
	constructor(public _AppService: AppService,
	            private _logger: LoggerService,
	            public _urgService: UrgService,
	            private _unitService: UnitService,
	            private _ringService: RingService,
	            private _router: Router) {
		this._AppService.app.pageTitle = '<%= APP_TITLE %>';
		//this.currentUnit = this._AppService.user.unit.id;
		//this._logger.warn('constructor');
	}

	/**
	 * Get the names OnInit
	 */
	ngOnInit() {
		//this.getUnits();
		//this.getUrgs();
		//this._logger.warn('init');
		this.getUrgsCount();
		this.loadUrgs(this._AppService.app.currentUnit.id);
	}

	/*------------------------------------------------------------------------------------*/
		// TODO: повторяется в home.component
	getUrgsCount() {
		this._urgService
			.countAll()
			.subscribe(
				(resp:any) => {this.urgsCount = resp},
				(error:any) => this.errorMessage = <any>error
			);
	}
	/*------------------------------------------------------------------------------------*/
	// getUnits() {
	// 	this._unitService
	// 		.getCompany()
	// 		.subscribe(
	// 			(resp:any) => this.units = resp,
	// 			(error:any) =>  this.errorMessage = <any>error,
	// 			() => {
	// 				// for (let i in this.units) {
	// 				// 	this.indexedOPGH[this.units[i].id] = i;
	// 				// }
	// 				// this.currentOrghIndex = this.indexedOPGH[this._AppService.app.currentOPGH.id];
	// 				// this._logger.debug(this.indexedOPGH, this.currentOrghIndex);
	// 				this._logger.debug('units loaded');
	// 			}
	// 		);
	// }
	/*------------------------------------------------------------------------------------*/
	opghTabChange(tab: MatTabChangeEvent) {
		this._logger.log('change tab to ', tab.index);
		//this._logger.debug(this.units[tab.index]);
		let id: any = this._unitService.units[tab.index].id;
		this._AppService.app.currentUnit.id = id;
		this._AppService.app.currentOPGH.id = id;
		this.urgs = [];
		if (this._AppService.dbg) {
			this._AppService.app.pageTitle = 'Узлы редуцирования газа (unit=' + id + ')';
		} else {
			this._AppService.app.pageTitle = 'Узлы редуцирования газа "' + this._unitService.units[tab.index].name + '"';
		}
	}

	/*------------------------------------------------------------------------------------*/
	// getCurrentOPGHIndex(): number {
	// 	let i = this._unitService.indexOPGH[this._AppService.app.currentOPGH.id];
	// 	this._logger.debug('=========', i);
	// // 	for (let i in this.units) {
	// // 		//this._logger.log(this.units[i].id, '=', this._AppService.app.currentUnit.id);
	// // 		if (this.units[i].id == this._AppService.app.currentUnit.id) {
	// // 			this._logger.log(i, this.units[i].id, '=', this._AppService.app.currentUnit.id);
	// // 			return <any>i;
	// // 		}
	// // 	}
	// 	return 1;
	// }
	/*------------------------------------------------------------------------------------*/
	// unitSelect(id:string) {
	// 	this._logger.debug('select unit =', id);
	// 	this._urgService
	// 		.getByUnitId(id)
	// 		.subscribe(
	// 			(resp:any) => this.urgs = resp,
	// 			(error:any) =>  this.errorMessage = <any>error,
	// 			() => {
	// 				//this._logger.log(this.units);
	// 				this._logger.debug('urgs loaded');
	// 			}
	// 		);
	// }
	/*------------------------------------------------------------------------------------*/
	unitClick(id: string) {
		// если клик по уже выбранному unit
		if (id == this._AppService.app.currentUnit.id) {
			return false;
		}
		// если админ
		// если клик по дочернему элементу
		// клик по unit к которому принадлежит пользователь
		if (
			!(
				this._AppService.checkRole('root')
				|| this._AppService.app.currentOPGH.id == this._AppService.user.unit.id
				|| this._unitService.indexOPGH_flat[id].parent_gasb == this._AppService.user.unit.id
				|| this._AppService.user.unit.id == id
			)
		) {
			return false
		}

		this._logger.log('select unit id =', id);
		this._AppService.app.currentUnit.id = id;
		this._AppService.filter.urg.page.currentPage = 1;
		this.urgs = [];
		this.loadUrgs(id);

		return false;
	}

	/*------------------------------------------------------------------------------------*/
	loadUrgs(id: string = '-1') {
		this._AppService.loadingInProgress(1);
		if (id === '-1') {
			id = this._AppService.app.currentUnit.id;
		}
		this._logger.log('load urgs for =', id);
		//this.urgs = [];
		this._urgService.loaded = false;
		this._urgService
			.getByUnitId(id, this._AppService.filter.urg, 'view')
			.subscribe(
				(resp:any) => {
                    this._AppService.loadingInProgress(-1);
					this.urgs = resp;
					//this.urgsCount = this._urgService.totalCount;
				},
				(error:any) => {
                    this._AppService.loadingInProgress(-1);
                    this.errorMessage = <any>error;},
				() => {
					//this._logger.log(this.urgs);
					this._urgService.loaded = true;
					if (this._AppService.dbg) {
						this._AppService.app.pageTitle = 'Узлы редуцирования газа (unit='
							+ this._AppService.app.currentUnit.id + ')';
					} else {

						try {
							this._AppService.app.pageTitle = 'Узлы редуцирования газа '
								+ this._unitService.indexOPGH_flat[<any>this._AppService.app.currentUnit.id].name + '';
						} catch (err) {
							this._AppService.app.pageTitle = 'Узлы редуцирования газа';
						}

					}
					//this._AppService.loadingInProgress(-1);
					this._logger.debug('urgs loaded. id = ', id);
				}
			);

		return false;
	}

	/*------------------------------------------------------------------------------------*/
	canAdd(item: any, child?: any) {
		// если админ
		// если клик по дочернему элементу
		// клик по unit к которому принадлежит пользователь
		return (
				this._AppService.checkRole('root')
				|| this._AppService.app.currentOPGH.id == this._AppService.user.unit.id
				|| this._unitService.indexOPGH_flat[item.id].parent_gasb == this._AppService.user.unit.id
				|| this._AppService.user.unit.id == item.id
			);
	}

	/*------------------------------------------------------------------------------------*/
	showParent(item: any, child: any) {
		//this._logger.debug(this._unitService.indexOPGH_flat[this._AppService.app.currentUnit.id], child.id);
		return this._AppService.checkRole('root')  // если админ
			|| child.id_gasb == this._AppService.user.unit.id     // если пользователь из текущего unit
			|| this._AppService.user.unit.id == this._AppService.app.currentOPGH.id   //
			|| this._unitService.indexOPGH_flat[this._AppService.app.currentUnit.id].parent_gasb == child.id_gasb;
	}

	// ргс
	showChild(grandson: any) {
		//this._logger.warn(grandson.id);
		if (!(grandson.id in this._unitService.indexOPGH_flat)) {
			return false
		}
		//this._logger.debug(this._AppService.user.unit.id, this._unitService.indexOPGH_flat[grandson.id].parent_gasb);
		return this._AppService.checkRole('root')
			|| this._AppService.user.unit.id == this._AppService.app.currentOPGH.id
			|| this._AppService.user.unit.id == this._unitService.indexOPGH_flat[grandson.id].parent_gasb
			|| this._AppService.user.unit.id == grandson.id;
		//return true;
	}

	/*------------------------------------------------------------------------------------*/
	addUrg(id_unit?: number) {
		this._logger.debug('add urg');
		if (id_unit) {
			this._AppService.app.currentUnit.id = <any>id_unit;
		}
		this._router.navigateByUrl('/urg/add');
		return false;
	}

	/*------------------------------------------------------------------------------------*/
	editUrg(id: string) {
		this._logger.debug('edit urg');
		this._router.navigateByUrl('/urg/edit/' + id);
		return false;
	}

	/*------------------------------------------------------------------------------------*/
	reloadAllData() {
		this._unitService.reloadCompanies();
		setTimeout(() => {
			this._AppService.toastrOpen('Данные перезагружены');
		}, 1000);
	}

	/*------------------------------------------------------------------------------------*/
	refreshData() {
		this.loadUrgs(this._AppService.app.currentUnit.id);
		return false;
	}

	/*------------------------------------------------------------------------------------*/
	getThis() {
		return this;
	}

	/*------------------------------------------------------------------------------------*/
	getShortName(name: string): string {
		return name.match(/"(.*)"/)[1];
	}

	/*------------------------------------------------------------------------------------*/
	runUrgSearch() {
		this._logger.log('navigate to "search"');
		this._router.navigateByUrl('/search');
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	getInRing(id_ring: string): Observable<any> {
		this._logger.log('get in ring from table...');

		return this._ringService
			.getUrgsInRingById(id_ring);
	}
	/*------------------------------------------------------------------------------------*/
	getCountLabelColor(item: string) {
		if (item.search(/всего/) > -1) return 'default';
		if (item.search(/ПГРП/) > -1) return 'primary';
		if (item.search(/ГРП/) > -1) return 'success';
		if (item.search(/ШРП/) > -1) return 'warning';
		if (item.search(/ГРУ/) > -1) return 'danger';
		return 'default';
	}
}
