import { Component, OnInit, Input, ViewChild } from '@angular/core';

import { AppService }     from '../../shared/services/app.service';
import { LoggerService }  from '../../shared/services/logger.service';
import { RefbookService } from '../../shared/services/refbook.service';

import { ModalDirective }   from 'ngx-bootstrap';
import { UrgFormComponent } from '../urg-form/urg-form.component';

import { Instrumentation }        from '../../shared/models/instrumentation';
import { DialogsService }         from '../../shared/components/dialogs/dialogs.service';
import { InstrumentationService } from '../../shared/services/instrumentation.service';

import * as _ from "lodash";

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
	moduleId: module.id,
	selector: 'urg-instrumentation',
	templateUrl: 'urg-instrumentation.component.html',
	styleUrls: ['urg-instrumentation.component.css']
})

export class UrgInstrumentationComponent implements OnInit {
	@Input() instrumentation: any = [];
	@Input() type: string;
	@Input() parent: UrgFormComponent;

	@ViewChild('modal_instrumentation') modal_instrumentation: ModalDirective;

	errorMessage: string;


	tmp_instrumentation: Instrumentation = new Instrumentation();

	typeRefbook : any = {};
	makeRefbook : any = {};
	modelRefbook : any = {};

	/**
	 * Creates an instance of the EquipmentComponent with the injected
	 * services.
	 *
	 * @param {AppService} _AppService - The injected AppService.
	 * @param {LoggerService} _logger - The injected LoggerService.
	 * @param {RefbookService} _refbookService - The injected RefbookService.
	 * @param {DialogsService} _dialogsService - The injected DialogsService.
	 * @param {InstrumentationService} _instrumentationService - The injected InstrumentationService.
	 */
	constructor(public _AppService: AppService,
	            private _logger: LoggerService,
	            public  _refbookService: RefbookService,
	            private _dialogsService: DialogsService,
	            private _instrumentationService: InstrumentationService
	) {}
	/*------------------------------------------------------------------------------------*/
	ngOnInit() {
		//this.getUnits();
		//this.getUrgs();
		//this.getItems(this.unit_id);
	}
	/*------------------------------------------------------------------------------------*/
	addInstrumentationShowDialog() {
		this.tmp_instrumentation = new Instrumentation();
		this.parent.instrumentation.push(this.tmp_instrumentation);
		this.modal_instrumentation.show();
	}
	/*------------------------------------------------------------------------------------*/
	selectType(id_type: number) {
		this._logger.log('selected type =', id_type);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	deleteFile(item: any, file: any) {
		this.parent.deleteFile(file, () => {
			this._logger.debug('delete file lodash');
			_.remove(item.files, (e:any) => {return e.id == file.id});
		})
	}
	/*------------------------------------------------------------------------------------*/
	addFileInstrumentation(item:Instrumentation) {
		// this.parent.fileToEquipment = {eq_id: device.id};
		// this.parent.modal_upload_equipment.show();
		this._dialogsService
			.upload('Загрузка файла',
							'',
							{
								id_obj: item.id,
								id_unit: this._AppService.app.currentUnit.id,
								url: this._AppService.baseURL+'file/upload',
							})
					.subscribe(
						(resp:any) => {
							this._logger.debug('confirm result =', resp);
							if (resp)
								this._instrumentationService
									.getById(item.id)
									.subscribe(
										(resp:any) => {
											item.files = resp.files;
										}
									);
						}
					);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	editInstrumentation(item: any) {
		this.tmp_instrumentation = item;
		//this.selectEqType(equipment.id_eq_type);
		// if (instrumentation.data==null) {
		// 	alert('Оборудование бвло сохранено не полностью!'+"\n"+ 'Удалите его и добавте заново');
		// 	return false;
		// }
		// this._refbookService.get_spr_data(equipment.id_eq_type)
		// 	.subscribe(
		// 		res => {
		// 			this.makeRefbook = res.make;
		// 			this.modelRefbook = res.model;
		// 			this.typeRefbook = res.type;
		// 			//this._logger.debug(this.modelRefbook);
		// 		}
		// 	);
		// this._logger.log('edit equipment=', equipment);
		// this.tmp_equipment = equipment;
		// if (equipment.id_eq_type==4) {
		// 	this.tmp_equipment.id_make = -99;
		// } else {
		// 	this.tmp_equipment.id_make = equipment.data.model.id_make;
		// }
		this.modal_instrumentation.show();
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	cancelSaveInstrumentation(instrumentation: Instrumentation) {
		this._logger.log('cancel save instrumentation', instrumentation);
		// удалить оборудование с id=-1

		for (let i in this.parent.instrumentation) {
			//this._logger.debug(i, eq[i].id);
			if (this.parent.instrumentation[i].id < 0) {
				this.parent.instrumentation.splice(<any>i, 1);
				break;
			}
		}
		//this.equipment[]
	}
	/*------------------------------------------------------------------------------------*/
	saveInstrumentation(instrumentation: Instrumentation) {
		this._logger.log('save instrumentation', instrumentation);
		this.parent._instrumentationService
			.save(instrumentation, this.parent.urg.id)
			.subscribe(
				(resp:any) => {
					this.parent.errors = [];
					if ((<any>resp).status==='ok') {
						this.tmp_instrumentation.id = (<any>resp).id;
						this._AppService.toastrOpen((<any>resp).message, 'success');
						this._logger.debug('saved equipment =', this.tmp_instrumentation.id);
					} else {
						this._AppService.toastrOpen((<any>resp).message, 'error', 0);
						let errors = (<any>resp).errors;
						for (let error in errors) {
							this.parent.errors.push(errors[error][0]);
							//this._logger.warn(errors[error][0]);
						}
						this.errorMessage = '';
					}
				},
				(error:any) => this.errorMessage
			);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	deleteInstrumentation(item: Instrumentation) {
		this._logger.log('delete instrumentation id =', item.id);
		//if (!confirm('Действительно хотите удалить "'+this._refbookService.indexedData[item.id_type].name+'"?')) return false;
		this._dialogsService
			.confirm('Удалить','Действительно хотите удалить "'+this._refbookService.indexedData[item.id_type].name+'"?')
			.subscribe((resp:any) => {
				if (resp) {
					this.parent._instrumentationService
						.delete(item.id)
						.subscribe(
							(resp:any) => {
								this._AppService.toastrOpen('КИП удален');
								this._logger.debug('deleted equipment =', item.id);

								let instr = this.parent.instrumentation;
								for (let i in instr) {
									this._logger.debug(i, instr[i].id);
									if (instr[i].id === item.id) {
										instr.splice(<any>i, 1);
										break;
									}
								}
							},
							(error:any) => this.errorMessage
						);
				}
			});

		return false;
	}
	/*------------------------------------------------------------------------------------*/
	getMakeSpr(id_type: number) {
		let spr: any = [];

		for (let item in this._refbookService.instrumentationsprs) {
			if (this._refbookService.instrumentationsprs[item].id_parent==id_type) {
				spr.push(this._refbookService.instrumentationsprs[item]);
			}
		}
		//this._logger.log(spr);
		//this._logger.log(this._refbookService.instrumentationsprs);
		spr.push({id:-2, name:'-не задана-'});
		spr.push({id:-1, name:'-отсутствует-'});
		return spr;
	}
	/*------------------------------------------------------------------------------------*/
	refreshData() {
		//this.parent.loadEquipment(this.parent.urg.id);
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	do_nothing() {
		return false;
	}
}
