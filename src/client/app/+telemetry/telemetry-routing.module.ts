import { NgModule }           from '@angular/core';
import { RouterModule }       from '@angular/router';
import { TelemetryComponent } from './telemetry.component';
import { AuthGuard }          from '../shared/services/auth-guard.service';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'telemetry',
        component: TelemetryComponent,
        canActivate: [AuthGuard]
      }
    ])
  ],
  exports: [RouterModule]
})
export class TelemetryRoutingModule { }
