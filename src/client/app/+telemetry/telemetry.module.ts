import { NgModule }               from '@angular/core';
import { CommonModule }           from '@angular/common';
import { TelemetryComponent }     from './telemetry.component';
import { SharedModule }           from '../shared/shared.module';
import { TelemetryRoutingModule } from './telemetry-routing.module';

@NgModule({
    imports: [CommonModule, SharedModule.forRoot(), TelemetryRoutingModule],
    declarations: [TelemetryComponent],
    exports: [TelemetryComponent]
})

export class TelemetryModule { }
