import { Component }   from '@angular/core';
import { Router }      from '@angular/router';

import { AppService }     from '../shared/services/app.service';
import { LoggerService }  from '../shared/services/logger.service';
import { UnitService }    from '../shared/services/unit.service';


/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
	moduleId: module.id,
	selector: 'sd-login',
	templateUrl: 'login.component.html',
	styleUrls: ['login.component.css'],
})

export class LoginComponent /*implements OnInit*/ {

	errorMessage: string;
	message: string;
	login_in_progress: boolean = false;
	user = {name: '', pass: '', remember_me:true};
	//n=0;
	/**
	 * Creates an instance of the LoginComponent with the injected
	 * AppService.
	 *
	 * @param {AppService} _AppService - The injected AppService.
	 * @param {LoggerService} _logger - The injected AppService.
	 * @param {Router} _router - The injected Router.
	 * @param {UnitService} _unit - The injected UnitService.
	 */
	constructor(
		public _AppService: AppService,
	    private _logger: LoggerService,
	    public _router: Router,
	    public _unit: UnitService
	) {
		this._AppService.app.pageTitle = 'Войти';
		this.message = this._AppService.user.isLoggedIn ? 'Вы вошли как:' : 'Введите учётные данные';
	}

	/*-----------------------------------------------------------------------------------------------*/
	login() {
		//this.n++;
		localStorage.removeItem('auth_token');
		this.message = 'Аутентификация ...';
		this.login_in_progress = true;
		this._AppService
			.login(this.user)
			.subscribe(
				(resp:any) => {
					this._logger.debug('auth res:',resp );
					this.user.pass = '';
					this.login_in_progress = false;
					this.message = resp.message;
					if (resp.status) {
						this._AppService.app.currentUnit = {
							id: resp.user.id_unit,
							//name: this._unit.indexOPGH_flat[resp.user.id_unit] ? this._unit.indexOPGH_flat[resp.user.id_unit].name : this._unit.indexOPGH2[resp.user.id_unit].name
							name: '=='
						} ;
						let id_opgh = this._unit.getOpghByUnit(resp.user.id_unit);
						this._AppService.app.currentOPGH  = {
							id: id_opgh,
							name: this._unit.indexOPGH2[id_opgh] ? this._unit.indexOPGH2[id_opgh].name : '-'
						};

						this._logger.styledLog('%c LOGIN:', 'background: #2dad40; color: #fff; font-weight: bold' , resp);

						if (this._AppService.redirectUrl) {
							let redirect = this._AppService.redirectUrl;
							this._AppService.redirectUrl = '';
							// Redirect the user
							this._router.navigate([redirect]);
						}
					}
				},
				(error:any) => this.errorMessage = <any>error
			);
	}

	/*-----------------------------------------------------------------------------------------------*/
	logout() {
		this._AppService.logout()
			.subscribe(
				(resp:any) => {
					this._logger.styledLog('%c LOGOUT:', 'background: #f00; color: #fff; font-weight: bold' , resp);
				}
			);
		this.message = 'Введите учётные данные';
	}

	/*-----------------------------------------------------------------------------------------------*/
	//ngOnInit() {}
	/*-----------------------------------------------------------------------------------------------*/
}
