import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule }         from '@angular/core';
import { CommonModule }     from '@angular/common';

import { PaginationModule } from 'ngx-bootstrap';
import { TypeaheadModule }  from 'ngx-bootstrap';
import { SelectModule }     from '../shared/components/ng2-select/select.module';

import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';

import { SharedModule }       from '../shared/shared.module';

import { SearchComponent }    from './search.component';
import { SearchRoutingModule }from './search-routing.module';

@NgModule({
	imports: [
		CommonModule,
		SharedModule,
		PaginationModule.forRoot(),
		TypeaheadModule.forRoot(),
		SelectModule,
		FormsModule,
		ReactiveFormsModule,
        SearchRoutingModule,
		MultiselectDropdownModule
	],
	declarations: [
		SearchComponent
	],
	exports: [SearchComponent]
})

export class SearchModule {}

