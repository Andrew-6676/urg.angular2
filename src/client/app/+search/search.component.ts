import { Component, OnInit, Input }       from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { AppService }     from '../shared/services/app.service';
import { LoggerService }  from '../shared/services/logger.service';
import { UrgService }     from '../shared/services/urg.service';
import { UnitService }    from '../shared/services/unit.service';
import { RefbookService } from '../shared/services/refbook.service';



/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
	moduleId: module.id,
	selector: 'urg-search',
	templateUrl: 'search.component.html',
	styleUrls: ['search.component.css'],
	providers: [UnitService, UrgService]
})

export class SearchComponent implements OnInit {
	loaded: boolean = false;
	errorMessage: string;
	//urg: UrgTable;

	filter = {

	};

	constructor(public _AppService: AppService,
	            private _logger: LoggerService,
	            private _aroute: ActivatedRoute,
	            private _router: Router,
	            public _refbookService: RefbookService,
	            private _urgService: UrgService
	) {
		this._AppService.app.pageTitle = 'Поиск';
	}
	/*------------------------------------------------------------------------------------*/
	ngOnInit() {
		//this.getUnits();
		//this.getUrgs();
		//this.getItems(this.unit_id);
	}
	/*------------------------------------------------------------------------------------*/
	runSearch() {
		this._logger.log('run search');
		return false;
	}
	/*------------------------------------------------------------------------------------*/
	do_nothing() {
		return false;
	}
}
