import { NgModule }          from '@angular/core';
import { CommonModule }      from '@angular/common';
import { HelpComponent }     from './help.component';
import { SharedModule }      from '../shared/shared.module';
import { HelpRoutingModule } from './help-routing.module';

import { HelpService }       from '../shared/services/help.service'

@NgModule({
    imports: [CommonModule, SharedModule.forRoot(), HelpRoutingModule],
    declarations: [HelpComponent],
    exports: [HelpComponent],
	providers: [HelpService]
})

export class HelpModule { }
