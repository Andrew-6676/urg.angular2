import { Component }      from '@angular/core';
import { ActivatedRoute, Params }  from '@angular/router';
import { AppService }     from '../shared/services/app.service';
import { LoggerService }  from '../shared/services/logger.service';
import { HelpService }    from '../shared/services/help.service';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-help',
  templateUrl: 'help.component.html',
  styleUrls: ['help.component.css']
})
export class HelpComponent {
	help : {title: string, text:string} = {title: '-', text:'-'};
	title:string = '-';

	constructor(
		public _AppService: AppService,
	    private _logger: LoggerService,
		private _aroute: ActivatedRoute,
	    private _helpService: HelpService
	) {
		this._AppService.app.pageTitle = 'Справка';

		this._aroute.params.forEach((params: Params)  => {
			let index = params['index'];

			if (index !== undefined) {
				this.loadHelp(index);
			} else {
				this.loadHelp();
			}
		});
	}

	loadHelp(index: string='index') {
		this._helpService.getByIndex(index)
			.subscribe(
				(resp:any) => {
					if (resp.length > 0) {
						this.help = resp[0];
						this._logger.debug('loaded help =', resp[0]);
					} else {
						this.help.title = 'Раздел не найден';
						this.help.text = 'Возможно, данный раздел справки ещё не заполнен';
					}
				}
			);
		this.title = index ? index : 'index';
		this._logger.warn(index);
	}
}
