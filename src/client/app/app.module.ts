import { NgModule }         from '@angular/core';
import { BrowserModule }    from '@angular/platform-browser';
import { APP_BASE_HREF }    from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent }     from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { TelemetryModule } from './+telemetry/telemetry.module';
import { AboutModule }     from './+about/about.module';
import { HelpModule }      from './+help/help.module';
import { HomeModule }      from './+home/home.module';
import { DocsModule }      from './+docs/docs.module';
import { LoginModule }     from './+login/login.module';
import { UrgModule }       from './+urg/urg.module';
import { SearchModule }    from './+search/search.module';

import { SharedModule }    from './shared/shared.module';
import { RefbookModule }   from './+refbook/refbook.module';

import { BreadcrumbModule, BreadcrumbService } from './shared/components/breadcrumb/breadcrumb.module';

import { AppService }      from './shared/services/app.service';
import { RefbookService }  from './shared/services/refbook.service';
import { AuthGuard }       from './shared/services/auth-guard.service';
import { UnitService }     from './shared/services/unit.service';

//import 'hammerjs/hammer.js';

@NgModule({
  imports: [BrowserModule, HttpClientModule, AppRoutingModule, SharedModule.forRoot(),
    HomeModule,
    DocsModule,
    AboutModule,
    HelpModule,
    TelemetryModule,
    LoginModule,
    UrgModule,
    SearchModule,
    RefbookModule,
    SharedModule.forRoot(),
    BreadcrumbModule
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    BreadcrumbService,
    AppService,
    UnitService,
    RefbookService,
    AuthGuard,
    {
      provide: APP_BASE_HREF,
      useValue: '<%= APP_BASE %>'
    }],
  bootstrap: [AppComponent]

})
export class AppModule {
}
