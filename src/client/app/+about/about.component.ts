import { Component }      from '@angular/core';
import { AppService }     from '../shared/services/app.service';
import { LoggerService }  from '../shared/services/logger.service';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-about',
  templateUrl: 'about.component.html',
  styleUrls: ['about.component.css']
})
export class AboutComponent {
	constructor(public _AppService: AppService,
	            private _logger: LoggerService
	) {
		this._AppService.app.pageTitle = 'О приложении';
	}

}
