import { NgModule }        from '@angular/core';
import { CommonModule }    from '@angular/common';
import { AboutComponent }  from './about.component';
import { SharedModule }    from '../shared/shared.module';
import { AboutRoutingModule } from './about-routing.module';

@NgModule({
    imports: [CommonModule, SharedModule.forRoot(), AboutRoutingModule],
    declarations: [AboutComponent],
    exports: [AboutComponent]
})

export class AboutModule { }
