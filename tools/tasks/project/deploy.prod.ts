import * as gulp from 'gulp';
import { join }  from 'path';
import Config    from '../../config';
import * as util from 'gulp-util';


export = () => {
	//console.log('Copy from '+Config.PROD_DEST+' to '+Config.DEPLOY_PROD_DEST);
	util.log('Copy', util.colors.yellow('from ' + Config.PROD_DEST + ' to ' + Config.DEPLOY_PROD_DEST));
	return gulp.src(join(Config.PROD_DEST, '**/*'))
		.pipe(gulp.dest(Config.DEPLOY_PROD_DEST));
};

