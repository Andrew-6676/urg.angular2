import Config from '../../config';
import { clean } from '../../utils';

/**
 * Удаляет всё из папки frontend/web
 * а надо оставить симлинки, .htaccess и, возможно, ещё что-то
 */
export = clean(Config.DEPLOY_PROD_DEST);
