import * as gulp from 'gulp';
import { join }  from 'path';
import Config    from '../../config';
import * as util from 'gulp-util';
//import * as gzip  from 'gulp-gzip';
let gzip = require('gulp-gzip');

export = () => {
	util.log('Compress', util.colors.yellow(Config.APP_DEST+'/js/app.js, /js/shims.js, /css/main.css'));
	//shims.js
	return gulp.src([join(Config.APP_DEST, '**/*.js'), join(Config.APP_DEST, '**/*.css')])
		.pipe(gzip(
			{
				//append: true,
				extension: 'gz',
				//preExtension: 'gz',
				level: 9
			}
		))
		.pipe(gulp.dest(Config.APP_DEST));
};

