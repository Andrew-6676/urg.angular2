import { EnvConfig } from './env-config.interface';

const DevConfig: EnvConfig = {
  API: 'http://urg.backend/',
  ENV: 'DEV'
};

export = DevConfig;

