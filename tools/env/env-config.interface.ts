// Feel free to extend this interface
// depending on your app specific config.
export interface EnvConfig {
  API?: string;
  APP_BASE?: string;
  ENV?: string;
  VERSION?: string;
}
