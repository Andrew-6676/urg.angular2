import { EnvConfig }     from './env-config.interface';
//import { ProjectConfig } from '../config/project.config';

import config from '../config';

const ProdConfig: EnvConfig = {
  API: config.APP_BASE+'backend/',
  ENV: 'PROD'
};

export = ProdConfig;

