import { join } from 'path';
import { argv } from 'yargs';
import { BUILD_TYPES, SeedConfig } from './seed.config';
import { ExtendPackages } from './seed.config.interfaces';
/**
 * This class extends the basic seed configuration, allowing for project specific overrides. A few examples can be found
 * below.
 */
export class ProjectConfig extends SeedConfig {

	PROJECT_TASKS_DIR = join(process.cwd(), this.TOOLS_DIR, 'tasks', 'project');

	/*--------------*/
	PROD_DEST = argv['region'] == 'vitebsk' ? `${this.DIST_DIR}/prod.vitebsk` : `${this.DIST_DIR}/prod.rb`;
	APP_DEST = this.BUILD_TYPE === BUILD_TYPES.DEVELOPMENT ? this.DEV_DEST : this.PROD_DEST;
	CSS_DEST = `${this.APP_DEST}/css`;
	JS_DEST = `${this.APP_DEST}/js`;
	/*-------------*/

	FONTS_DEST = `${this.APP_DEST}/fonts`;
	FONTS_SRC = [
		'node_modules/bootstrap3/dist/fonts/**',
	];
	DEPLOY_PROD_DEST = '../urg.yii2/frontend/web';
	ENABLE_SCSS = true;
	APP_TITLE = 'Паспорт узла редуцирования газа';

	constructor() {
		super();

		// this.APP_TITLE = 'Put name of your app here';
		// this.GOOGLE_ANALYTICS_ID = 'Your site's ID';

		/* Enable typeless compiler runs (faster) between typed compiler runs. */
		// this.TYPED_COMPILE_INTERVAL = 5;

		// Add `NPM` third-party libraries to be injected/bundled.
		this.NPM_DEPENDENCIES = [
			...this.NPM_DEPENDENCIES,
			{src: 'jquery/dist/jquery.min.js', inject: 'libs'},
			{src: 'bootstrap3/dist/js/bootstrap.min.js', inject: 'libs'},
			{src: 'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', inject: 'libs'},
			{src: 'bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js', inject: 'libs'},
			{src: 'bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css', inject: true, vendor: false},
			{src: 'ngx-toastr/toastr.css', inject: true, vendor: false},
			//{src: '@angular/material/core/theming/prebuilt/indigo-pink.css', inject: true}
		];

		// Add `local` third-party libraries to be injected/bundled.
		this.APP_ASSETS = [
			// {src: `${this.APP_SRC}/your-path-to-lib/libs/jquery-ui.js`, inject: true, vendor: false}
			// {src: `${this.CSS_SRC}/path-to-lib/test-lib.css`, inject: true, vendor: false},
		];

		this.ROLLUP_INCLUDE_DIR = [
			...this.ROLLUP_INCLUDE_DIR,
			//'node_modules/moment/**'
		];

		this.ROLLUP_NAMED_EXPORTS = [
			...this.ROLLUP_NAMED_EXPORTS,
			//{'node_modules/immutable/dist/immutable.js': [ 'Map' ]},
		];

		// Add packages (e.g. ng2-translate)
		// let additionalPackages: ExtendPackages[] = [{
		//   name: 'ng2-translate',
		//   // Path to the package's bundle
		//   path: 'node_modules/ng2-translate/bundles/ng2-translate.umd.js'
		// }];
		//
		// this.addPackagesBundles(additionalPackages);


		let material_modules = [
			'core',             'button',
			'input',    	    'form-field',
			'dialog',   	    'list',
			'icon',			    'card',
			'tooltip',		    'tabs',
			'progress-bar',     'checkbox',
			'progress-spinner', 'toolbar',
			'expansion',        'button-toggle',
			'divider'
		];

		let material_cdk_modules = [
			'a11y',			'accordion',
			'bidi',			'coercion',
			'collections',	'keycodes',
			'layout',		'observers',
			'overlay',		'platform',
			'portal',		'scrolling',
			'stepper',		'table',
		];

		let material_packages:ExtendPackages[] = [{
			name: '@angular/material',
			path: 'node_modules/@angular/material/bundles/material.umd.js'
		}];
		for (let m of material_modules) {
			material_packages.push({
				name: '@angular/material/'+m,
				path: 'node_modules/@angular/material/bundles/material-'+m+'.umd.js',
			});
		}

		let material_cdk_packages:ExtendPackages[] = [{
			name: '@angular/cdk',
			path: 'node_modules/@angular/cdk/bundles/cdk.umd.js'
		}];
		for (let c of material_cdk_modules) {
			material_packages.push({
				name: '@angular/cdk/'+c,
				path: 'node_modules/@angular/cdk/bundles/cdk-'+c+'.umd.js'
			});
		}

		this.addPackagesBundles(material_packages);
		this.addPackagesBundles(material_cdk_packages);

		this.addPackageBundles({
			name: 'moment',
			path: 'node_modules/moment/moment.js',
		});

		this.addPackageBundles({
			name: 'lodash',
			path: 'node_modules/lodash/lodash.js',
		});

		this.addPackageBundles({
			name: 'ngx-bootstrap',
			path: 'node_modules/ngx-bootstrap/bundles/ngx-bootstrap.umd.min.js',
		});

		this.addPackageBundles({
			name: 'angular-2-dropdown-multiselect',
			path: 'node_modules/angular-2-dropdown-multiselect/bundles/dropdown.umd.min.js',
		});


		this.addPackageBundles({
			name: 'ngx-toastr',
			path: 'node_modules/ngx-toastr/toastr.umd.js',
		});

		this.addPackageBundles({
			name: 'ngx-uploader',
			path: 'node_modules/ngx-uploader/bundles/ngx-uploader.umd.js',
		});

		this.addPackageBundles({
		  name: 'ng2-dnd',
		  path: 'node_modules/ng2-dnd/bundles/ng2-dnd.umd.js',
		});

		/* Add proxy middleware */
		// this.PROXY_MIDDLEWARE = [
		//   require('http-proxy-middleware')('/api', { ws: false, target: 'http://localhost:3003' })
		// ];

		/* Add to or override NPM module configurations: */
		// this.PLUGIN_CONFIGS['browser-sync'] = { ghostMode: false };
	}

	// frameworkVersion() {
	// 	let pkg = require('../../package.json');
	// 	return pkg.dependencies["@angular/core"];
	// }
}
